---
title: Clang-tidy
weight: 40
pre: "4.3.2. "
---

The results of running clang-tidy are [here](https://libesys.gitlab.io/esysos/esysos/clang_tidy/esysos_esysc-clang_tidy.html) or directly seen below.

{{< unsafe >}}
<iframe src="https://libesys.gitlab.io/esysos/esysos/clang_tidy/esysos_esysc-clang_tidy.html" width="100%" height="800px"></iframe>
{{< /unsafe >}}
