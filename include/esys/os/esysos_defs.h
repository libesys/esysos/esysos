/*!
 * \file esysos/esysos_defs.h
 * \brief Definitions needed for esysos
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#ifdef ESYSOS_EXPORTS
#define ESYSOS_API __declspec(dllexport)
#elif ESYSOS_USE
#define ESYSOS_API __declspec(dllimport)
#else
#define ESYSOS_API
#endif

#include "esys/os/autolink.h"
