/*!
 * \file esys/os/objplatif.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysos_defs.h"

#include <esys/base/objplatif.h>

namespace esys::os
{

/*! \class ObjPlatIf esys/os/objplatif.h "esys/os/objplatif.h"
 *  \brief Interface to initiliaze object platform
 */
class ESYSOS_API ObjPlatIf : public virtual base::ObjPlatIf
{
public:
    //! Default constructor
    ObjPlatIf();

    //! Destructor
    ~ObjPlatIf() override;

    //! Set the name of the object
    /*!
     * \param[in] name the name of the task
     */
    virtual void set_name(const char *name);

    //! Get the name of the object
    /*!
     * \return the name of the task
     */
    virtual const char *get_name() const;
};

} // namespace esys::os
