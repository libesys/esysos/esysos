/*!
 * \file esys/os/timebase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysos_defs.h"

#include <cstdint>

namespace esys::os
{

/*! \class TimeBase esys/os/timebase.h "esys/os/timebase.h"
 *  \brief Base class of all Time implementations
 */
class ESYSOS_API TimeBase
{
public:
    using Stamp = uint64_t;

    /*! \enum Source esys/os/timebase.h "esys/os/timebase.h"
     *  \brief The time source
     */
    enum Source
    {
        DEFAULT_CLK,            //!< The default clock of the platform
        MCU_CLK_BASED,          //!< A clock based on the MCU
        PERMANENT_EXT_CLK_BASED //!< A permanent external clock source
    };

    /*! \class Struct esys/os/timebase.h "esys/os/timebase.h"
     *  \brief Class holding the time information
     */
    class Struct
    {
    public:
        //! Default constructor
        Struct()
        {
        }

        //! Set the seconds
        /*!
         * \parem[in] seconds the seconds
         */
        void set_seconds(uint8_t seconds)
        {
            m_seconds = seconds;
        }

        //! Get the seconds
        /*!
         * \return the seconds
         */
        uint8_t get_seconds()
        {
            return m_seconds;
        }

        //! Set the minutes
        /*!
         * \parem[in] minutes the minutes
         */
        void set_minutes(uint8_t minutes)
        {
            m_minutes = minutes;
        }

        //! Get the minutes
        /*!
         * \return the minutes
         */
        uint8_t get_minutes()
        {
            return m_minutes;
        }

        //! Set the hours
        /*!
         * \parem[in] hours the hours
         */
        void set_hours(uint8_t hours)
        {
            m_hours = hours;
        }

        //! Get the hours
        /*!
         * \return the hours
         */
        uint8_t get_hours()
        {
            return m_hours;
        }

    protected:
        uint8_t m_hours = 0;   //!< The hours
        uint8_t m_minutes = 0; //!< The minutes
        uint8_t m_seconds = 0; //!< The seconds
    };

    /*! \class Date esys/os/timebase.h "esys/os/timebase.h"
     *  \brief Class holding the date information
     */
    class Date
    {
    public:
        //! Default constructor
        Date()
        {
        }

        //! Set the day
        /*!
         * \parem[in] day the day
         */
        void set_day(uint8_t day)
        {
            m_day = day;
        }

        //! Get the day
        /*!
         * \return the day
         */
        uint8_t get_day()
        {
            return m_day;
        }

        //! Set the month
        /*!
         * \parem[in] month the month
         */
        void set_month(uint8_t month)
        {
            m_month = month;
        }

        //! Get the month
        /*!
         * \return the month
         */
        uint8_t get_month()
        {
            return m_month;
        }

        //! Set the year
        /*!
         * \parem[in] year the year
         */
        void set_year(uint8_t year)
        {
            m_year = year;
        }

        //! Get the year
        /*!
         * \return the year
         */
        uint16_t get_year()
        {
            return m_year;
        }

    protected:
        uint16_t m_year = 0; //!< The year
        uint8_t m_month = 0; //!< The month
        uint8_t m_day = 0;   //!< The day
    };

    /*! \class DateTime esys/os/timebase.h "esys/os/timebase.h"
     *  \brief Class holding the date and time information
     */
    class DateTime
    {
    public:
        //! Default constructor
        DateTime()
            : m_date()
            , m_time()
        {
        }

        //! Constructor from EPOCH time in ms from 1.1.1970
        /*!
         * \param[in] ms EPOCH time in ms from 1.1.1970
         */
        DateTime(uint64_t ms);

        //! Set the EPOCH time in ms from 1.1.1970
        /*!
         * \param[in] ms EPOCH time in ms from 1.1.1970
         */
        void set_epoch_ms(uint64_t ms);

        //! Get the date
        /*!
         * \return the date
         */
        Date &get_date()
        {
            return m_date;
        }

        //! Get the time
        /*!
         * \return the time
         */
        Struct &get_time()
        {
            return m_time;
        }

    protected:
        Date m_date;   //!< The date
        Struct m_time; //!< The time
    };

    //! Set the default source for the time
    /*!
     * \param[in] dft_time_source the default source for the time
     */
    static void set_dft_time_source(Source dft_time_source);

    //! Get the default clock source for the time
    /*!
     * \return the default clock source for the time
     */
    static Source get_dft_time_source();

    //! Convert EPOC time (days from 1.1.1970) to civil calendar time UTC.
    /*!
     * \param[in] days days from 1.1.1970
     * \param[out] year the year
     * \param[out] month the month
     * \param[out] day the day
     */
    static void civil_from_days(uint64_t days, int32_t *year, uint32_t *month, uint32_t *day);

protected:
    static Source m_dft_time_source; //!< The default clock source for the time
};

} // namespace esys::os
