/*!
 * \file esys/os/taskkind.h
 * \brief The TaskType
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysos_defs.h"

#include <cstdint>

namespace esys::os
{

enum class TaskKind : uint8_t
{
    NOT_SET = 0xFF,
    INTERNAL = 0,
    APPLICATION,
    STANDALONE,
};

// Need so that clang-format does set the namespaces on next line
} // namespace esys::os
