/*!
 * \file esysos/version.h
 * \brief Version info for esysos_t
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

#ifndef __ESYSOS_T_VERSION_H__
#define __ESYSOS_T_VERSION_H__

// Bump-up with each new version
#define ESYSOS_T_MAJOR_VERSION 0
#define ESYSOS_T_MINOR_VERSION 0
#define ESYSOS_T_RELEASE_NUMBER 1
#define ESYSOS_T_VERSION_STRING _T("esysos_t 0.0.1")

// Must be updated manually as well each time the version above changes
#define ESYSOS_T_VERSION_NUM_DOT_STRING "0.0.1"
#define ESYSOS_T_VERSION_NUM_STRING "0001"

// nothing should be updated below this line when updating the version

#define ESYSOS_T_VERSION_NUMBER \
    (ESYSOS_T_MAJOR_VERSION * 1000) + (ESYSOS_T_MINOR_VERSION * 100) + ESYSOS_T_RELEASE_NUMBER
#define ESYSOS_T_BETA_NUMBER 1
#define ESYSOS_T_VERSION_FLOAT                                                                   \
    ESYSOS_T_MAJOR_VERSION + (ESYSOS_T_MINOR_VERSION / 10.0) + (ESYSOS_T_RELEASE_NUMBER / 100.0) \
        + (ESYSOS_T_BETA_NUMBER / 10000.0)

// check if the current version is at least major.minor.release
#define ESYSOS_T_CHECK_VERSION(major, minor, release)                                                            \
    (ESYSOS_T_MAJOR_VERSION > (major) || (ESYSOS_T_MAJOR_VERSION == (major) && ESYSOS_T_MINOR_VERSION > (minor)) \
     || (ESYSOS_T_MAJOR_VERSION == (major) && ESYSOS_T_MINOR_VERSION == (minor)                                  \
         && ESYSOS_T_RELEASE_NUMBER >= (release)))

#endif
