/*!
 * \file esys/os/taskplat.h
 * \brief The System Platform
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#if defined(ESYSOS_DFLT_MP)
#include "esys/os/mp/taskplat.h"
#elif defined(ESYSOS_DFLT_BOOST)
#include "esys/os/impl_boost/tasklat.h"
#elif defined(ESYSOS_DFLT_FREERTOS)
#include "esys/os/freertos/taskplat.h"
#elif defined(ESYSOS_DFLT_ESYSC)
#include "esys/os/esysc/taskplat.h"
#else
#define ESYSOS_USE_MP 1
#include "esys/os/mp/taskplat.h"
#endif

#include "esys/os/selectplat.h"
