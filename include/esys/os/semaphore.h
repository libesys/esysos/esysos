/*!
 * \file esys/os/semaphore.h
 * \brief The Semaphore
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#if defined(ESYSOS_DFLT_MP)
#include "esys/os/mp/semaphore.h"
#elif defined(ESYSOS_DFLT_BOOST)
#include "esys/os/impl_boost/semaphore.h"
#elif defined(ESYSOS_DFLT_FREERTOS)
#include "esys/os/freertos/semaphore.h"
#elif defined(ESYSOS_DFLT_ESYSC)
#include "esys/os/esysc/semaphore.h"
#else
#define ESYSOS_USE_MP 1
#include "esys/os/mp/semaphore.h"
#endif

#include "esys/os/selectplat.h"
