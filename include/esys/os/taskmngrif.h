/*!
 * \file esys/os/taskmngrif.h
 * \brief The Task Manager
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysos_defs.h"
#include "esys/os/taskbase.h"
#include "esys/os/mutexif.h"

namespace esys::os
{

class ESYSOS_API TaskMngrIf
{
public:
    TaskMngrIf();
    virtual ~TaskMngrIf();

    //! Called by a Task when it's completed
    /*! \param[in] task the Task which is completed
     */
    virtual void done(TaskBase *task) = 0;

    //! Called by a Task when it's starting
    /*! \param[in] task the Task which is started
     */
    virtual void started(TaskBase *task) = 0;

    //! Called by a Task when it's stopping
    /*! \param[in] task the Task which is stopping
     */
    virtual int32_t stopping(TaskBase *task) = 0;
};

} // namespace esys::os
