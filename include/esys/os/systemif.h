/*!
 * \file esys/os/systemif.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysos_defs.h"

namespace esys::os
{

/*! \class SystemIf esys/os/systemif.h "esys/os/systemif.h"
 *  \brief The interface for an Operating System
 */
class ESYSOS_API SystemIf
{
public:
    //! Default constructor
    SystemIf();

    //! Destructor
    virtual ~SystemIf();

    //! Initialize the Operating System
    /*!
     *  \return 0 if successful, < 0 otherwise
     */
    virtual int init() = 0;

    //! Called to run the Operating System
    /*!
     *  \return 0 if successful, < 0 otherwise
     */
    virtual int run() = 0;

    //! Release the Operating System
    /*!
     *  \return 0 if successful, < 0 otherwise
     */
    virtual int release() = 0;
};

} // namespace esys::os
