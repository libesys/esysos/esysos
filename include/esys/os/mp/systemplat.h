/*!
 * \file esys/os/mp/systemplat.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysos_defs.h"
#include "esys/os/systemif.h"

#include <memory>

namespace esys::os::mp
{

/*! \class SystemPlat esys/os/mp/systemplat.h "esys/os/mp/systemplat.h"
 *  \brief Multi-Platform Operating System
 */
class ESYSOS_API SystemPlat : public SystemIf
{
public:
    //! Default constructor
    SystemPlat();

    //! Destructor
    ~SystemPlat() override;

    int init() override;

    int run() override;

    int release() override;

private:
    std::shared_ptr<SystemIf> m_system;
};

} // namespace esys::os::mp

#ifdef ESYSOS_USE_MP
namespace esys::os
{
using namespace mp;
}
#endif
