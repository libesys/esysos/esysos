/*!
 * \file esys/os/mp/semaphore.h
 * \brief Declaration of the Multi-Platform Semaphore class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os//esysos_defs.h"
#include "esys/os/mp/semaphoreplat.h"

#include <esys/base/object.h>

namespace esys::os::mp
{

/*! \class Semaphore esys/os/mp/semaphore.h "esys/os/mp/semaphore.h"
 *  \brief Multi-Platform Semaphore class
 */
class ESYSOS_API Semaphore : public base::Object, public SemaphorePlat
{
public:
    //! Constructor
    /*!
     * \param[in] name the name of the Semaphore
     * \param[in] count the initial count of the semaphore
     */
    Semaphore(const base::ObjectName &name, int count);

    //! Destructor
    ~Semaphore() override;
};

} // namespace esys::os::mp
