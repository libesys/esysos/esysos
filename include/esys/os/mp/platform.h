/*!
 * \file esys/os/mp/platform.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysos_defs.h"
#include "esys/os/mp/platformbase.h"
#include "esys/os/mp/pluginmngr.h"

#include <map>
#include <memory>

//<swig_inc/>

namespace esys::os::mp
{

/*! \class Platform esys/os/mp/platform.h "esys/os/mp/platform.h"
 * \brief Multi-platform implementation of the Plaform class
 */
class ESYSOS_API Platform : public PlatformBase
{
public:
    //! Constructor
    Platform();

    //! Destructor
    ~Platform() override;

    int init();

    std::shared_ptr<PlatformBase> get_current();
    int set_current(const std::string &name);

    std::shared_ptr<MutexIf> new_mutex(const std::string &name, MutexIf::Type type = MutexIf::Type::DEFAULT) override;

    std::shared_ptr<SemaphoreIf> new_semaphore(const std::string &name, int count) override;

    std::shared_ptr<TaskPlatIf> new_task_plat(const std::string &name) override;

    std::shared_ptr<SystemIf> new_system() override;

    static Platform &get();

private:
    static std::shared_ptr<Platform> s_platform;

    PluginMngr m_mngr;
    std::map<std::string, std::shared_ptr<PlatformBase>> m_platform_map;
    std::shared_ptr<PlatformBase> m_current;
};

} // namespace esys::os::mp
