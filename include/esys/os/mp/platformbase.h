/*!
 * \file esys/os/mp/platformbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysos_defs.h"
#include "esys/os/mutexif.h"
#include "esys/os/semaphoreif.h"
#include "esys/os/systemif.h"
#include "esys/os/taskplatif.h"

#include <memory>
#include <string>

//<swig_inc/>

namespace esys::os::mp
{

/*! \class PlatformBase esys/os/mp/platformbase.h "esys/os/mp/platformbase.h"
 * \brief Base class of all Plaform implementation
 */
class ESYSOS_API PlatformBase
{
public:
    //! Constructor
    PlatformBase();

    //! Destructor
    virtual ~PlatformBase();

    //! Create a new Mutex
    /*!
     * \return the mutex
     */
    virtual std::shared_ptr<MutexIf> new_mutex(const std::string &name,
                                               MutexIf::Type type = MutexIf::Type::DEFAULT) = 0;

    //! Create a new Semaphore
    /*!
     * \param[in] count the initial count of the semaphore
     * \return the semaphore
     */
    virtual std::shared_ptr<SemaphoreIf> new_semaphore(const std::string &name, int count) = 0;

    //! Create a new Task Platform
    /*!
     * \return the Task Platform
     */
    virtual std::shared_ptr<TaskPlatIf> new_task_plat(const std::string &name) = 0;

    //! Create a new System Platform
    /*!
     * \return the System Platform
     */
    virtual std::shared_ptr<SystemIf> new_system() = 0;
};

} // namespace esys::os::mp
