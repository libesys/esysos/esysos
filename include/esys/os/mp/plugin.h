/*!
 * \file esys/os/mp/plugin.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysos_defs.h"
#include "esys/os/mp/platformbase.h"

#include <esys/base/pluginbase.h>

#include <memory>

#define DECLARE_ESYSOS_PLUGIN(exp) DECLARE_ESYSBASE_PLUGIN(exp, esysos, esys::os::mp::Plugin)

#define DEFINE_ESYSOS_PLUGIN(exp, class_) DEFINE_ESYSBASE_PLUGIN(exp, esysos, esys::os::mp::Plugin, class_)

//<swig_inc/>

namespace esys::os::mp
{

/*! \class Plugin esys/os/mp/plugin.h "esys/os/mp/plugin.h"
 * \brief Plugin class
 *
 */
class ESYSOS_API Plugin : public base::PluginBase
{
public:
    //! Constructor
    Plugin();

    //! Destructor
    ~Plugin() override;

    virtual std::shared_ptr<PlatformBase> get_platform() = 0;
};

using PluginEntryFunction = Plugin *(*)();

} // namespace esys::os::mp
