/*!
 * \file esys/os/mp/semaphoreplat.h
 * \brief Header of the Multi-Platform Semaphore class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysos_defs.h"
#include "esys/os/semaphoreif.h"
#include "esys/os/mp/objplatif.h"

#include <memory>
#include <string>

namespace esys::os::mp
{

/*! \class SemaphorePlat esys/os/mp/semaphoreplat.h "esys/os/mp/semaphoreplat.h"
 *  \brief Boost Semaphore class
 */
class ESYSOS_API SemaphorePlat : public SemaphoreIf, public ObjPlatIf
{
public:
    //! Constructor
    /*!
     * \param[in] count the initial count of the semaphore
     */
    SemaphorePlat(const std::string &name, int count);

    //! Destructor
    ~SemaphorePlat() override;

    void set_init_count(int count) override;
    int get_init_count() const override;

    void set_max_count(int max_count) override;
    int get_max_count() const override;

    int get_count() const override;

    int plat_init() override;
    int plat_release() override;

    int post(bool from_isr = false) override;
    int wait(bool from_isr = false) override;
    int try_wait(bool from_isr = false) override;
    int wait_for(int ms, bool from_isr = false) override;

private:
    //!< \cond DOXY_IMPL
    std::shared_ptr<SemaphoreIf> m_semaphore;
    int m_init_count = 0;
    int m_max_count = -1;
    //!< \endcond
};

} // namespace esys::os::mp
