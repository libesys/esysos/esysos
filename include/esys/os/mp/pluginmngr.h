/*!
 * \file esys/os/mp/pluginmngr.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysos_defs.h"
#include "esys/os/mp/plugin.h"

#include <esys/base/pluginmngrcore_t.h>

#include <vector>
#include <map>

//<swig_inc/>

namespace esys::os::mp
{

/*! \class PluginMngr esys/os/pluginmngr.h "esys/os/pluginmngr.h"
 * \brief Plugin manager class for esysos
 *
 */
class ESYSOS_API PluginMngr : public base::PluginMngrCore_t<Plugin>
{
public:
    using BaseType = base::PluginMngrCore_t<Plugin>;

    using BaseType::get;

    //! Constructor
    PluginMngr();

    //! Destructor
    ~PluginMngr() override;

    base::PluginBase *get_plugin_from_entry_fct(void *entry_fct) override;

protected:
    int plugin_loaded(Plugin *plugin) override;
};

} // namespace esys::os::mp
