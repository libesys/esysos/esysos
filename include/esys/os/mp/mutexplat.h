/*!
 * \file esys/os/mp/mutexplat.h
 * \brief Declaration of the Multi-Platform Mutex class
 *
 * \cond
 *__legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 *__legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysos_defs.h"
#include "esys/os/mutexif.h"
#include "esys/os/mp/objplatif.h"

#include <memory>

namespace esys::os::mp
{

/*! \class MutexPlat esys/os/mp/mutexplat.h "esys/os/mp/mutexplat.h"
 *  \brief Multi-Platform Mutex class
 */
class ESYSOS_API MutexPlat : public MutexIf, public ObjPlatIf
{
public:
    MutexPlat(const std::string &name, Type type = Type::DEFAULT);
    ~MutexPlat() override;

    void set_type(Type type) override;
    Type get_type() const override;

    int plat_init() override;
    int plat_release() override;

    int lock(bool from_isr = false) override;
    int unlock(bool from_isr = false) override;
    int try_lock(bool from_isr = false) override;

private:
    std::shared_ptr<MutexIf> m_mutex;
    Type m_type = Type::DEFAULT;
};

} // namespace esys::os::mp
