/*!
 * \file esys/os/mp/objplatif.h
 * \brief Declaration of the Multi-Platform Object platform interfaces
 *
 * \cond
 *__legal_b__
 *
 * Copyright (c) 2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 *__legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysos_defs.h"
#include "esys/os/objplatif.h"

#include <string>

namespace esys::os::mp
{

/*! \class ObjPlatIf esys/os/mp/objplatif.h "esys/os/mp/objplatif.h"
 *  \brief Multi-Platform object platform interface
 */
class ESYSOS_API ObjPlatIf : public virtual os::ObjPlatIf
{
public:
    ObjPlatIf();
    ObjPlatIf(const std::string &name);

    ~ObjPlatIf() override;

    void set_name(const char *name) override;
    const char *get_name() const override;

private:
    //!< \cond DOXY_IMPL
    std::string m_name;
    //!< \endcond
};

} // namespace esys::os::mp
