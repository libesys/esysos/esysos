/*!
 * \file esysos/version.h
 * \brief Version info for esysos
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

// Bump-up with each new version
#define ESYSOS_MAJOR_VERSION 0
#define ESYSOS_MINOR_VERSION 1
#define ESYSOS_RELEASE_NUMBER 0
#define ESYSOS_VERSION_STRING "ESysOS 0.1.0"

// Must be updated manually as well each time the version above changes
#define ESYSOS_VERSION_NUM_DOT_STRING "0.1.0"
#define ESYSOS_VERSION_NUM_STRING "0100"

// nothing should be updated below this line when updating the version

#define ESYSOS_VERSION_NUMBER (ESYSOS_MAJOR_VERSION * 1000) + (ESYSOS_MINOR_VERSION * 100) + ESYSOS_RELEASE_NUMBER
#define ESYSOS_BETA_NUMBER 1
#define ESYSOS_VERSION_FLOAT                                                               \
    ESYSOS_MAJOR_VERSION + (ESYSOS_MINOR_VERSION / 10.0) + (ESYSOS_RELEASE_NUMBER / 100.0) \
        + (ESYSOS_BETA_NUMBER / 10000.0)

// check if the current version is at least major.minor.release
#define ESYSOS_CHECK_VERSION(major, minor, release)                                                        \
    (ESYSOS_MAJOR_VERSION > (major) || (ESYSOS_MAJOR_VERSION == (major) && ESYSOS_MINOR_VERSION > (minor)) \
     || (ESYSOS_MAJOR_VERSION == (major) && ESYSOS_MINOR_VERSION == (minor) && ESYSOS_RELEASE_NUMBER >= (release)))
