/*!
 * \file esys/os/freertos/test/version.h
 * \brief Version info for esysos_freertos_t
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

// Bump-up with each new version
#define ESYSOS_FREERTOS_T_MAJOR_VERSION 0
#define ESYSOS_FREERTOS_T_MINOR_VERSION 0
#define ESYSOS_FREERTOS_T_RELEASE_NUMBER 1
#define ESYSOS_FREERTOS_T_VERSION_STRING "esysos_freertos_t 0.0.1"

// Must be updated manually as well each time the version above changes
#define ESYSOS_FREERTOS_T_VERSION_NUM_DOT_STRING "0.0.1"
#define ESYSOS_FREERTOS_T_VERSION_NUM_STRING "0001"

// nothing should be updated below this line when updating the version

#define ESYSOS_FREERTOS_T_VERSION_NUMBER                                               \
    (ESYSOS_FREERTOS_T_MAJOR_VERSION * 1000) + (ESYSOS_FREERTOS_T_MINOR_VERSION * 100) \
        + ESYSOS_FREERTOS_T_RELEASE_NUMBER
#define ESYSOS_FREERTOS_T_BETA_NUMBER 1
#define ESYSOS_FREERTOS_T_VERSION_FLOAT                                        \
    ESYSOS_FREERTOS_T_MAJOR_VERSION + (ESYSOS_FREERTOS_T_MINOR_VERSION / 10.0) \
        + (ESYSOS_FREERTOS_T_RELEASE_NUMBER / 100.0) + (ESYSOS_FREERTOS_T_BETA_NUMBER / 10000.0)

// check if the current version is at least major.minor.release
#define ESYSOS_FREERTOS_T_CHECK_VERSION(major, minor, release)                                    \
    (ESYSOS_FREERTOS_T_MAJOR_VERSION > (major)                                                    \
     || (ESYSOS_FREERTOS_T_MAJOR_VERSION == (major) && ESYSOS_FREERTOS_T_MINOR_VERSION > (minor)) \
     || (ESYSOS_FREERTOS_T_MAJOR_VERSION == (major) && ESYSOS_FREERTOS_T_MINOR_VERSION == (minor) \
         && ESYSOS_FREERTOS_T_RELEASE_NUMBER >= (release)))
