/*!
 * \file esys/os/freertos/test/fix/system.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include <esys/os/systemif.h>

#include <FreeRTOS.h>

namespace esys::os::freertos::test::fix
{

class System : public SystemIf
{
public:
    static const size_t mainREGION_1_SIZE = 2001;
    static const size_t mainREGION_2_SIZE = 18005;
    static const size_t mainREGION_3_SIZE = 1007;
    static const uint32_t AdditionalOffset = 19;

    static_assert((AdditionalOffset + mainREGION_1_SIZE + mainREGION_2_SIZE + mainREGION_3_SIZE)
                      < configTOTAL_HEAP_SIZE,
                  "Heap too small");

    System();
    ~System() override;

    int init() override;
    int run() override;
    int release() override;

private:
    uint8_t m_heap[configTOTAL_HEAP_SIZE];

    const HeapRegion_t m_heap_regions[4] = {{m_heap + 1, mainREGION_1_SIZE},
                                            {m_heap + 15 + mainREGION_1_SIZE, mainREGION_2_SIZE},
                                            {m_heap + 19 + mainREGION_1_SIZE + mainREGION_2_SIZE, mainREGION_3_SIZE},
                                            {nullptr, 0}};
};

} // namespace esys::os::freertos::test::fix
