/*!
 * \file esys/os/esysos_freertos_defs.h
 * \brief Definitions needed for esysos_freertos
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#ifdef ESYSOS_FREERTOS_EXPORTS
#define ESYSOS_FREERTOS_API __declspec(dllexport)
#elif ESYSOS_FREERTOS_USE
#define ESYSOS_FREERTOS_API __declspec(dllimport)
#else
#define ESYSOS_FREERTOS_API
#endif

#include "esys/os/freertos/autolink.h"
