/*!
 * \file esys/os/freertos/mutexplat.h
 * \brief Declaration of the FreeRTOS Mutex class
 *
 * \cond
 *__legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 *__legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/freertos/esysos_freertos_defs.h"
#include "esys/os/mutexif.h"

#ifndef ESYSOS_FREERTOS_PIMPL
#include "esys/os/freertos/mutexplatimpl.h"
#else
#include <memory>
#endif

namespace esys::os::freertos
{

#ifndef ESYSOS_FREERTOS_PIMPL
typedef MutexPlatImpl MutexPlat;

#else

class ESYSOS_FREERTOS_API MutexPlatImpl;

/*! \class MutexPlat esys/os/freertos/mutexplat.h "esys/os/freertos/mutexplat.h"
 *  \brief FreeRTOS Mutex class
 */
class ESYSOS_FREERTOS_API MutexPlat : public MutexIf
{
public:
    MutexPlat(Type type = Type::DEFAULT);
    ~MutexPlat() override;

    void set_type(Type type) override;
    Type get_type() const override;

    int plat_init() override;
    int plat_release() override;

    int lock(bool from_isr = false) override;
    int unlock(bool from_isr = false) override;
    int try_lock(bool from_isr = false) override;

private:
    std::unique_ptr<MutexPlatImpl> m_impl; //!< The PIMPL object
};

#endif

} // namespace esys::os::freertos
