/*!
 * \file esys/os/freertos/systemplat.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/freertos/esysos_freertos_defs.h"

#include <esys/os/systemif.h>

namespace esys::os::freertos
{

/*! \class SystemPlat esys/os/freertos/systemplat.h "esys/os/freertos/systemplat.h"
 *  \brief FreeRTOS PLatform Operating System
 */
class ESYSOS_FREERTOS_API SystemPlat : public SystemIf
{
public:
    //! Default constructor
    SystemPlat();

    //! Destructor
    ~SystemPlat() override;

    int init() override;

    int run() override;

    int release() override;

protected:
    int init_heap();
    int init_heap5();
};

} // namespace esys::os::freertos
