/*!
 * \file esys/os/freertos/taskplat.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/freertos/esysos_freertos_defs.h"

#include <esys/os/taskplatif.h>

#ifndef ESYSOS_FREERTOS_PIMPL
#include "esys/os/freertos/taskplatimpl.h"
#else
#include <memory>
#endif

namespace esys::os::freertos
{

#ifndef ESYSOS_FREERTOS_PIMPL
typedef TaskPlatImpl TaskPlat;

#else

class ESYSOS_FREERTOS_API TaskPlatImpl;

/*! \class TaskPlat esys/os/freertos/taskplat.h "esys/os/freertos/taskplat.h"
 *  \brief
 */
class ESYSOS_FREERTOS_API TaskPlat : public virtual TaskPlatIf
{
public:
    //! Default constructor
    TaskPlat();

    TaskPlat(const char *name);

    //! Destructor
    ~TaskPlat() override;

    int plat_init() override;
    int plat_release() override;

    int start() override;
    int stop() override;
    int kill() override;

    void sleep(uint32_t ms) override;
    void usleep(uint32_t us) override;
    void exit_system(int result = -1) override;

    void set_task_base(TaskBase *task) override;

    //! Return the PIMPL
    /*!
     * \return the PIMPL
     */
    TaskPlatImpl *get_impl();

private:
    //!< \cond DOXY_IMPL
    std::unique_ptr<TaskPlatImpl> m_impl; //!< The PIMPL
    //!< \endcond
};

#endif

} // namespace esys::os::freertos
