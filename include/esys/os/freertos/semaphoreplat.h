/*!
 * \file esys/os/freertos/semaphoreplat.h
 * \brief Declaration of the FreeRTOS Semaphore platform class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/freertos/esysos_freertos_defs.h"

#include <esys/os/semaphoreif.h>

#ifndef ESYSOS_FREERTOS_PIMPL
#include "esys/os/freertos/semaphoreplatimpl.h"
#else
#include <memory>
#endif

namespace esys::os::freertos
{

#ifndef ESYSOS_FREERTOS_PIMPL
typedef SemaphorePlatImpl SemaphorePlat;

#else
class ESYSOS_FREERTOS_API SemaphorePlatImpl;

/*! \class SemaphorePlat esys/os/freertos/semaphoreplat.h "esys/os/freertos/semaphoreplat.h"
 *  \brief FreeRTOS Semaphore platform class
 */
class ESYSOS_FREERTOS_API SemaphorePlat : public SemaphoreIf
{
public:
    //! Constructor
    /*!
     * \param[in] count the initial count of the semaphore
     */
    explicit SemaphorePlat(int count);

    SemaphorePlat(const char *name, int count);

    //! Destructor
    ~SemaphorePlat() override;

    void set_init_count(int count) override;
    int get_init_count() const override;

    void set_max_count(int max_count) override;
    int get_max_count() const override;

    int plat_init() override;
    int plat_release() override;

    int get_count() const override;

    int post(bool from_isr = false) override;
    int wait(bool from_isr = false) override;
    int try_wait(bool from_isr = false) override;
    int wait_for(int ms, bool from_isr = false) override;

private:
    //!< \cond DOXY_IMPL
    std::unique_ptr<SemaphorePlatImpl> m_impl; //!< PIMPL
    //!< \endcond
};

#endif

} // namespace esys::os::freertos
