/*!
 * \file esysos/version.h
 * \brief Version info for esysos_freertos
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

// Bump-up with each new version
#define ESYSOS_FREERTOS_MAJOR_VERSION 0
#define ESYSOS_FREERTOS_MINOR_VERSION 0
#define ESYSOS_FREERTOS_RELEASE_NUMBER 1
#define ESYSOS_FREERTOS_VERSION_STRING "esysos_freertos 0.0.1"

// Must be updated manually as well each time the version above changes
#define ESYSOS_FREERTOS_VERSION_NUM_DOT_STRING "0.0.1"
#define ESYSOS_FREERTOS_VERSION_NUM_STRING "0001"

// nothing should be updated below this line when updating the version

#define ESYSOS_FREERTOS_VERSION_NUMBER \
    (ESYSOS_FREERTOS_MAJOR_VERSION * 1000) + (ESYSOS_FREERTOS_MINOR_VERSION * 100) + ESYSOS_FREERTOS_RELEASE_NUMBER
#define ESYSOS_FREERTOS_BETA_NUMBER 1
#define ESYSOS_FREERTOS_VERSION_FLOAT                                                                                 \
    ESYSOS_FREERTOS_MAJOR_VERSION + (ESYSOS_FREERTOS_MINOR_VERSION / 10.0) + (ESYSOS_FREERTOS_RELEASE_NUMBER / 100.0) \
        + (ESYSOS_FREERTOS_BETA_NUMBER / 10000.0)

// check if the current version is at least major.minor.release
#define ESYSOS_FREERTOS_CHECK_VERSION(major, minor, release)                                  \
    (ESYSOS_FREERTOS_MAJOR_VERSION > (major)                                                  \
     || (ESYSOS_FREERTOS_MAJOR_VERSION == (major) && ESYSOS_FREERTOS_MINOR_VERSION > (minor)) \
     || (ESYSOS_FREERTOS_MAJOR_VERSION == (major) && ESYSOS_FREERTOS_MINOR_VERSION == (minor) \
         && ESYSOS_FREERTOS_RELEASE_NUMBER >= (release)))
