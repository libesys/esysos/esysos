/*!
 * \file esys/os/freertos/mp/plugin.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/freertos/esysos_freertos_defs.h"

#include <esys/os/mp/plugin.h>

#include <esys/base/plugin_t.h>

namespace esys::os::freertos::mp
{

class ESYSOS_FREERTOS_API Plugin : public esys::base::Plugin_t<Plugin, os::mp::Plugin>
{
public:
    using BaseType = esys::base::Plugin_t<Plugin, os::mp::Plugin>;

    Plugin();
    ~Plugin() override;

    std::shared_ptr<os::mp::PlatformBase> get_platform() override;
};

} // namespace esys::os::freertos::mp

DECLARE_ESYSOS_PLUGIN(ESYSOS_FREERTOS_API);
