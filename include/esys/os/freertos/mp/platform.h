/*!
 * \file esys/os/freertos/mp/platform.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/freertos/esysos_freertos_defs.h"
#include "esys/os/mp/platformbase.h"

#include <memory>

//<swig_inc/>

namespace esys::os::freertos::mp
{

/*! \class Platform esys/os/freertos/mp/platform.h "esys/os/freertos/mp/platform.h"
 * \brief FreeRTOS implementation of the Platform
 */
class ESYSOS_FREERTOS_API Platform : public os::mp::PlatformBase
{
public:
    //! Constructor
    Platform();

    //! Destructor
    ~Platform() override;

    std::shared_ptr<MutexIf> new_mutex(const std::string &name, MutexIf::Type type = MutexIf::Type::DEFAULT) override;

    std::shared_ptr<SemaphoreIf> new_semaphore(const std::string &name, int count) override;

    std::shared_ptr<TaskPlatIf> new_task_plat(const std::string &name) override;

    std::shared_ptr<SystemIf> new_system() override;
};

} // namespace esys::os::freertos::mp
