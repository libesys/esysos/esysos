/*!
 * \file esys/os/taskplatif.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysos_defs.h"
#include "esys/os/objplatif.h"

#include <inttypes.h>

namespace esys::os
{

class ESYSOS_API TaskBase;

/*! \class TaskPlatIf esys/os/taskplatif.h "esys/os/taskplatif.h"
 *  \brief Defines the platform dependent API for a Task
 */
class ESYSOS_API TaskPlatIf : public virtual os::ObjPlatIf
{
public:
    //! Constructor
    TaskPlatIf();

    //! Destructor
    ~TaskPlatIf() override;

    //! Starts the Task
    /*!
     *  \return 0 if successful, < 0 otherwise
     */
    virtual int start() = 0;

    //! Stops the Task
    /*!
     *  \return 0 if successful, < 0 otherwise
     */
    virtual int stop() = 0;

    //! Kills the Task
    /*!
     *  \return 0 if successful, < 0 otherwise
     */
    virtual int kill() = 0;

    //! Sleep for a given number of milliseconds
    /*!
     *  \param[in] ms the number of milliseconds
     */
    virtual void sleep(uint32_t ms) = 0;

    //! Sleep for a given number of microseconds
    /*!
     *  \param[in] us the number of microseconds
     */
    virtual void usleep(uint32_t us) = 0;

    //! Set the task used
    /*! This is only needed to support the Multi Platform
     *  \param[in] task the task used
     */
    virtual void set_task_base(os::TaskBase *task) = 0;

    virtual void exit_system(int result = -1) = 0;

    //! Return the maximum stack size from boot up to when called
    /*!
     *  \return maximum size in bytes of the stack since boot
     */
    virtual int get_max_runtime_stack_size();

    //! Return the minimum remaining stack size from boot up to when called
    /*!
     *  \return minimum remaining size in bytes of the stack since boot
     */
    virtual int get_min_remaining_runtime_stack_size();
};

} // namespace esys::os
