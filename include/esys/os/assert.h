/*!
 * \file esys/os/assert.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#ifdef NDEBUG
#ifdef assert
#undef assert
#endif
#define assert(cond)
#else

#ifdef _MSC_VER
#include <cassert>
#else
#include <assert.h>
#endif

#endif

#ifdef __cplusplus

namespace esys::os
{

template<unsigned int SMALL, unsigned int BIG>
class StaticAssertLE
{
public:
    static constexpr unsigned int m_small = SMALL;
    static constexpr unsigned int m_big = BIG;

    static constexpr bool m_value = (m_small <= m_big);

    static_assert(m_value, "Too small");
};

} // namespace esys::os

#define ESYSOS_STATIC_ASSERT_LE(SS, BB, MSG_) static_assert(esys::os::StaticAssertLE<SS, BB>::m_value, MSG_);

#endif
