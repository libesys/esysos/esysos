/*!
 * \file esys/os/config.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#if defined __has_include
#if __has_include(<esys/os/configdefs.h>)
#include <esys/os/configdefs.h>
#else
#include "esys/os/configdefs_dft.h"
#endif
#endif

#ifdef WIN32
#define ESYSOS_VHW 1
#ifdef ESYSOS_HW
#undef ESYSOS_HW
#endif
#endif
