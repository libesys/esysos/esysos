/*!
 * \file esys/os/semaphoreif.h
 * \brief he Semaphore virtual interface declaration
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysos_defs.h"
#include "esys/os/objplatif.h"

namespace esys::os
{

/*! \class SemaphoreIf esys/os/semaphoreif.h "esys/os/semaphoreif.h"
 *  \brief Base class of all Semaphore implementation
 */
class ESYSOS_API SemaphoreIf : public virtual ObjPlatIf
{
public:
    //! Default constructor
    SemaphoreIf();

    //! Destructor
    ~SemaphoreIf() override;

    //! Set the semaphore init count
    /*!
     * \param[in] count the semaphore init count
     */
    virtual void set_init_count(int init_count) = 0;

    //! Get the semaphore init count
    /*!
     * \return the semaphore init count
     */
    virtual int get_init_count() const = 0;

    //! Set the semaphore maximum count
    /*!
     * \param[in] count the semaphore maximum count
     */
    virtual void set_max_count(int max_count) = 0;

    //! Get the semaphore maximum count
    /*!
     * \return the semaphore maximum count
     */
    virtual int get_max_count() const = 0;

    //! Returns the semaphore count
    /*!
     * \return the semaphore count
     */
    virtual int get_count() const = 0;

    //! Increases the semaphore counter
    /*!
     * \param[in] from_isr true indicates that the call is made from interrupt context
     * \return 0 if successful, < 0 otherwise
     */
    virtual int post(bool from_isr = false) = 0;

    //! Wait on the semaphore
    /*!
     * \param[in] from_isr true indicates that the call is made from interrupt context
     * \return 0 if successful, < 0 otherwise
     */
    virtual int wait(bool from_isr = false) = 0;

    //! Wait on the semaphore for a given amount of time
    /*!
     * \param[in] ms the time to wait in ms
     * \param[in] from_isr true indicates that the call is made from interrupt context
     * \return 0 if successful, < 0 otherwise
     */
    virtual int wait_for(int ms, bool from_isr = false) = 0;

    //! Same functionality as Wait on the semaphore, but never actually wait
    /*!
     * \param[in] from_isr true indicates that the call is made from interrupt context
     * \return 0 if successful, < 0 otherwise
     */
    virtual int try_wait(bool from_isr = false) = 0;
};

} // namespace esys::os
