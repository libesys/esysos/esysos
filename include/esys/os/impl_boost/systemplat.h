/*!
 * \file esys/os/impl_boost/systemplat.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysos_defs.h"
#include "esys/os/systemif.h"

namespace esys::os::impl_boost
{

/*! \class SystemPlat esys/os/impl_boost/systemplat.h "esys/os/impl_boost/systemplat.h"
 *  \brief Boost Platform Operating System
 */
class ESYSOS_API SystemPlat : public SystemIf
{
public:
    //! Default constructor
    SystemPlat();

    //! Destructor
    ~SystemPlat() override;

    int init() override;

    int run() override;

    int release() override;
};

} // namespace esys::os::impl_boost
