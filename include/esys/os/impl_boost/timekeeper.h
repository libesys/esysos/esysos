/*!
 * \file esys/os/impl_boost/timekeeper.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysos_defs.h"
#include "esys/os/impl_boost/timeinfo.h"

#include <boost/thread.hpp>

#include <map>
#include <memory>
#include <thread>

namespace esys::os::impl_boost
{

/*! \class TimeKeeper esys/os/impl_boost/timekeeper.h "esys/os/impl_boost/timekeeper.h"
 *  \brief
 */
class ESYSOS_API TimeKeeper
{
public:
    TimeKeeper();

    ~TimeKeeper();

    static std::shared_ptr<TimeInfo> get_current();
    static void register_time_info(boost::thread::id thread_id, std::shared_ptr<TimeInfo> time_info);

    static std::size_t size();
    static std::shared_ptr<TimeInfo> get(int idx);

private:
    using ThreadIdMap = std::map<boost::thread::id, std::shared_ptr<TimeInfo>>;

    static ThreadIdMap s_thread_id_map;
    static boost::mutex s_data_mutex;
};

} // namespace esys::os::impl_boost
