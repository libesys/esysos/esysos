/*!
 * \file esys/os/impl_boost/taskplatregistry.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysos_defs.h"
#include "esys/os/taskplatif.h"

#include <boost/thread.hpp>

#include <map>
#include <memory>
#include <vector>

namespace esys::os::impl_boost
{

class ESYSOS_API TaskPlat;

/*! \class TaskPlatRegistry esys/os/impl_boost/taskplatregistry.h "esys/os/impl_boost/taskplatregistry.h"
 *  \brief
 */
class ESYSOS_API TaskPlatRegistry
{
public:
    TaskPlatRegistry();

    virtual ~TaskPlatRegistry();

    static TaskPlatRegistry &get();

    TaskPlat *get_current_task_plat();
    void register_task(TaskPlat *task);
    void register_task(boost::thread::id thread_id, TaskPlat *task);

    std::size_t get_size();
    TaskPlat *get_task_plat(int idx);

    void start_all();
    void wait_all_done();
    void clear();

private:
    struct Helper
    {
        TaskPlat *m_task_plat = nullptr;
    };

    static std::unique_ptr<TaskPlatRegistry> s_registry;

    using ThreadIdMap = std::map<boost::thread::id, std::shared_ptr<Helper>>;
    using TaskPlatHelperVec = std::vector<std::shared_ptr<Helper>>;

    ThreadIdMap m_thread_id_map;
    TaskPlatHelperVec m_task_plat_helpers;
    std::vector<TaskPlat *> m_task_plats;
    boost::mutex m_data_mutex;
};

} // namespace esys::os::impl_boost
