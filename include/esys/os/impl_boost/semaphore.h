/*!
 * \file esys/os/impl_boost/semaphore.h
 * \brief Declaration of the Boost Semaphore class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os//esysos_defs.h"
#include "esys/os/impl_boost/semaphoreplat.h"

#include <esys/base/object.h>

namespace esys::os::impl_boost
{

/*! \class Semaphore esys/os/impl_boost/semaphore.h "esys/os/impl_boost/semaphore.h"
 *  \brief Boost Semaphore class
 */
class ESYSOS_API Semaphore : public base::Object, public SemaphorePlat
{
public:
    //! Constructor
    /*!
     * \param[in] name the name of the Semaphore
     * \param[in] count the initial count of the semaphore
     */
    Semaphore(const base::ObjectName &name, int count);

    //! Destructor
    ~Semaphore() override;
};

} // namespace esys::os::impl_boost
