/*!
 * \file esys/os/impl_boost/timeinfo.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysos_defs.h"

#include <boost/date_time/posix_time/posix_time.hpp>

#include <cstdint>
#include <map>

namespace esys::os::impl_boost
{

/*! \class TimeInfo esys/os/impl_boost/timeinfo.h "esys/os/impl_boost/timeinfo.h"
 *  \brief
 */
class ESYSOS_API TimeInfo
{
public:
    TimeInfo();

    ~TimeInfo();

    const boost::posix_time::ptime &get_boost_started_time() const;

    uint32_t millis();
    uint64_t micros();
    uint32_t micros_low();

private:
    boost::posix_time::ptime m_boost_started_time;
};

} // namespace esys::os::impl_boost
