/*!
 * \file esys/os/mutexif.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysos_defs.h"
#include "esys/os/objplatif.h"

namespace esys::os
{

/*! \class MutexIf esys/os/mutexif.h "esys/os/mutexif.h"
 *  \brief The base class of all mutex implementation
 */
class ESYSOS_API MutexIf : public virtual ObjPlatIf
{
public:
    enum class Type
    {
        DEFAULT,  //!< Standard Mutex
        RECURSIVE //!< Recursive Mutex
    };

    //! Default constructor
    MutexIf();

    //! Destructor
    ~MutexIf() override;

    //! Set the type of the mutex
    /*!
     *  \param[in] type the type of the mutex
     */
    virtual void set_type(Type type) = 0;

    //! Get the type of the Mutex
    /*!
     *  \return the type of the Mutex
     */
    virtual Type get_type() const = 0;

    //! Lock the Mutex
    /*!
     *  \return 0 if success, otherwise < 0
     */
    virtual int lock(bool from_isr = false) = 0;

    //! Unlock the Mutex
    /*!
     *  \return 0 if success, otherwise < 0
     */
    virtual int unlock(bool from_isr = false) = 0;

    //! Try locking the Mutex
    /*!
     *  \return 0 if success, otherwise < 0
     */
    virtual int try_lock(bool from_isr = false) = 0;
};

} // namespace esys::os
