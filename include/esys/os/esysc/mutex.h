/*!
 * \file esys/os/esysc/mutex.h
 * \brief Declaration of the ESysC/SystemC Mutex class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysc/esysos_esysc_defs.h"
#include "esys/os/esysc/mutexplat.h"

#include <esys/base/object.h>

namespace esys::os::esysc
{

/*! \class Mutex esys/os/esysc/mutex.h "esys/os/esysc/mutex.h"
 *  \brief ESysC/SystemC Mutex class
 */
class ESYSOS_ESYSC_API Mutex : public base::Object, public MutexPlat
{
public:
    //! Constructor
    /*!
     * \param[in] name the name of the Mutex
     * \param[in] count the initial count of the semaphore
     */
    Mutex(const base::ObjectName &name, Type type = Type::DEFAULT);

    //! Destructor
    ~Mutex() override;
};

} // namespace esys::os::esysc
