/*!
 * \file esys/os/esysc/time.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysc/esysos_esysc_defs.h"

#include <esys/os/timebase.h>

namespace esys::os::esysc
{

/*! \class Time esys/os/esysc/time.h "esys/os/esysc/time.h"
 *  \brief
 */
class ESYSOS_ESYSC_API Time : public TimeBase
{
public:
    static uint32_t millis(Source source = DEFAULT_CLK);
    static uint64_t micros(Source source = DEFAULT_CLK);
    static uint32_t micros_low();

    static void sleep(uint32_t ms);
    static void usleep(uint32_t us);

    static int32_t set_time(Struct &time);
    static void get_time(Struct &time);
    static int32_t set_date(Date &date);
    static void get_date(Date &date);
    static int32_t set_date_time(DateTime &date_time);
    static void get_date_time(DateTime &date_time);

    static int32_t start_timestamp();
    static int32_t stop_timestamp();
};

} // namespace esys::os::esysc
