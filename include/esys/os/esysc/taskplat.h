/*!
 * \file esys/os/esysc/taskplat.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysc/esysos_esysc_defs.h"
#include "esys/os/taskplatif.h"
#include "esys/os/esysc/objplatif.h"

#include <memory>
#include <string>

namespace esys::os::esysc
{

class ESYSOS_ESYSC_API TaskPlatImpl;

/*! \class TaskPlat esys/os/esysc/taskplat.h "esys/os/esysc/taskplat.h"
 *  \brief
 */
class ESYSOS_ESYSC_API TaskPlat : public virtual TaskPlatIf, public ObjPlatIf
{
public:
    //! Default constructor
    TaskPlat();

    TaskPlat(const std::string &name);

    //! Destructor
    ~TaskPlat() override;

    int plat_init() override;
    int plat_release() override;

    int start() override;
    int stop() override;
    int kill() override;

    void sleep(uint32_t ms) override;
    void usleep(uint32_t us) override;

    void set_task_base(TaskBase *task) override;
    void exit_system(int result = -1) override;

    //! Return the PIMPL
    /*!
     * \return the PIMPL
     */
    TaskPlatImpl *get_impl();

private:
    //!< \cond DOXY_IMPL
    std::unique_ptr<TaskPlatImpl> m_impl; //!< The PIMPL
    TaskBase *m_task_base = nullptr;
    //!< \endcond
};

} // namespace esys::os::esysc
