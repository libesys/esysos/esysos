/*!
 * \file esys/os/esysc/test/fix/system.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include <esys/os/systemif.h>

#include <systemc.h>
#include <sysc/sc_simulation.h>

namespace esys::os::esysc::test::fix
{

class System : public SystemIf, public sc_simulation
{
public:
    System();
    ~System() override;

    int init() override;
    int run() override;
    int release() override;

    int main() override;

private:
};

} // namespace esys::os::esysc::test::fix
