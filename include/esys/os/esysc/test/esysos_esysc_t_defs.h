/*!
 * \file esysos/esysos_esysc_t_defs.h
 * \brief Definitions needed for esysos_esysc_t
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#ifdef ESYSOS_ESYSC_T_EXPORTS
#define ESYSOS_ESYSC_T_API __declspec(dllexport)
#elif ESYSOS_ESYSC_T_USE
#define ESYSOS_ESYSC_T_API __declspec(dllimport)
#else
#define ESYSOS_ESYSC_T_API
#endif
