/*!
 * \file esys/os/esysc/mutexplat.h
 * \brief Declaration of the ESysC/SystemC Mutex class
 *
 * \cond
 *__legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 *__legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysos_defs.h"
#include "esys/os/mutexif.h"
#include "esys/os/esysc/objplatif.h"

#include <memory>

namespace esys::os::esysc
{

class ESYSOS_ESYSC_API MutexPlatImpl;

/*! \class MutexPlat esys/os/esysc/mutexplat.h "esys/os/esysc/mutexplat.h"
 *  \brief Boost Mutex class
 */
class ESYSOS_ESYSC_API MutexPlat : public MutexIf, public ObjPlatIf
{
public:
    MutexPlat(const std::string &name, Type type = Type::DEFAULT);
    ~MutexPlat() override;

    void set_type(Type type) override;
    Type get_type() const override;

    int plat_init() override;
    int plat_release() override;

    int lock(bool from_isr = false) override;
    int unlock(bool from_isr = false) override;
    int try_lock(bool from_isr = false) override;

private:
    std::unique_ptr<MutexPlatImpl> m_impl; //!< The PIMPL object
};

} // namespace esys::os::esysc
