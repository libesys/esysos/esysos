/*!
 * \file esys/os/esysc/mp/plugin.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysc/esysos_esysc_defs.h"

#include <esys/os/mp/plugin.h>

#include <esys/base/plugin_t.h>

namespace esys::os::esysc::mp
{

class ESYSOS_ESYSC_API Plugin : public esys::base::Plugin_t<Plugin, os::mp::Plugin>
{
public:
    using BaseType = esys::base::Plugin_t<Plugin, os::mp::Plugin>;

    Plugin();
    ~Plugin() override;

    std::shared_ptr<os::mp::PlatformBase> get_platform() override;
};

} // namespace esys::os::esysc::mp

DECLARE_ESYSOS_PLUGIN(ESYSOS_ESYSC_API);
