/*!
 * \file esys/os/esysc/esysos_esysc_defs.h
 * \brief Definitions needed for esysos_esysc
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#ifdef ESYSOS_ESYSC_EXPORTS
#define ESYSOS_ESYSC_API __declspec(dllexport)
#elif ESYSOS_ESYSC_USE
#define ESYSOS_ESYSC_API __declspec(dllimport)
#else
#define ESYSOS_ESYSC_API
#endif

#include "esys/os/esysc/autolink.h"
