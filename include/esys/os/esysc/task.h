/*!
 * \file esys/os/esysc/task.h
 * \brief Declaration of the ESysC/SystemC Task class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysc/esysos_esysc_defs.h"
#include "esys/os/esysc/taskplat.h"

#include <esys/os/taskbase.h>

#include <esys/base/object.h>

namespace esys::os::esysc
{

/*! \class Task esys/os/esysc/task.h "esys/os/esysc/task.h"
 *  \brief ESysC/SystemC Task class
 */
class ESYSOS_ESYSC_API Task : public base::Object, public TaskBase, public TaskPlat
{
public:
    //! Constructor
    /*!
     * \param[in] name the name of the Semaphore
     * \param[in] count the initial count of the semaphore
     */
    Task(const base::ObjectName &name);

    //! Destructor
    ~Task() override;
};

} // namespace esys::os::esysc
