/*!
 * \file esys/os/esysc/systemplat.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysc/esysos_esysc_defs.h"

#include <esys/os/systemif.h>

#include <memory>

namespace esys::os::esysc
{

class ESYSOS_ESYSC_API SystemPlatImpl;

/*! \class SystemPlat esys/os/esysc/systemplat.h "esys/os/esysc/systemplat.h"
 *  \brief ESysC/SystemC Operating System
 */
class ESYSOS_ESYSC_API SystemPlat : public SystemIf
{
public:
    //! Default constructor
    SystemPlat();

    //! Destructor
    ~SystemPlat() override;

    int init() override;

    int run() override;

    int release() override;

private:
    std::unique_ptr<SystemPlatImpl> m_impl;
};

} // namespace esys::os::esysc
