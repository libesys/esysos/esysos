/*!
 * \file esys/os/selectplat.h
 * \brief The Semaphore Platform
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#if defined(ESYSOS_DFLT_MP)
namespace esys::os
{
using namespace mp;
}
#elif defined(ESYSOS_DFLT_BOOST)
namespace esys::os
{
using namespace impl_boost;
}
#elif defined(ESYSOS_DFLT_FREERTOS)
namespace esys::os
{
using namespace freertos;
}
#elif defined(ESYSOS_DFLT_ESYSC)
namespace esys::os
{
using namespace esysc;
}
#else
namespace esys::os
{
using namespace mp;
}
#endif
