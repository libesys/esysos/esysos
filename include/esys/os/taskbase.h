/*!
 * \file esys/os/taskbase.h
 * \brief The Task
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysos_defs.h"
#include "esys/os/taskkind.h"
#include "esys/os/taskplatif.h"

namespace esys::os
{

class ESYSOS_API TaskMngrIf;

/*! \class TaskBase esys/os/taskbase.h "esys/os/taskbase.h"
 *  \brief Base class of all Task implementations
 */
class ESYSOS_API TaskBase : public virtual TaskPlatIf
{
public:
    static const uint16_t DFT_STACK_SIZE = 512; // in bytes

    enum class Priority : uint8_t
    {
        LOWEST0,
        LOWEST1,
        LOWEST2,
        LOWEST3,
        LOW0,
        LOW1,
        LOW2,
        LOW3,
        HIGH0,
        HIGH1,
        HIGH2,
        HIGH3,
        HIGHEST0,
        HIGHEST1,
        HIGHEST2,
        HIGHEST3,
        CRITICAL0,
        CRITICAL1,
        CRITICAL2,
        CRITICAL3,
    };

    enum class State : uint8_t
    {
        INSTANCIATED = 0,
        INITILIAZED,
        STARTED,
        STOPPING,
        STOPPED
    };

    //! Constructor
    TaskBase();

    //! Destructor
    ~TaskBase() override;

    //! The actual code run by the Task
    /*!
     *  \return 0 if successful, <0 otherwise
     */
    virtual int entry() = 0;

    //! Set the size of the stack allocated for the Task
    /*!
     *  \param[in] stack_size the number of bytes allocated for the stack
     */
    void set_stack_size(unsigned int stack_size);

    //! Returns the size of the stack allocated for the Task
    /*!
     *  \return the number of bytes allocated for the stack
     */
    unsigned int get_stack_size() const;

    //! Set the priority of the Task
    /*!
     *  \param[in] priority the Task priority
     */
    void set_priority(Priority priority);

    //! Returns the priority of the Task
    /*!
     *  \return the Task priority
     */
    Priority get_priority() const;

    //! Set the kind of Task
    /*!
     *  \param[in] kind the kind of Task
     */
    void set_kind(TaskKind kind);

    //! Get the kind of Task
    /*!
     *  \return the kind of Task
     */
    TaskKind get_kind() const;

    //! Set the state of Task
    /*!
     *  \param[in] state the state of Task
     */
    void set_state(State state);

    //! Get the state of Task
    /*!
     *  \param[in] state the state of Task
     */
    State get_state() const;

    //! Set the next Task handled by the same TaskMngr
    /*!
     * \param[in] task the next Task
     */
    void set_next(TaskBase *task);

    //! Returns the next Task handled by the same Task Manager
    /*!
     *  \return the next Task
     */
    TaskBase *get_next() const;

    //! Set the previous Task handled by the same TaskMngr
    /*!
     * \param[in] task the previous Task
     */
    void set_prev(TaskBase *task);

    //! Returns the previous Task handled by the same Task Manager
    /*!
     *  \return the previous Task
     */
    TaskBase *get_prev() const;

    //! Set the exit value of the Task
    /*!
     * \param[in] task the next Task
     */
    void set_exit(int exit_value);

    //! Returns the exit value of a Task
    /*!
     *  \return the exit value <0 if an error occured, otherwise success
     */
    int get_exit() const;

    //! Set the Task Manager handling this Task
    /*!
     * \param[in] mngr the Task Manager
     */
    void set_mngr_if(TaskMngrIf *mngr);

    //! Returns the Task Manager handling this Task
    /*!
     *  \return the Task Manager
     */
    TaskMngrIf *get_mngr_if() const;

    virtual void done();
    virtual void started();

private:
    //!< \cond DOXY_IMPL
    TaskBase *m_next = nullptr;              //!< The next Task
    TaskBase *m_prev = nullptr;              //!< The previous Task
    int m_exit_value = 0;                    //!< The value returned by the Task when completed
    uint16_t m_stack_size = DFT_STACK_SIZE;  //!< The size in bytes of the stack of the Task
    Priority m_priority = Priority::LOW0;    //!< Priority of the Task
    TaskMngrIf *m_mngr_if = nullptr;         //!< The Task Manager interface handling this Task
    TaskKind m_kind = TaskKind::APPLICATION; //!< The type of the Task: Application or Internal
    State m_state = State::INSTANCIATED;     //!< The state of the Task
    //!< \endcond
};

} // namespace esys::os
