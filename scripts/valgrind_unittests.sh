#!/bin/bash

echo "${TXT_S}Valgrind unit test starts ...${TXT_CLEAR}"

source scripts/start_ssh_agent

cd build_dev
mkdir -p build/cmake
cd build
cd cmake

make esysos_t-valgrind-html -j`nproc --all`
make esysos_freertos_t-valgrind-html -j`nproc --all`
make esysos_esysc_t-valgrind-html -j`nproc --all`

mkdir -p ../../../public/logs
mkdir -p ../../../public/esysos/valgrind
mkdir -p ../../../public/esysos_freertos/valgrind
mkdir -p ../../../public/esysos_esysc/valgrind

# Copy all raw reports to folder logs
cp src/esysos/src/esys/os/*_junit.txt ../../../public/logs
cp src/esysos/src/esys/os/*-valgrind-out.* ../../../public/logs

# Copy processed reports for esysos_t
cp src/esysos/src/esys/os/esysos_t-valgrind-out.* ../../../public/esysos/valgrind
cp -R src/esysos/src/esys/os/test/valgrind_html/* ../../../public/esysos/valgrind

# Copy processed reports for esysos_freertos_t
cp src/esysos/src/esys/os/esysos_freertos_t-valgrind-out.* ../../../public/esysos_freertos/valgrind
cp -R src/esysos/src/esys/os/freertos/test/valgrind_html/* ../../../public/esysos_freertos/valgrind

# Copy processed reports for esysos_esys_t
cp src/esysos/src/esys/os/esysos_esysc_t-valgrind-out.* ../../../public/esysos_esysc/valgrind
cp -R src/esysos/src/esys/os/esysc/test/valgrind_html/* ../../../public/esysos_esysc/valgrind

echo "${TXT_S}Valgrind unit test done.${TXT_CLEAR}"
