#!/bin/bash

echo "${TXT_S}Build ...${TXT_CLEAR}"
echo "pwd = "`pwd`

cd build_dev
mkdir build
cd build
mkdir cmake
cd cmake
pwd

cmake ../.. -DESYSOS_BUILD_DOC=On -DESYSOS_COVERAGE=On -DESYSOS_BUILD_UTS=On -DCMAKE_BUILD_TYPE=Debug
if [ ! $? -eq 0 ]; then
   echo "${TXT_E}Build failed: cmake failed.${TXT_CLEAR}"
   exit 1
fi

echo "    ${TXT_S}Build esysos_t ...${TXT_CLEAR}"
make esysos_t -j`nproc --all`
if [ ! $? -eq 0 ]; then
   echo "${TXT_E}Cmake failed.${TXT_CLEAR}"
   exit 1
fi
echo "    ${TXT_S}Build esysos_t done.${TXT_CLEAR}"

echo "    ${TXT_S}Build esysos_freertos_t ...${TXT_CLEAR}"
make esysos_freertos_t -j`nproc --all`
if [ ! $? -eq 0 ]; then
   echo "${TXT_E}Cmake failed.${TXT_CLEAR}"
   exit 1
fi
echo "    ${TXT_S}Build esysos_freertos_t done.${TXT_CLEAR}"

echo "    ${TXT_S}Build esysos_esysc_t ...${TXT_CLEAR}"
make esysos_esysc_t -j`nproc --all`
if [ ! $? -eq 0 ]; then
   echo "${TXT_E}Cmake failed.${TXT_CLEAR}"
   exit 1
fi
echo "    ${TXT_S}Build esysos_esysc_t done.${TXT_CLEAR}"

echo "    ${TXT_S}Build esysos_doc ...${TXT_CLEAR}"
make esysos_doc -j`nproc --all`
if [ ! $? -eq 0 ]; then
   echo "${TXT_E}Build esysos_doc failed.${TXT_CLEAR}"
   exit 1
fi
echo "    ${TXT_S}Build esysos_doc done.${TXT_CLEAR}"

mkdir -p ../../../public/cpp_api
cp -R src/esysos/doc/html ../../../public/cpp_api

echo "pwd = "`pwd`
echo "${TXT_S}Build done.${TXT_CLEAR}"
