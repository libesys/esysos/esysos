#!/bin/bash

echo "${TXT_S}Run cland-tidy ...${TXT_CLEAR}"

source scripts/start_ssh_agent

cd build_dev
cd build
cd cmake

mkdir -p ../../../public/clang_tidy

make esysos-tidy-html -j`nproc --all`
RESULT_ESYSOS_TIDY=$?

cp -R src/esysos/src/esys/os/esysos-clang_tidy.html ../../../public/clang_tidy
cp -R src/esysos/src/esys/os/esysos-clang_tidy.log ../../../public/clang_tidy

if [ ! ${RESULT_ESYSOS_TIDY} -eq 0 ]; then
   echo "${TXT_W}Clang-tidy for eysos failed.${TXT_CLEAR}"
fi

make esysos_freertos-tidy-html -j`nproc --all`
RESULT_ESYSOS_FREERTOS_TIDY=$?

cp -R src/esysos/src/esys/os/freertos/esysos_freertos-clang_tidy.html ../../../public/clang_tidy
cp -R src/esysos/src/esys/os/freertos/esysos_freertos-clang_tidy.log ../../../public/clang_tidy

if [ ! ${RESULT_ESYSOS_FREERTOS_TIDY} -eq 0 ]; then
   echo "${TXT_W}Clang-tidy for eysos_freertos failed.${TXT_CLEAR}"
fi

make esysos_esysc-tidy-html -j`nproc --all`
RESULT_ESYSOS_ESYSC_TIDY=$?

cp -R src/esysos/src/esys/os/esysc/esysos_esysc-clang_tidy.html ../../../public/clang_tidy
cp -R src/esysos/src/esys/os/esysc/esysos_esysc-clang_tidy.log ../../../public/clang_tidy

if [ ! ${RESULT_ESYSOS_ESYSC_TIDY} -eq 0 ]; then
   echo "${TXT_W}Clang-tidy for eysos_esysc failed.${TXT_CLEAR}"
fi

echo "${TXT_S}Run cland-tidy done.${TXT_CLEAR}"
