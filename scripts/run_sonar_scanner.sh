#!/bin/bash

echo "${TXT_S}Run SonarCloud scanner ...${TXT_CLEAR}"

cd build_dev/src/esysos

if [ -z "${SKIP_SONAR_CLOUD}" ]; then
    if [ -z "${SKIP_CODE_COVERAGE}" ]; then
        ../../../sonar-scanner/bin/sonar-scanner \
            -Dsonar.host.url="${SONAR_HOST_URL}" \
            -Dsonar.token="${SONAR_TOKEN}" \
            -Dsonar.cfamily.compile-commands=../../build/cmake/compile_commands.json \
            -Dsonar.coverageReportPaths=../../build/cmake/esysos_coverage_sonarqube.xml,../../build/cmake/esysos_freertos_coverage_sonarqube.xml
    else
        ../../../sonar-scanner/bin/sonar-scanner \
            -Dsonar.host.url="${SONAR_HOST_URL}" \
            -Dsonar.token="${SONAR_TOKEN}" \
            -Dsonar.cfamily.compile-commands=../../build/cmake/compile_commands.json
    fi
fi

echo "${TXT_S}Run SonarCloud scanner done.${TXT_CLEAR}"
