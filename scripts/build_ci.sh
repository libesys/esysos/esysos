#!/bin/bash

echo "${TXT_S}Build ...${TXT_CLEAR}"
echo "pwd = "`pwd`

cd build_dev
mkdir build
cd build
mkdir cmake
cd cmake
pwd

if [ -z "${SKIP_CODE_COVERAGE}" ]; then
   cmake ../.. -DESYSOS_COVERAGE=On -DESYSOS_BUILD_UTS=On -DCMAKE_BUILD_TYPE=Debug
else
   cmake ../.. -DESYSOS_BUILD_UTS=On -DCMAKE_BUILD_TYPE=Debug
fi

if [ ! $? -eq 0 ]; then
   echo "${TXT_E}Build failed: cmake failed.${TXT_CLEAR}"
   exit 1
fi

if [ "$(uname)" == "Darwin" ]; then
   N=`sysctl -n hw.logicalcpu`
else
   N=`nproc --all`
fi

make esysos_t -j$N
if [ ! $? -eq 0 ]; then
   echo "${TXT_E}Build failed: esysrepo_t.${TXT_CLEAR}"
   exit 1
fi

make esysos_freertos_t -j$N
if [ ! $? -eq 0 ]; then
   echo "${TXT_E}Build failed: esysrepo_freertos_t.${TXT_CLEAR}"
   exit 1
fi

make esysos_esysc_t -j$N
if [ ! $? -eq 0 ]; then
   echo "${TXT_E}Build failed: esysrepo_freertos_t.${TXT_CLEAR}"
   exit 1
fi

echo "pwd = "`pwd`
echo "${TXT_S}Build done.${TXT_CLEAR}"
