#!/bin/bash

echo "${TXT_S}Run code coverage on unit tests ...${TXT_CLEAR}"

source scripts/start_ssh_agent

cd build_dev
cd build
cd cmake

if [ "$(uname)" == "Darwin" ]; then
   N=`sysctl -n hw.logicalcpu`
else
   N=`nproc --all`
fi

mkdir -p ../../../public/esysos/coverage
mkdir -p ../../../public/esysos_freertos/coverage
mkdir -p ../../../public/esysos_esysc/coverage
mkdir -p ../../../public/logs

make esysos_coverage -j$N
RESULT_ESYSOS_T_UT=$?

lcov --list esysos_coverage.info

cp -R esysos_coverage/* ../../../public/esysos/coverage
cp esysos_coverage.info ../../../public/logs/esysos_coverage.info
cp report.junit ../../../public/logs/coverage_esysos_junit.txt
cp report.junit ../../coverage_esysos_junit.txt

make esysos_freertos_coverage -j$N
RESULT_ESYSOS_FREERTOS_T_UT=$?

lcov --list esysos_freertos_coverage.info

cp -R esysos_freertos_coverage/* ../../../public/esysos_freertos/coverage
cp esysos_freertos_coverage.info ../../../public/logs/esysos_freertos_coverage.info
cp report.junit ../../../public/logs/coverage_freertos_esysos_junit.txt
cp report.junit ../../coverage_freertos_esysos_junit.txt

COVERAGE_ERROR=0

if [ ! ${RESULT_ESYSOS_T_UT} -eq 0 ]; then
   echo "${TXT_E}Code coverage on unit tests esysos_t failed.${TXT_CLEAR}"
   COVERAGE_ERROR=1
fi

if [ ! ${RESULT_ESYSOS_FREERTOS_T_UT} -eq 0 ]; then
   echo "${TXT_E}Code coverage on unit tests esysos_freertos_t failed.${TXT_CLEAR}"
   COVERAGE_ERROR=1
fi

if [ ! ${COVERAGE_ERROR} -eq 0 ]; then
   exit 1
fi

echo "${TXT_S}Run code coverage on unit tests done.${TXT_CLEAR}"
