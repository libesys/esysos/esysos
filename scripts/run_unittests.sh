#!/bin/bash

echo "${TXT_S}Run unit tests ...${TXT_CLEAR}"

source scripts/start_ssh_agent

cd build_dev

build/cmake/bin/esysos_t --logger=HRF,test_suite,stdout:JUNIT,all,esysos_t_report_junit.txt
ESYSOS_T_RESULT_UT=$?

build/cmake/bin/esysos_freertos_t --logger=HRF,test_suite,stdout:JUNIT,all,esysos_freertos_t_report_junit.txt
ESYSOS_FREERTOS_T_RESULT_UT=$?

build/cmake/bin/esysos_esysc_t --logger=HRF,test_suite,stdout:JUNIT,all,esysos_esysc_t_report_junit.txt
ESYSOS_ESYSC_T_RESULT_UT=$?

mkdir -p ../public/logs
cp *_junit.txt ../public/logs

UT_ERROR=0

if [ ! ${ESYSOS_T_RESULT_UT} -eq 0 ]; then
   echo "${TXT_E}Unit tests esysos_t failed.${TXT_CLEAR}"
   UT_ERROR=1
fi

if [ ! ${ESYSOS_FREERTOS_T_RESULT_UT} -eq 0 ]; then
   echo "${TXT_E}Unit tests esysos_freertos_t failed.${TXT_CLEAR}"
   UT_ERROR=1
fi

if [ ! ${ESYSOS_ESYSC_T_RESULT_UT} -eq 0 ]; then
   echo "${TXT_E}Unit tests esysos_esysc_t failed.${TXT_CLEAR}"
   # UT_ERROR=1
fi

if [ ! ${UT_ERROR} -eq 0 ]; then
   exit 1
fi

echo "${TXT_S}Run unit tests done.${TXT_CLEAR}"
