
/*!
 * \file esys/os/timebase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/timebase.h"

namespace esys::os
{

TimeBase::Source TimeBase::m_dft_time_source = MCU_CLK_BASED;

void TimeBase::set_dft_time_source(Source dft_time_source)
{
    m_dft_time_source = dft_time_source;
}

TimeBase::Source TimeBase::get_dft_time_source()
{
    return m_dft_time_source;
}

// civil_from_days inspired by http://howardhinnant.github.io/date_algorithms.html#civil_from_days
void TimeBase::civil_from_days(uint64_t z, int32_t *py, uint32_t *pm, uint32_t *pd)
{
    z += 719468;
    const auto era = static_cast<int32_t>((z >= 0 ? z : z - 146096) / 146097);
    const auto doe = static_cast<uint32_t>(z - era * 146097);                   // [0, 146096]
    const uint32_t yoe = (doe - doe / 1460 + doe / 36524 - doe / 146096) / 365; // [0, 399]
    const int32_t y = static_cast<int32_t>(yoe) + era * 400;
    const uint32_t doy = doe - (365 * yoe + yoe / 4 - yoe / 100); // [0, 365]
    const uint32_t mp = (5 * doy + 2) / 153;                      // [0, 11]
    const uint32_t d = doy - (153 * mp + 2) / 5 + 1;              // [1, 31]
    const uint32_t m = mp + (mp < 10 ? 3 : -9);                   // [1, 12]
    *py = y + (m <= 2);
    *pm = m;
    *pd = d;
}

TimeBase::DateTime::DateTime(uint64_t ms)
{
    set_epoch_ms(ms);
}

void TimeBase::DateTime::set_epoch_ms(uint64_t ms)
{
    uint64_t secs = ms / 1000;
    uint64_t mins = secs / 60;
    uint64_t hours = mins / 60;
    uint64_t days = hours / 24;
    uint32_t cyear;
    uint32_t cmonth;
    uint32_t cday;
    civil_from_days(days, (int32_t *)&cyear, &cmonth, &cday);

    hours = hours - 24 * days;
    mins = mins - 60 * (24 * days + hours);
    secs = secs - 60 * (60 * (24 * days + hours) + mins);

    m_date.set_year(static_cast<uint8_t>(cyear));
    m_date.set_month(static_cast<uint8_t>(cmonth));
    m_date.set_day(static_cast<uint8_t>(cday));

    m_time.set_hours(static_cast<uint8_t>(hours));
    m_time.set_minutes(static_cast<uint8_t>(mins));
    m_time.set_seconds(static_cast<uint8_t>(secs));
}

} // namespace esys::os
