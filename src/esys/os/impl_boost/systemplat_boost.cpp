/*!
 * \file esys/os/impl_boost/systemplat_boost.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/impl_boost/systemplat.h"
#include "esys/os/impl_boost/taskplatregistry.h"

namespace esys::os::impl_boost
{

SystemPlat::SystemPlat()
    : SystemIf()
{
}

SystemPlat::~SystemPlat() = default;

int SystemPlat::init()
{
    return 0;
}

int SystemPlat::run()
{
    TaskPlatRegistry::get().start_all();

    TaskPlatRegistry::get().wait_all_done();
    return 0;
}

int SystemPlat::release()
{
    TaskPlatRegistry::get().clear();
    return 0;
}

} // namespace esys::os::impl_boost
