/*!
 * \file esys/os/impl_boost/mutexplatimpl.cpp
 * \brief Implementation of the Boost Mutex PIMPL class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/impl_boost/mutexplatimpl.h"

namespace esys::os::impl_boost
{

MutexPlatImpl::MutexPlatImpl(MutexIf::Type type)
    : m_type(type)
{
}

MutexPlatImpl::~MutexPlatImpl() = default;

void MutexPlatImpl::set_type(MutexIf::Type type)
{
    m_type = type;
}

MutexIf::Type MutexPlatImpl::get_type() const
{
    return m_type;
}

int MutexPlatImpl::plat_init()
{
    assert(m_mutex == nullptr);
    assert(m_recursive_mutex == nullptr);

    if (get_type() == MutexIf::Type::DEFAULT)
    {
        if (m_mutex != nullptr) return -1;
        m_mutex = std::make_unique<boost::mutex>();
        return 0;
    }
    else
    {
        if (m_recursive_mutex != nullptr) return -1;
        m_recursive_mutex = std::make_unique<boost::recursive_mutex>();
        return 0;
    }
}

int MutexPlatImpl::plat_release()
{
    assert((m_mutex != nullptr) || (m_recursive_mutex != nullptr));

    if (get_type() == MutexIf::Type::DEFAULT)
    {
        if (m_mutex == nullptr) return -1;
        m_mutex.reset();
        return 0;
    }
    else
    {
        if (m_recursive_mutex == nullptr) return -1;
        m_recursive_mutex.reset();
        return 0;
    }
}

int MutexPlatImpl::lock()
{
    if (get_type() == MutexIf::Type::DEFAULT)
    {
        assert(m_mutex != nullptr);

        if (m_mutex == nullptr) return -1;
        m_mutex->lock();
        return 0;
    }
    else
    {
        assert(m_recursive_mutex != nullptr);

        if (m_recursive_mutex == nullptr) return -1;
        m_recursive_mutex->lock();
        return 0;
    }
}

int MutexPlatImpl::unlock()
{
    if (get_type() == MutexIf::Type::DEFAULT)
    {
        assert(m_mutex != nullptr);

        if (m_mutex == nullptr) return -1;
        m_mutex->unlock();
        return 0;
    }
    else
    {
        assert(m_recursive_mutex != nullptr);

        if (m_recursive_mutex == nullptr) return -1;
        m_recursive_mutex->unlock();
        return 0;
    }
}

int MutexPlatImpl::try_lock()
{
    bool result;

    if (get_type() == MutexIf::Type::DEFAULT)
    {
        assert(m_mutex != nullptr);

        if (m_mutex == nullptr) return -1;
        result = m_mutex->try_lock();
    }
    else
    {
        assert(m_recursive_mutex != nullptr);

        if (m_recursive_mutex == nullptr) return -1;
        result = m_recursive_mutex->try_lock();
    }

    if (result) return 0;
    return -1;
}

} // namespace esys::os::impl_boost
