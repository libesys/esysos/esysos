/*!
 * \file esys/os/impl_boost/test/taskplat01_boost.cpp
 * \brief For precompiled headers
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/test/esysos_t_prec.h"

#include <esys/os/impl_boost/semaphoreplat.h>
#include <esys/os/impl_boost/systemplat.h>
#include <esys/os/impl_boost/taskplat.h>
#include <esys/os/impl_boost/taskplatregistry.h>
#include <esys/os/taskbase.h>

#include <iostream>

namespace esys::os::impl_boost::test
{

namespace taskplat01
{

class MyTask : public TaskBase, public TaskPlat
{
public:
    MyTask();

    int entry() override;

    int get_value() const;

protected:
    int m_value = 0;
};

MyTask::MyTask()
    : TaskBase()
    , TaskPlat()
{
    set_task_base(this);
    set_name("MyTask");
    set_kind(TaskKind::STANDALONE);
}

int MyTask::entry()
{
    ++m_value;
    sleep(1000);

    return 0;
}

int MyTask::get_value() const
{
    return m_value;
}

/*! \class TaskPlat01Boost esys/os/impl_boost/test/taskplat01_boost.cpp
 * "esys/os/impl_boost/test/taskplat01_boost.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(TaskPlat01Boost)
{
    SystemPlat system;
    MyTask task;

    int result = system.init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = task.plat_init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = system.run();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    ESYSTEST_REQUIRE_EQUAL(task.get_value(), 1);
    ESYSTEST_REQUIRE_EQUAL(TaskPlatRegistry::get().get_size(), 1);

    auto task_plat = TaskPlatRegistry::get().get_task_plat(0);
    ESYSTEST_REQUIRE_NE(task_plat, nullptr);
    ESYSTEST_REQUIRE_EQUAL(task_plat->get_name(), task.get_name());

    result = task.plat_release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = system.release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);
}

} // namespace taskplat01

} // namespace esys::os::impl_boost::test
