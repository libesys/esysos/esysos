/*!
 * \file esys/os/impl_boost/test/systemexit01_boost.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/test/esysos_t_prec.h"

#include <esys/os/impl_boost/systemplat.h>
#include <esys/os/impl_boost/taskplat.h>

#include <esys/os/taskbase.h>

#include <iostream>

namespace esys::os::impl_boost::test
{

namespace system_exit01
{

class MyTask : public TaskBase, public TaskPlat
{
public:
    MyTask(const char *name);

    int entry() override;
};

MyTask::MyTask(const char *name)
    : TaskBase()
    , TaskPlat(name)
{
    set_task_base(this);
    set_name(name);
    set_kind(TaskKind::STANDALONE);
}

int MyTask::entry()
{
    int count = 10;

    while (1)
    {
        if (count < 0)
        {
            exit_system();
        }
        --count;
        sleep(100);
    }

    return 0;
}

/*! \class SystemExit01Boost esys/os/impl_boost/test/systemexit01_boost.cpp
 * "esys/os/impl_boost/test/systemexit01_boost.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(SystemExit01Boost)
{
    SystemPlat system;
    MyTask task("MyTask");

    int result = system.init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = task.plat_init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = system.run();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = task.plat_release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = system.release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);
}

} // namespace system_exit01

} // namespace esys::os::impl_boost::test
