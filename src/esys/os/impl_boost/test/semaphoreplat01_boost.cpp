/*!
 * \file esys/os/impl_boost/test/semaphoreplat01_boost.cpp
 * \brief For precompiled headers
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/test/esysos_t_prec.h"

#include <esys/os/impl_boost/semaphoreplat.h>

#include <iostream>

namespace esys::os::impl_boost::test
{

/*! \class SemaphorePlat01Boost esys/os/impl_boost/test/semaphoreplat01_boost.cpp
 * "esys/os/impl_boost/test/semaphoreplat01_boost.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(SemaphorePlat01Boost)
{
    SemaphorePlat sem(0);

    ESYSTEST_REQUIRE_EQUAL(sem.get_count(), 0);

    ESYSTEST_REQUIRE_EQUAL(sem.post(), 0);

    ESYSTEST_REQUIRE_EQUAL(sem.get_count(), 1);

    ESYSTEST_REQUIRE_EQUAL(sem.wait(), 0);

    ESYSTEST_REQUIRE_EQUAL(sem.try_wait(), -1);

    ESYSTEST_REQUIRE_EQUAL(sem.post(), 0);

    ESYSTEST_REQUIRE_EQUAL(sem.try_wait(), 0);
}

} // namespace esys::os::impl_boost::test
