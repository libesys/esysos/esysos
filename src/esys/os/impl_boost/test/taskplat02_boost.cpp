/*!
 * \file esys/os/impl_boost/test/taskplat02_boost.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/test/esysos_t_prec.h"

#include <esys/os/impl_boost/semaphoreplat.h>
#include <esys/os/impl_boost/systemplat.h>
#include <esys/os/impl_boost/taskplat.h>
#include <esys/os/impl_boost/time.h>
#include <esys/os/impl_boost/timeinfo.h>
#include <esys/os/impl_boost/timekeeper.h>
#include <esys/os/taskbase.h>

#include <boost/thread.hpp>

#include <iostream>

namespace esys::os::impl_boost::test
{

namespace taskplat02
{

class MyTask : public TaskBase, public TaskPlat
{
public:
    MyTask(bool master);

    int entry() override;
    int master();
    int slave();

    void set_sem_rem(SemaphorePlat *sem_rem);
    SemaphorePlat *get_sem_rem();

    SemaphorePlat *get_sem();

protected:
    bool m_master = false;
    SemaphorePlat m_sem{0};
    SemaphorePlat *m_sem_rem = nullptr;
};

MyTask::MyTask(bool master)
    : TaskBase()
    , TaskPlat()
    , m_master(master)
{
    set_task_base(this);
    if (master)
        set_name("MyTaskMaster");
    else
        set_name("MyTaskSlave");
    set_kind(TaskKind::STANDALONE);
}

int MyTask::entry()
{
    auto time_info = std::make_shared<TimeInfo>();
    boost::thread::id thread_id = boost::this_thread::get_id();

    TimeKeeper::register_time_info(thread_id, time_info);

    if (m_master) return master();
    return slave();
}

int MyTask::master()
{
    int result;

    result = m_sem.wait();
    ESYSTEST_MT_REQUIRE_EQUAL(0, result);

    ESYSTEST_MT_REQUIRE_NE(m_sem_rem, nullptr);
    result = m_sem_rem->post();
    ESYSTEST_MT_REQUIRE_EQUAL(result, 0);

    result = m_sem.wait();
    ESYSTEST_MT_REQUIRE_EQUAL(result, 0);

    ESYSTEST_MT_REQUIRE_GE(Time::millis(), (uint32_t)200);

    return 0;
}

int MyTask::slave()
{
    int result;
    Time::sleep(100);

    ESYSTEST_MT_REQUIRE_NE(m_sem_rem, nullptr);

    result = m_sem_rem->post();
    ESYSTEST_MT_REQUIRE_EQUAL(result, 0);

    result = m_sem.wait();
    ESYSTEST_MT_REQUIRE_EQUAL(result, 0);

    Time::sleep(100);
    result = m_sem_rem->post();
    ESYSTEST_MT_REQUIRE_EQUAL(result, 0);

    return 0;
}

void MyTask::set_sem_rem(SemaphorePlat *sem_rem)
{
    m_sem_rem = sem_rem;
}

SemaphorePlat *MyTask::get_sem_rem()
{
    return m_sem_rem;
}

SemaphorePlat *MyTask::get_sem()
{
    return &m_sem;
}

/*! \class TaskPlat02Boost esys/os/impl_boost/test/taskplat02_boost.cpp
 * "esys/os/impl_boost/test/taskplat02_boost.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(TaskPlat02Boost)
{
    SystemPlat system;
    MyTask master{true};
    MyTask slave{false};

    master.set_sem_rem(slave.get_sem());
    slave.set_sem_rem(master.get_sem());

    int result = system.init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = master.plat_init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = slave.plat_init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = system.run();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = master.plat_release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = slave.plat_release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = system.release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);
}

} // namespace taskplat02

} // namespace esys::os::impl_boost::test
