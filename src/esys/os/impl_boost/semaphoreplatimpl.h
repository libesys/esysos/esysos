/*!
 * \file esys/os/impl_boost/semaphoreplatimpl.h
 * \brief Header of the Boost Semaphore PIML class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysos_defs.h"
#include "esys/os/semaphoreif.h"

#include <boost/thread.hpp>

#include <memory>

namespace esys::os::impl_boost
{

/*! \class SemaphorePlatImpl esys/os/impl_boost/semaphoreplatimpl.h "esys/os/impl_boost/semaphoreplatimpl.h"
 *  \brief the Boost Semaphore PIML class
 */
class ESYSOS_API SemaphorePlatImpl
{
public:
    //! Constructor
    /*!
     * \param[in] count the initial count of the semaphore
     */
    explicit SemaphorePlatImpl(int count);

    //! Destructor
    ~SemaphorePlatImpl();

    void set_init_count(int count);
    int get_init_count() const;

    void set_max_count(int max_count);
    int get_max_count() const;

    int plat_init();

    int post();
    int wait();
    int try_wait();

    int get_count();

    int wait_for(int ms);

private:
    //!< \cond DOXY_IMPL
    boost::mutex m_mutex;
    boost::condition_variable m_cv;
    int m_init_count = 0;
    int m_count = 0;
    int m_max_count = -1;
    //!< \endcond
};

} // namespace esys::os::impl_boost
