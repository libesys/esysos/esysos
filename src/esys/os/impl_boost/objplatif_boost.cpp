/*!
 * \file esys/os/impl_boost/objplatif_boost.cpp
 * \brief Definition of the Boost Object platform interfaces
 *
 * \cond
 *__legal_b__
 *
 * Copyright (c) 2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 *__legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/impl_boost/objplatif.h"

namespace esys::os::impl_boost
{

ObjPlatIf::ObjPlatIf()
    : os::ObjPlatIf()
{
}

ObjPlatIf::ObjPlatIf(const std::string &name)
    : m_name(name)
{
}

ObjPlatIf::~ObjPlatIf() = default;

void ObjPlatIf::set_name(const char *name)
{
    m_name = name;
}

const char *ObjPlatIf::get_name() const
{
    return m_name.c_str();
}

} // namespace esys::os::impl_boost
