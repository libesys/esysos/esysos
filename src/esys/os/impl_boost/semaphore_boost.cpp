/*!
 * \file esys/os/impl_boost/semaphore_impl_boost.cpp
 * \brief Definition of the Boost Semaphore class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/impl_boost/semaphore.h"

namespace esys::os::impl_boost
{

Semaphore::Semaphore(const base::ObjectName &name, int count)
    : base::Object(name)
    , SemaphorePlat(count)
{
}

Semaphore::~Semaphore() = default;

} // namespace esys::os::impl_boost
