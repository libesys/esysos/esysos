/*!
 * \file esys/os/impl_boost/taskplatimpl_boost.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/impl_boost/taskplatimpl.h"
#include "esys/os/impl_boost/taskplat.h"
#include "esys/os/impl_boost/taskplatregistry.h"

#include <boost/thread.hpp>

#include <cassert>

namespace esys::os::impl_boost
{

TaskPlatImpl::TaskPlatImpl(TaskPlat *self)
    : m_self(self)
{
}

TaskPlatImpl::~TaskPlatImpl() = default;

int TaskPlatImpl::plat_init()
{
    TaskPlatRegistry::get().register_task(this->self());
    return 0;
}

int TaskPlatImpl::plat_release()
{
    // Nothing needed for the Boost implementation
    return 0;
}

int TaskPlatImpl::start()
{
    assert(m_task != nullptr);
    assert(m_task->get_state() == os::TaskBase::State::INSTANCIATED);

    m_thread = boost::thread(boost::bind(&TaskPlatImpl::call_entry, this));

    TaskPlatRegistry::get().register_task(m_thread.get_id(), this->self());

    m_sem_registered.post();

    return 0;
}

int TaskPlatImpl::stop()
{

    if (m_thread.joinable()) m_thread.join();

    return 0;
}

int32_t TaskPlatImpl::kill()
{
    // Nothing can be done for the Boost implementation
    return 0;
}

void TaskPlatImpl::sleep(uint32_t ms)
{
    boost::this_thread::sleep_for(boost::chrono::milliseconds(ms));
}

void TaskPlatImpl::usleep(uint32_t us)
{
    boost::this_thread::sleep_for(boost::chrono::microseconds(us));
}

void TaskPlatImpl::wait_done()
{
    if (m_thread.joinable()) m_thread.join();
}

void TaskPlatImpl::wait_started()
{
    m_sem_started.wait();
}

void TaskPlatImpl::exit_system([[maybe_unused]] int result)
{
    m_thread.interrupt();
}

void TaskPlatImpl::call_entry()
{
    assert(m_task != nullptr);
    assert(self()->get_name() != nullptr);

    set_thread_name(m_thread.get_id(), self()->get_name());

    m_task->started();

    m_sem_registered.wait();

    m_sem_started.post();

    m_task->entry();
    m_task->done();
}

void TaskPlatImpl::set_task_base(os::TaskBase *task)
{
    m_task = task;
}

os::TaskBase *TaskPlatImpl::get_task_base()
{
    return m_task;
}

void TaskPlatImpl::set_name(const std::string &name)
{
    m_name = name;
}

const std::string &TaskPlatImpl::get_name() const
{
    return m_name;
}

TaskPlat *TaskPlatImpl::self()
{
    return m_self;
}

#ifdef WIN32

const DWORD MS_VC_EXCEPTION = 0x406D1388;
#pragma pack(push, 8)
typedef struct THREADNAME_INFO
{
    DWORD dwType;     // Must be 0x1000.
    LPCSTR szName;    // Pointer to name (in user addr space).
    DWORD dwThreadID; // Thread ID (-1=caller thread).
    DWORD dwFlags;    // Reserved for future use, must be zero.
} THREADNAME_INFO;
#pragma pack(pop)

void _SetThreadName(DWORD threadId, const std::string &threadName)
{
    THREADNAME_INFO info;
    info.dwType = 0x1000;
    info.szName = threadName.c_str();
    info.dwThreadID = threadId;
    info.dwFlags = 0;
    __try
    {
        RaiseException(MS_VC_EXCEPTION, 0, sizeof(info) / sizeof(ULONG_PTR), (ULONG_PTR *)&info);
    }
    __except (EXCEPTION_EXECUTE_HANDLER)
    {
    }
}
#else
#endif

void TaskPlatImpl::set_thread_name(boost::thread::id threadId, const std::string &threadName)
{
#ifdef WIN32
    const char *cchar = threadName.c_str();
    // convert HEX string to DWORD
    unsigned int dwThreadId;
    std::stringstream ss;
    ss << std::hex << threadId;
    ss >> dwThreadId;
    // set thread name
    _SetThreadName((DWORD)dwThreadId, cchar);
#else
#endif
}

} // namespace esys::os::impl_boost
