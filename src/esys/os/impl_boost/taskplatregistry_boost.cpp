/*!
 * \file esys/os/impl_boost/taskplatregistry_boost.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/impl_boost/taskplatregistry.h"
#include "esys/os/impl_boost/taskplat.h"
#include "esys/os/impl_boost/taskplatimpl.h"
#include "esys/os/assert.h"

namespace esys::os::impl_boost
{

std::unique_ptr<TaskPlatRegistry> TaskPlatRegistry::s_registry;

TaskPlatRegistry &TaskPlatRegistry::get()
{
    if (s_registry == nullptr) s_registry = std::make_unique<TaskPlatRegistry>();

    return *s_registry.get();
}

TaskPlatRegistry::TaskPlatRegistry() = default;

TaskPlatRegistry::~TaskPlatRegistry() = default;

TaskPlat *TaskPlatRegistry::get_current_task_plat()
{
    boost::lock_guard lock(m_data_mutex);

    boost::thread::id thread_id = boost::this_thread::get_id();
    ThreadIdMap::iterator it;

    it = m_thread_id_map.find(thread_id);

    assert(it != m_thread_id_map.end());

    return it->second->m_task_plat;
}

void TaskPlatRegistry::register_task(TaskPlat *task)
{
    m_task_plats.push_back(task);
}

void TaskPlatRegistry::register_task(boost::thread::id thread_id, TaskPlat *task)
{
    boost::lock_guard lock(m_data_mutex);

    auto helper = std::make_shared<Helper>();

    helper->m_task_plat = task;

    m_thread_id_map[thread_id] = helper;
    m_task_plat_helpers.push_back(helper);
}

std::size_t TaskPlatRegistry::get_size()
{
    boost::lock_guard lock(m_data_mutex);

    return m_task_plat_helpers.size();
}

TaskPlat *TaskPlatRegistry::get_task_plat(int idx)
{
    if (idx >= m_task_plat_helpers.size()) return nullptr;
    return m_task_plat_helpers[idx]->m_task_plat;
}

void TaskPlatRegistry::start_all()
{
    int result = 0;

    for (auto task_plat : m_task_plats)
    {
        result = task_plat->start();
        assert(result == 0);
        task_plat->get_impl()->wait_started();
    }
}

void TaskPlatRegistry::wait_all_done()
{
    for (auto helper : m_task_plat_helpers)
    {
        helper->m_task_plat->wait_done();
    }
}

void TaskPlatRegistry::clear()
{
    m_task_plats.clear();
    m_task_plat_helpers.clear();
    m_thread_id_map.clear();
}

} // namespace esys::os::impl_boost
