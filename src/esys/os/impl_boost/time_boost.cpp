/*!
 * \file esys/os/impl_boost/time_boost.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/impl_boost/time.h"
#include "esys/os/impl_boost/timekeeper.h"

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread.hpp>

namespace esys::os::impl_boost
{

uint32_t Time::millis(Source source)
{
    assert(TimeKeeper::get_current() != nullptr);

    return TimeKeeper::get_current()->millis();
}

uint64_t Time::micros(Source source)
{
    assert(TimeKeeper::get_current() != nullptr);

    return TimeKeeper::get_current()->micros();
}

uint32_t Time::micros_low()
{
    assert(TimeKeeper::get_current() != nullptr);

    return TimeKeeper::get_current()->micros_low();
}

void Time::sleep(uint32_t ms)
{
    ::boost::this_thread::sleep_for(::boost::chrono::milliseconds(ms));
}

void Time::usleep(uint32_t us)
{
    ::boost::this_thread::sleep_for(::boost::chrono::microseconds(us));
}

static ::boost::posix_time::ptime g_ptime_when_set_done;
static ::boost::posix_time::ptime g_ptime_set;
static bool g_date_or_time_set = false;

int32_t Time::set_time(Struct &time)
{
    g_ptime_when_set_done = ::boost::posix_time::second_clock::universal_time();
    g_ptime_set = ::boost::posix_time::ptime(
        g_ptime_when_set_done.date(),
        ::boost::posix_time::time_duration(time.get_hours(), time.get_minutes(), time.get_seconds()));
    g_date_or_time_set = true;

    return 0;
}

void Time::get_time(Struct &time)
{
    if (g_date_or_time_set == false)
    {
        ::boost::posix_time::ptime date_time = ::boost::posix_time::second_clock::universal_time();
        ::boost::posix_time::time_duration duration;

        duration = date_time.time_of_day();
        time.set_hours(static_cast<uint8_t>(duration.hours()));
        time.set_minutes(static_cast<uint8_t>(duration.minutes()));
        time.set_seconds(static_cast<uint8_t>(duration.seconds()));
        return;
    }

    ::boost::posix_time::ptime date_time_now = ::boost::posix_time::second_clock::universal_time();
    ::boost::posix_time::ptime simulated_time = g_ptime_set + (date_time_now - g_ptime_when_set_done);
    ::boost::posix_time::time_duration duration;

    duration = simulated_time.time_of_day();
    time.set_hours(static_cast<uint8_t>(duration.hours()));
    time.set_minutes(static_cast<uint8_t>(duration.minutes()));
    time.set_seconds(static_cast<uint8_t>(duration.seconds()));
}

int32_t Time::set_date(Date &date)
{
    g_ptime_when_set_done = ::boost::posix_time::second_clock::universal_time();
    g_ptime_set =
        ::boost::posix_time::ptime(::boost::gregorian::date(date.get_year(), date.get_month(), date.get_day()),
                                   g_ptime_when_set_done.time_of_day());
    g_date_or_time_set = true;

    return 0;
}

void Time::get_date(Date &date)
{
    if (g_date_or_time_set == false)
    {
        ::boost::posix_time::ptime date_time = ::boost::posix_time::second_clock::universal_time();

        date.set_year(static_cast<uint8_t>(date_time.date().year()));
        date.set_month(static_cast<uint8_t>(date_time.date().month()));
        date.set_day(static_cast<uint8_t>(date_time.date().day()));
        return;
    }

    ::boost::posix_time::ptime date_time_now = ::boost::posix_time::second_clock::universal_time();
    ::boost::posix_time::ptime simulated_time = g_ptime_set + (date_time_now - g_ptime_when_set_done);
    ::boost::posix_time::time_duration duration;

    date.set_year(static_cast<uint8_t>(simulated_time.date().year()));
    date.set_month(static_cast<uint8_t>(simulated_time.date().month()));
    date.set_day(static_cast<uint8_t>(simulated_time.date().day()));
}

int32_t Time::set_date_time(DateTime &date_time)
{
    g_ptime_when_set_done = ::boost::posix_time::second_clock::universal_time();
    g_ptime_set = ::boost::posix_time::ptime(
        ::boost::gregorian::date(date_time.get_date().get_year(), date_time.get_date().get_month(),
                                 date_time.get_date().get_day()),
        ::boost::posix_time::time_duration(date_time.get_time().get_hours(), date_time.get_time().get_minutes(),
                                           date_time.get_time().get_seconds()));
    g_date_or_time_set = true;

    return 0;
}

void Time::get_date_time(DateTime &date_time)
{
    get_date(date_time.get_date());
    get_time(date_time.get_time());
}

int32_t Time::start_timestamp()
{
    return 0; // Since always enabled
}

int32_t Time::stop_timestamp()
{
    return 0; // Since always enabled
}

} // namespace esys::os::impl_boost
