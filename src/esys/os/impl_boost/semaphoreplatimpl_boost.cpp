/*!
 * \file esys/os/impl_boost/semaphoreplatimpl_boost.h
 * \brief Implementation of the Boost Semaphore PIML class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/impl_boost/semaphoreplatimpl.h"

#include <boost/chrono.hpp>

namespace esys::os::impl_boost
{

SemaphorePlatImpl::SemaphorePlatImpl(int count)
    : m_count(count)
{
}

SemaphorePlatImpl::~SemaphorePlatImpl()
{
}

void SemaphorePlatImpl::set_init_count(int init_count)
{
    m_init_count = init_count;
}

int SemaphorePlatImpl::get_init_count() const
{
    return m_init_count;
}

void SemaphorePlatImpl::set_max_count(int max_count)
{
    m_max_count = max_count;
}

int SemaphorePlatImpl::get_max_count() const
{
    return m_max_count;
}

int SemaphorePlatImpl::plat_init()
{
    m_count = m_init_count;
    return 0;
}

int SemaphorePlatImpl::get_count()
{
    return m_count;
}

int SemaphorePlatImpl::post()
{
    boost::unique_lock lock(m_mutex);

    ++m_count;

    m_cv.notify_one();
    return 0;
}

int SemaphorePlatImpl::wait()
{
    boost::unique_lock lock(m_mutex);

    m_cv.wait(lock, [this]() { return (m_count > 0); });

    --m_count;
    return 0;
}

int SemaphorePlatImpl::try_wait()
{
    boost::unique_lock lock(m_mutex);

    if (m_count == 0) return -1;
    --m_count;
    return 0;
}

int SemaphorePlatImpl::wait_for(int ms)
{
    boost::unique_lock lock(m_mutex);

    boost::chrono::milliseconds period(ms);
    if (!m_cv.wait_for(lock, period, [this]() { return (m_count > 0); })) return -1;

    --m_count;
    return 0;
}

} // namespace esys::os::impl_boost
