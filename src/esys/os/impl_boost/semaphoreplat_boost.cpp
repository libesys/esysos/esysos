/*!
 * \file esys/os/impl_boost/semaphoreplat_boost.cpp
 * \brief Implementation of the Boost SemaphorePlat class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/impl_boost/semaphoreplat.h"
#include "esys/os/impl_boost/semaphoreplatimpl.h"

namespace esys::os::impl_boost
{

SemaphorePlat::SemaphorePlat(int count)
    : SemaphoreIf()
    , ObjPlatIf()
{
    m_impl = std::make_unique<SemaphorePlatImpl>(count);
}

SemaphorePlat::~SemaphorePlat() = default;

void SemaphorePlat::set_init_count(int count)
{
    return m_impl->set_init_count(count);
}

int SemaphorePlat::get_init_count() const
{
    return m_impl->get_init_count();
}

void SemaphorePlat::set_max_count(int max_count)
{
    m_impl->set_max_count(max_count);
}

int SemaphorePlat::get_max_count() const
{
    return m_impl->get_max_count();
}

int SemaphorePlat::get_count() const
{
    return m_impl->get_count();
}

int SemaphorePlat::plat_init()
{
    return m_impl->plat_init();
}

int SemaphorePlat::plat_release()
{
    return 0;
}

int SemaphorePlat::post(bool from_isr)
{
    return m_impl->post();
}

int SemaphorePlat::wait(bool from_isr)
{
    return m_impl->wait();
}

int SemaphorePlat::try_wait(bool from_isr)
{
    return m_impl->try_wait();
}

int SemaphorePlat::wait_for(int ms, bool from_isr)
{
    return m_impl->wait_for(ms);
}

} // namespace esys::os::impl_boost
