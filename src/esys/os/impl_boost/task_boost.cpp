/*!
 * \file esys/os/impl_boost/task_impl_boost.cpp
 * \brief Definition of the Boost Task class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/impl_boost/task.h"

namespace esys::os::impl_boost
{

Task::Task(const base::ObjectName &name)
    : base::Object(name)
    , TaskBase()
    , TaskPlat(name.get_name())
{
    set_task_base(this);
}

Task::~Task() = default;

} // namespace esys::os::impl_boost
