/*!
 * \file esys/os/impl_boost/taskplatimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysos_defs.h"
#include "esys/os/taskplatif.h"
#include "esys/os/taskbase.h"
#include "esys/os/impl_boost/semaphoreplat.h"

#include <boost/thread.hpp>

#include <string>

namespace esys::os::impl_boost
{

// class ESYSOS_API TaskMngrImpl;
class ESYSOS_API TaskPlat;

/*! \class TaskImpl esys/os/impl_boost/taskplatimpl.h "esys/os/impl_boost/taskplatimpl.h"
 *  \brief Boost implementation of a Task
 */
class ESYSOS_API TaskPlatImpl
{
public:
    explicit TaskPlatImpl(TaskPlat *self);
    ~TaskPlatImpl();

    int plat_init();
    int plat_release();

    int start();
    int stop();
    int kill();

    void sleep(uint32_t ms);
    void usleep(uint32_t us);

    void wait_done();
    void wait_started();
    void exit_system(int result);

    void call_entry();

    void set_task_base(TaskBase *task);
    TaskBase *get_task_base();

    void set_name(const std::string &name);
    const std::string &get_name() const;

    static void set_thread_name(boost::thread::id threadId, const std::string &threadName);

    TaskPlat *self();

private:
    std::string m_name; //!< The name of the task
    TaskPlat *m_self = nullptr;
    os::TaskBase *m_task = nullptr; //!< Pointer to the TaskBase object using this TaskPlatImpl object
    boost::thread m_thread;         //!< Boost thread used
    SemaphorePlat m_sem_registered{0};
    SemaphorePlat m_sem_started{0};
};

} // namespace esys::os::impl_boost
