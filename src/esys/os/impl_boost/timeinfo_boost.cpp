/*!
 * \file esys/os/impl_boost/timeinfo_boost.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/impl_boost/timeinfo.h"

namespace esys::os::impl_boost
{

TimeInfo::TimeInfo()
{
    m_boost_started_time = boost::posix_time::microsec_clock::local_time();
}

TimeInfo::~TimeInfo() = default;

const boost::posix_time::ptime &TimeInfo::get_boost_started_time() const
{
    return m_boost_started_time;
}

uint32_t TimeInfo::millis()
{
    boost::posix_time::ptime now = boost::posix_time::microsec_clock::local_time();

    boost::posix_time::time_duration diff = now - m_boost_started_time;
    return (uint32_t)diff.total_milliseconds();
}

uint64_t TimeInfo::micros()
{
    boost::posix_time::ptime now = boost::posix_time::microsec_clock::local_time();

    boost::posix_time::time_duration diff = now - m_boost_started_time;
    return (uint64_t)diff.total_microseconds();
}

uint32_t TimeInfo::micros_low()
{
    uint64_t micros;
    boost::posix_time::ptime now = boost::posix_time::microsec_clock::local_time();

    boost::posix_time::time_duration diff = now - m_boost_started_time;

    micros = (uint64_t)diff.total_microseconds();
    micros = (micros << 32) >> 32;
    return (uint32_t)micros;
}

} // namespace esys::os::impl_boost
