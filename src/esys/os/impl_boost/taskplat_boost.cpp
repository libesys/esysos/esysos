/*!
 * \file esys/os/impl_boost/taskplat_boost.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/impl_boost/taskplat.h"
#include "esys/os/impl_boost/taskplatimpl.h"

namespace esys::os::impl_boost
{

TaskPlat::TaskPlat()
    : TaskPlatIf()
    , ObjPlatIf()
{
    m_impl = std::make_unique<TaskPlatImpl>(this);
}

TaskPlat::TaskPlat(const char *name)
    : TaskPlatIf()
    , ObjPlatIf(name)
{
    m_impl = std::make_unique<TaskPlatImpl>(this);
}

TaskPlat::~TaskPlat()
{
}

int TaskPlat::plat_init()
{
    assert(m_impl != nullptr);

    return m_impl->plat_init();
}

int TaskPlat::plat_release()
{
    assert(m_impl != nullptr);

    return m_impl->plat_release();
}

int32_t TaskPlat::start()
{
    assert(m_impl != nullptr);

    return m_impl->start();
}

int32_t TaskPlat::stop()
{
    assert(m_impl != nullptr);

    return m_impl->stop();
}

int32_t TaskPlat::kill()
{
    return 0;
}

void TaskPlat::sleep(uint32_t ms)
{
    assert(m_impl != nullptr);

    return m_impl->sleep(ms);
}

void TaskPlat::usleep(uint32_t us)
{
    assert(m_impl != nullptr);

    return m_impl->usleep(us);
}

void TaskPlat::set_task_base(os::TaskBase *task)
{
    assert(m_impl != nullptr);

    return m_impl->set_task_base(task);
}

void TaskPlat::exit_system(int result)
{
    assert(m_impl != nullptr);

    m_impl->exit_system(result);
}

void TaskPlat::wait_done()
{
    assert(m_impl != nullptr);

    m_impl->wait_done();
}

TaskPlatImpl *TaskPlat::get_impl()
{
    return m_impl.get();
}

} // namespace esys::os::impl_boost
