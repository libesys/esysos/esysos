/*!
 * \file esys/os/impl_boost/timekeeper.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/impl_boost/timekeeper.h"

namespace esys::os::impl_boost
{

boost::mutex TimeKeeper::s_data_mutex;

TimeKeeper::ThreadIdMap TimeKeeper::s_thread_id_map;

std::shared_ptr<TimeInfo> TimeKeeper::get_current()
{
    boost::lock_guard lock(s_data_mutex);

    boost::thread::id thread_id = boost::this_thread::get_id();
    ThreadIdMap::iterator it;

    it = s_thread_id_map.find(thread_id);

    if (it == s_thread_id_map.end()) return nullptr;
    return it->second;
}

void TimeKeeper::register_time_info(boost::thread::id thread_id, std::shared_ptr<TimeInfo> time_info)
{
    boost::lock_guard lock(s_data_mutex);

    s_thread_id_map[thread_id] = time_info;
}

std::size_t TimeKeeper::size()
{
    boost::lock_guard lock(s_data_mutex);

    return s_thread_id_map.size();
}

std::shared_ptr<TimeInfo> TimeKeeper::get(int idx)
{
    boost::lock_guard lock(s_data_mutex);

    auto it = s_thread_id_map.begin();

    for (; (it != s_thread_id_map.end()) && (idx != 0); --idx, ++it)
    {
    }
    if (it == s_thread_id_map.end()) return nullptr;
    return it->second;
}

TimeKeeper::TimeKeeper() = default;

TimeKeeper::~TimeKeeper() = default;

} // namespace esys::os::impl_boost
