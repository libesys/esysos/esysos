/*!
 * \file esys/os/impl_boost/mutexplatimpl.h
 * \brief Header of the Boost Mutex PIMPL class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysos_defs.h"
#include "esys/os/mutexif.h"

#include <boost/thread.hpp>

#include <memory>

namespace esys::os::impl_boost
{

/*! \class MutexPlatImpl esys/impl_boost/mutexplatimpl.h "esys/impl_boost/mutexplatimpl.h"
 *  \brief Boost Mutex class
 */
class ESYSOS_API MutexPlatImpl
{
public:
    explicit MutexPlatImpl(MutexIf::Type type = MutexIf::Type::DEFAULT);
    ~MutexPlatImpl();

    void set_type(MutexIf::Type type);
    MutexIf::Type get_type() const;

    int plat_init();
    int plat_release();

    int lock();
    int unlock();
    int try_lock();

private:
    MutexIf::Type m_type = MutexIf::Type::DEFAULT;
    std::unique_ptr<boost::mutex> m_mutex;
    std::unique_ptr<boost::recursive_mutex> m_recursive_mutex;
};

} // namespace esys::os::impl_boost
