/*!
 * \file esys/os/mutexif.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/mutexif.h"

namespace esys::os
{

MutexIf::MutexIf()
    : ObjPlatIf()
{
}

MutexIf::~MutexIf() = default;

} // namespace esys::os
