/*!
 * \file esys/os/taskplatif.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/taskplatif.h"

namespace esys::os
{

TaskPlatIf::TaskPlatIf() = default;

TaskPlatIf::~TaskPlatIf() = default;

int TaskPlatIf::get_max_runtime_stack_size()
{
    return -1;
}

int TaskPlatIf::get_min_remaining_runtime_stack_size()
{
    return -1;
}

} // namespace esys::os
