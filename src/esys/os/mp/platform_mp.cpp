/*!
 * \file esys/os/mp/platform_mp.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/mp/platform.h"
#include "esys/os/impl_boost/mp/platform.h"

namespace esys::os::mp
{

std::shared_ptr<Platform> Platform::s_platform;

Platform &Platform::get()
{
    if (s_platform == nullptr)
    {
        s_platform = std::make_shared<Platform>();
        s_platform->init();
    }

    return *s_platform.get();
}

Platform::Platform()
    : mp::PlatformBase()
{
}

Platform::~Platform() = default;

int Platform::init()
{
    auto boot_plat = std::make_shared<impl_boost::mp::Platform>();

    m_platform_map.clear();

    m_platform_map["boost"] = boot_plat;
    m_current = boot_plat;

    int result = m_mngr.load_if_not_loaded();
    if (result < 0) return result;

    auto const &plugins = m_mngr.get_plugins();
    for (auto plugin : plugins)
    {
        m_platform_map[plugin->get_short_name()] = plugin->get_platform();
    }
    return 0;
}

std::shared_ptr<PlatformBase> Platform::get_current()
{
    return m_current;
}

int Platform::set_current(const std::string &name)
{
    auto plat_it = m_platform_map.find(name);
    if (plat_it == m_platform_map.end()) return -1;

    m_current = plat_it->second;
    return 0;
}

std::shared_ptr<MutexIf> Platform::new_mutex(const std::string &name, MutexIf::Type type)
{
    if (get_current() == nullptr) return nullptr;

    return get_current()->new_mutex(name, type);
}

std::shared_ptr<SemaphoreIf> Platform::new_semaphore(const std::string &name, int count)
{
    if (get_current() == nullptr) return nullptr;

    return get_current()->new_semaphore(name, count);
}

std::shared_ptr<TaskPlatIf> Platform::new_task_plat(const std::string &name)
{
    if (get_current() == nullptr) return nullptr;

    return get_current()->new_task_plat(name);
}

std::shared_ptr<SystemIf> Platform::new_system()
{
    if (get_current() == nullptr) return nullptr;

    return get_current()->new_system();
}

} // namespace esys::os::mp
