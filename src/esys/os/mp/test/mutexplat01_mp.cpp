/*!
 * \file esys/os/mp/test/mutexplat01_mp.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/test/esysos_t_prec.h"

#include <esys/os/mp/mutexplat.h>
#include <esys/os/mp/platform.h>
#include <esys/os/mp/systemplat.h>

#include <iostream>

namespace esys::os::mp::test
{

/*! \class MutexPlat01MP esys/os/mp/test/mutexplat01_mp.cpp "esys/os/mp/test/mutexplat01_mp.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(MutexPlat01MP)
{
    SystemPlat system;
    MutexPlat mutex("mutex");

    int result = system.init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = mutex.plat_init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = mutex.plat_release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = system.release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

#ifdef WIN32
    // currently this will generate an error under Linux:
    // "corrupted double-linked list" from ESysC/SystemC
    Platform::get().set_current("esysc");

    result = system.init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = mutex.plat_init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = mutex.plat_release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = system.release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);
#endif

    Platform::get().set_current("freertos");

    result = system.init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = mutex.plat_init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = mutex.plat_release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = system.release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);
}

} // namespace esys::os::mp::test
