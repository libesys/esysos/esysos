/*!
 * \file esys/os/mp/test/pluginmngr01.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/test/esysos_t_prec.h"

#include <esys/os/mp/pluginmngr.h>

#include <boost/filesystem.hpp>

#include <iostream>

namespace esys::os::mp::test
{

/*! \class PluginMngr01 esys/os/mp/test/pluginmngr01.cpp "esys/os/mp/test/pluginmngr01.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(PluginMngr01)
{
    boost::filesystem::path path;

    auto &ctrl = os::test::TestCaseCtrl::get();
    path = ctrl.GetTempFilesFolder();
    path /= ("pluginmngr01");

    bool remove_all = true;

    try
    {
        if (boost::filesystem::exists(path)) boost::filesystem::remove_all(path);
    }
    catch (const boost::filesystem::filesystem_error &e)
    {
        remove_all = false;
        std::cerr << e.what() << std::endl;
    }

    ESYSTEST_REQUIRE_EQUAL(remove_all, true);

    bool folder_created = boost::filesystem::create_directories(path);
    ESYSTEST_REQUIRE_EQUAL(folder_created, true);

    PluginMngr mngr;

    boost::filesystem::path search_folder;

#ifdef WIN32
    search_folder = ctrl.GetArgV()[0];
    search_folder = boost::filesystem::absolute(search_folder);
    search_folder = search_folder.parent_path(); // Remove binary filename
#else
    search_folder = ctrl.GetArgV()[0];
    search_folder = boost::filesystem::absolute(search_folder);
    search_folder = search_folder.parent_path(); // Remove binary filename
    search_folder = search_folder.parent_path(); // Remobe bin folder
#endif

    mngr.set_search_folder(search_folder.string());

    ESYSTEST_REQUIRE_EQUAL(mngr.get_is_loaded(), false);

    int result = mngr.load();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    auto &plugins = mngr.get_plugins();
    ESYSTEST_REQUIRE_EQUAL(plugins.size(), 2);

    auto plugin = plugins[0];

    //! \TODO do something

    result = mngr.release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);
}

} // namespace esys::os::mp::test
