/*!
 * \file esys/os/mp/test/taskplat01_mp.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/test/esysos_t_prec.h"

#include <esys/os/mp/taskplat.h>
#include <esys/os/mp/platform.h>
#include <esys/os/mp/systemplat.h>
#include <esys/os/taskbase.h>

#include <iostream>

namespace esys::os::mp::test
{

namespace semaphore_plat01
{

class MyTask : public TaskBase, public TaskPlat
{
public:
    MyTask(const char *name);

    int entry() override;
};

MyTask::MyTask(const char *name)
    : TaskBase()
    , TaskPlat(name)
{
    set_task_base(this);
    set_kind(TaskKind::STANDALONE);
}

int MyTask::entry()
{
    sleep(1000);

    return 0;
}

/*! \class SemaphorePlat01MP esys/os/mp/test/taskplat01_mp.cpp "esys/os/mp/test/taskplat01_mp.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(TaskPlat01MP)
{
    SystemPlat system;
    MyTask task("mytask");

    int result = system.init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = task.plat_init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = task.plat_release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = system.release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

#ifdef WIN32
    // currently this will generate an error under Linux:
    // "corrupted double-linked list" from ESysC/SystemC
    Platform::get().set_current("esysc");

    result = system.init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = task.plat_init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = task.plat_release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = system.release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);
#endif

    Platform::get().set_current("freertos");

    result = system.init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = task.plat_init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = task.plat_release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = system.release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);
}

} // namespace semaphore_plat01

} // namespace esys::os::mp::test
