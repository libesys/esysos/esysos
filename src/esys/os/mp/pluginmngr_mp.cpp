/*!
 * \file esys/os/mp/pluginmngr.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/mp/pluginmngr.h"
#include "esys/os/mp/plugin.h"

#include <cassert>

namespace esys::os::mp
{

PluginMngr::PluginMngr()
    : BaseType("esysos")
{
    set_entry_fct_name("get_esysos_plugin");

    // Needed if used from Python code, as the location
    // of the Python executable is not useful
    add_env_var_search_folder("PYESYSOS_INST_DIR");
    add_env_var_search_folder("ESYSOS_INST_DIR");
    add_env_var_search_folder("PYESYSOS_INST_DIR");
    add_env_var_search_folder("ESYSOS_INST_DIR");
    add_env_var_search_folder("PYESYSSDK_INST_DIR");
    add_env_var_search_folder("ESYSSDK_INST_DIR");
}

PluginMngr::~PluginMngr() = default;

base::PluginBase *PluginMngr::get_plugin_from_entry_fct(void *entry_fct)
{
    auto the_entry_fct = (PluginEntryFunction)entry_fct;
    Plugin *plugin;

    assert(entry_fct != nullptr);

    plugin = (*the_entry_fct)();

    return plugin;
}

int PluginMngr::plugin_loaded(Plugin *plugin)
{
    return 0;
}

} // namespace esys::os::mp
