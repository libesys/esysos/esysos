/*!
 * \file esys/os/mp/systemplat_mp.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/mp/systemplat.h"
#include "esys/os/mp/platform.h"
#include "esys/os/assert.h"

namespace esys::os::mp
{

SystemPlat::SystemPlat()
    : SystemIf()
{
}

SystemPlat::~SystemPlat() = default;

int SystemPlat::init()
{
    assert(m_system == nullptr);
    m_system = Platform::get().new_system();
    assert(m_system != nullptr);

    if (m_system == nullptr) return -1;

    return m_system->init();
}

int SystemPlat::run()
{
    assert(m_system != nullptr);

    if (m_system == nullptr) return -1;

    return m_system->run();
}

int SystemPlat::release()
{
    assert(m_system != nullptr);

    if (m_system == nullptr) return -1;

    int result = m_system->release();

    m_system.reset();
    return result;
}

} // namespace esys::os::mp
