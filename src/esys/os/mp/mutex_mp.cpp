/*!
 * \file esys/os/mp/mutex_mp.cpp
 * \brief Definition of the Boost Mutex class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/mp/mutex.h"

namespace esys::os::mp
{

Mutex::Mutex(const base::ObjectName &name, Type type)
    : base::Object(name)
    , MutexPlat(name.get_name(), type)
{
}

Mutex::~Mutex() = default;

} // namespace esys::os::mp
