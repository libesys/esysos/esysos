/*!
 * \file esys/os/mp/platformbase_mp.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/mp/platformbase.h"

namespace esys::os::mp
{

PlatformBase::PlatformBase() = default;

PlatformBase::~PlatformBase() = default;

} // namespace esys::os::mp
