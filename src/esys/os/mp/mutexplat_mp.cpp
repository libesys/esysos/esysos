/*!
 * \file esys/os/mp/mutexplat_mp.cpp
 * \brief Definition of the Multi-Platform Mutex class
 *
 * \cond
 *__legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 *__legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/mp/mutexplat.h"
#include "esys/os/mp/platform.h"

namespace esys::os::mp
{

MutexPlat::MutexPlat(const std::string &name, Type type)
    : MutexIf()
    , ObjPlatIf(name)
    , m_type(type)
{
}

MutexPlat::~MutexPlat() = default;

void MutexPlat::set_type(Type type)
{
    m_type = type;
}

MutexIf::Type MutexPlat::get_type() const
{
    return m_type;
}

int MutexPlat::plat_init()
{
    assert(m_mutex == nullptr);
    m_mutex = Platform::get().new_mutex(get_name(), get_type());
    assert(m_mutex != nullptr);

    if (m_mutex == nullptr) return -1;

    return m_mutex->plat_init();
}

int MutexPlat::plat_release()
{
    if (m_mutex == nullptr) return -1;

    int result = m_mutex->plat_release();

    m_mutex.reset();
    return result;
}

int MutexPlat::lock(bool from_isr)
{
    assert(m_mutex != nullptr);

    return m_mutex->lock(from_isr);
}

int MutexPlat::unlock(bool from_isr)
{
    assert(m_mutex != nullptr);

    return m_mutex->unlock(from_isr);
}

int MutexPlat::try_lock(bool from_isr)
{
    assert(m_mutex != nullptr);

    return m_mutex->try_lock(from_isr);
}

} // namespace esys::os::mp
