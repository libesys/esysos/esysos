/*!
 * \file esys/os/mp/taskplat_mp.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/mp/taskplat.h"
#include "esys/os/mp/platform.h"

namespace esys::os::mp
{

TaskPlat::TaskPlat()
    : TaskPlatIf()
    , ObjPlatIf()
{
}

TaskPlat::TaskPlat(const char *name)
    : TaskPlatIf()
    , ObjPlatIf(name)
{
}

TaskPlat::~TaskPlat() = default;

int TaskPlat::plat_init()
{
    assert(m_task_plat == nullptr);
    m_task_plat = Platform::get().new_task_plat(get_name());
    assert(m_task_plat != nullptr);

    if (m_task_plat == nullptr) return -1;

    m_task_plat->set_task_base(get_task_base());
    return m_task_plat->plat_init();
}

int TaskPlat::plat_release()
{
    if (m_task_plat == nullptr) return -1;

    int result = m_task_plat->plat_release();

    m_task_plat.reset();
    return result;
}

int32_t TaskPlat::start()
{
    assert(m_task_plat != nullptr);

    return m_task_plat->start();
}

int32_t TaskPlat::stop()
{
    assert(m_task_plat != nullptr);

    return m_task_plat->stop();
}

int32_t TaskPlat::kill()
{
    assert(m_task_plat != nullptr);

    return m_task_plat->kill();
}

void TaskPlat::sleep(uint32_t ms)
{
    assert(m_task_plat != nullptr);

    return m_task_plat->sleep(ms);
}

void TaskPlat::usleep(uint32_t us)
{
    assert(m_task_plat != nullptr);

    return m_task_plat->usleep(us);
}

void TaskPlat::set_task_base(os::TaskBase *task_base)
{
    m_task_base = task_base;

    if (m_task_plat == nullptr) return;
    m_task_plat->set_task_base(task_base);
}

TaskBase *TaskPlat::get_task_base()
{
    return m_task_base;
}

void TaskPlat::exit_system(int result)
{
    assert(m_task_plat != nullptr);

    m_task_plat->exit_system(result);
}

} // namespace esys::os::mp
