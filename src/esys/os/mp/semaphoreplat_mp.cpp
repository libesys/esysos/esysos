/*!
 * \file esys/os/mp/semaphoreplat_mp.cpp
 * \brief Implementation of the Multi-Platform SemaphorePlat class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/mp/semaphoreplat.h"
#include "esys/os/mp/platform.h"

namespace esys::os::mp
{

SemaphorePlat::SemaphorePlat(const std::string &name, int count)
    : SemaphoreIf()
    , ObjPlatIf(name)
{
}

SemaphorePlat::~SemaphorePlat() = default;

void SemaphorePlat::set_init_count(int init_count)
{
    m_init_count = init_count;
}

int SemaphorePlat::get_init_count() const
{
    return m_init_count;
}

void SemaphorePlat::set_max_count(int max_count)
{
    m_max_count = max_count;
}

int SemaphorePlat::get_max_count() const
{
    return m_max_count;
}

int SemaphorePlat::get_count() const
{
    assert(m_semaphore != nullptr);
    if (m_semaphore == nullptr) return -1;

    return m_semaphore->get_count();
}

int SemaphorePlat::plat_init()
{
    assert(m_semaphore == nullptr);
    m_semaphore = Platform::get().new_semaphore(get_name(), get_init_count());
    assert(m_semaphore != nullptr);

    if (m_semaphore == nullptr) return -1;

    m_semaphore->set_max_count(get_max_count());
    return m_semaphore->plat_init();
}

int SemaphorePlat::plat_release()
{
    if (m_semaphore == nullptr) return -1;

    int result = m_semaphore->plat_release();

    m_semaphore.reset();
    return result;
}

int SemaphorePlat::post(bool from_isr)
{
    assert(m_semaphore != nullptr);

    if (m_semaphore == nullptr) return -1;
    return m_semaphore->post(from_isr);
}

int SemaphorePlat::wait(bool from_isr)
{
    assert(m_semaphore != nullptr);

    if (m_semaphore == nullptr) return -1;
    return m_semaphore->wait(from_isr);
}

int SemaphorePlat::try_wait(bool from_isr)
{
    assert(m_semaphore != nullptr);

    if (m_semaphore == nullptr) return -1;
    return m_semaphore->try_wait(from_isr);
}

int SemaphorePlat::wait_for(int ms, bool from_isr)
{
    assert(m_semaphore != nullptr);

    if (m_semaphore == nullptr) return -1;
    return m_semaphore->wait_for(ms, from_isr);
}

} // namespace esys::os::mp
