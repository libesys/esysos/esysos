/*!
 * \file esys/os/esysc/test/emptytest.cpp
 * \brief Empty test to test the CI
 *
 * \cond
 * __legal_b__
 *
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysc/test/esysos_esysc_t_prec.h"

#include <iostream>

namespace esys::os::esysc::test
{

/*! \class EmpyTest sys/os/esysc/test/emptytest.cpp "sys/os/esysc/test/emptytest.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(EmptyTest)
{
}

} // namespace esys::os::esysc::test
