/*!
 * \file esys/os/esysc/test/testcasectrl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysc/test/esysos_esysc_t_prec.h"
#include "esys/os/esysc/test/testcasectrl.h"

#include <iostream>
#include <cassert>

namespace esys::os::esysc::test
{

TestCaseCtrl *TestCaseCtrl::g_test_case = nullptr;

TestCaseCtrl &TestCaseCtrl::get()
{
    assert(g_test_case != nullptr);

    return *g_test_case;
}

TestCaseCtrl::TestCaseCtrl()
    : esystest::TestCaseCtrl()
{
    g_test_case = this;

    AddSearchPathEnvVar("ESYSOS_FREERTOS");
    AddSearchPath("res/esysos_esysc_t");                  // If cwd is root of the emdev git repo
    AddSearchPath("../../src/esysos/res/esysos_esysc_t"); // if cwd is the bin folder

    m_logger.Set(&std::cout);
    esystest::Logger::Set(&m_logger);
}

TestCaseCtrl::~TestCaseCtrl()
{
}

ESYSTEST_GLOBAL_FIXTURE(TestCaseCtrl);

} // namespace esys::os::esysc::test
