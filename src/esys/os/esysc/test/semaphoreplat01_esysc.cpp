/*!
 * \file esys/os/esysc/test/semaphoreplat01_esysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysc/test/esysos_esysc_t_prec.h"
#include "esys/os/esysc/test/fix/system.h"

#include <esys/os/esysc/semaphoreplat.h>
#include <esys/os/esysc/taskplat.h>
#include <esys/os/taskbase.h>

#include <iostream>

namespace esys::os::esysc::test
{

namespace semaphore_plat01
{

class MyTask : public TaskBase, public TaskPlat
{
public:
    MyTask();

    int plat_init() override;

    int entry() override;

protected:
    SemaphorePlat m_sem{"TestSem", 0};
};

MyTask::MyTask()
    : TaskBase()
    , TaskPlat("MyTask")
{
    set_task_base(this);
    set_kind(TaskKind::STANDALONE);
}

int MyTask::plat_init()
{
    int result = TaskPlat::plat_init();
    if (result < 0) return result;

    result = m_sem.plat_init();
    return result;
}

int MyTask::entry()
{
    ESYSTEST_REQUIRE_EQUAL(m_sem.get_count(), 0);

    ESYSTEST_REQUIRE_EQUAL(m_sem.post(), 0);

    ESYSTEST_REQUIRE_EQUAL(m_sem.get_count(), 1);

    ESYSTEST_REQUIRE_EQUAL(m_sem.wait(), 0);

    ESYSTEST_REQUIRE_EQUAL(m_sem.try_wait(), -1);

    ESYSTEST_REQUIRE_EQUAL(m_sem.post(), 0);

    ESYSTEST_REQUIRE_EQUAL(m_sem.try_wait(), 0);
    return 0;
}

/*! \class SemaphorePlat01ESysC esys/os/esysc/test/semaphoreplat01_esysc.cpp
 * "esys/os/esysc/test/semaphoreplat01_esysc.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(SemaphorePlat01ESysC)
{
    fix::System system;
    MyTask task;

    int result = system.init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = task.plat_init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = system.run();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = task.plat_release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = system.release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);
}

} // namespace semaphore_plat01

} // namespace esys::os::esysc::test
