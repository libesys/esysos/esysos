/*!
 * \file esys/os/esysc/test/fix/system_esysc_fix.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysc/test/esysos_esysc_t_prec.h"
#include "esys/os/esysc/test/fix/system.h"

#include <systemc.h>

namespace esys::os::esysc::test::fix
{

System::System()
    : SystemIf()
    , sc_simulation()
{
    sc_simulation::set_display_copyright_message(false);
}

System::~System()
{
}

int System::init()
{
    return 0;
}

int System::run()
{
    return sc_simulation::run();
}

int System::release()
{
    return 0;
}

int System::main()
{
    sc_start();
    return 0;
}

} // namespace esys::os::esysc::test::fix
