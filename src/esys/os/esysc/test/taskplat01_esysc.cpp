/*!
 * \file esys/os/esysc/test/taskplat01_esysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysc/test/esysos_esysc_t_prec.h"
#include "esys/os/esysc/test/fix/system.h"

#include <esys/os/esysc/semaphoreplat.h>
#include <esys/os/esysc/taskplat.h>
#include <esys/os/taskbase.h>

#include <iostream>

namespace esys::os::esysc::test
{

namespace taskplat01
{

class MyTask : public TaskBase, public TaskPlat
{
public:
    MyTask();

    int plat_init() override;

    int entry() override;

    void wait_done();

    int get_value() const;

protected:
    SemaphorePlat m_sem_done{"SemDone", 0};
    int m_value = 0;
};

MyTask::MyTask()
    : TaskBase()
    , TaskPlat("MyTask")
{
    set_task_base(this);
    set_kind(TaskKind::STANDALONE);
}

int MyTask::plat_init()
{
    int result = TaskPlat::plat_init();
    if (result < 0) return result;

    result = m_sem_done.plat_init();
    return result;
}

int MyTask::entry()
{
    ++m_value;
    sleep(1000);

    m_sem_done.post();
    return 0;
}

void MyTask::wait_done()
{
    m_sem_done.wait();
}

int MyTask::get_value() const
{
    return m_value;
}

/*! \class TaskPlat01ESysC esys/os/esysc/test/taskplat01_esysc.cpp
 * "esys/os/esysc/test/taskplat01_esysc.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(TaskPlat01ESysC)
{
    fix::System system;
    MyTask task;

    int result = system.init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = task.plat_init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = system.run();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    task.wait_done();
    ESYSTEST_REQUIRE_EQUAL(task.get_value(), 1);

    result = task.plat_release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = system.release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);
}

} // namespace taskplat01

} // namespace esys::os::esysc::test
