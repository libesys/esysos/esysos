/*!
 * \file esys/os/esysc/test/taskplat02_esysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysc/test/esysos_esysc_t_prec.h"
#include "esys/os/esysc/test/fix/system.h"

#include <esys/os/esysc/semaphoreplat.h>
#include <esys/os/esysc/taskplat.h>
#include <esys/os/esysc/time.h>
#include <esys/os/taskbase.h>

#include <iostream>

namespace esys::os::esysc::test
{

namespace taskplat02
{

class MyTask : public TaskBase, public TaskPlat
{
public:
    MyTask(bool master);

    int plat_init() override;

    int entry() override;
    int master();
    int slave();

    void wait_done();

    void set_sem_rem(SemaphorePlat *sem_rem);
    SemaphorePlat *get_sem_rem();

    SemaphorePlat *get_sem();

protected:
    bool m_master = false;
    SemaphorePlat m_sem{"Sem", 0};
    SemaphorePlat *m_sem_rem = nullptr;
    SemaphorePlat m_sem_done{"SemDone", 0};
    int m_value = 0;
};

MyTask::MyTask(bool master)
    : TaskBase()
    , TaskPlat()
    , m_master(master)
{
    set_task_base(this);
    if (master)
        set_name("MyTaskMaster");
    else
        set_name("MyTaskSlave");
    set_kind(TaskKind::STANDALONE);
}

int MyTask::plat_init()
{
    int result = TaskPlat::plat_init();
    if (result < 0) return result;

    result = m_sem_done.plat_init();
    if (result < 0) return result;

    result = m_sem.plat_init();
    return result;
}

int MyTask::entry()
{
    if (m_master) return master();
    return slave();
}

int MyTask::master()
{
    int result;

    result = m_sem.wait();
    ESYSTEST_MT_REQUIRE_EQUAL(0, result);

    ESYSTEST_MT_REQUIRE_NE(m_sem_rem, nullptr);
    result = m_sem_rem->post();
    ESYSTEST_MT_REQUIRE_EQUAL(result, 0);

    result = m_sem.wait();
    ESYSTEST_MT_REQUIRE_EQUAL(result, 0);

    ESYSTEST_MT_REQUIRE_GE(Time::millis(), (uint32_t)200);

    m_sem_done.post();
    return 0;
}

int MyTask::slave()
{
    int result;
    Time::sleep(100);

    ESYSTEST_MT_REQUIRE_NE(m_sem_rem, nullptr);

    result = m_sem_rem->post();
    ESYSTEST_MT_REQUIRE_EQUAL(result, 0);

    result = m_sem.wait();
    ESYSTEST_MT_REQUIRE_EQUAL(result, 0);

    Time::sleep(100);
    result = m_sem_rem->post();
    ESYSTEST_MT_REQUIRE_EQUAL(result, 0);

    m_sem_done.post();
    return 0;
}

void MyTask::wait_done()
{
    m_sem_done.wait();
}

void MyTask::set_sem_rem(SemaphorePlat *sem_rem)
{
    m_sem_rem = sem_rem;
}

SemaphorePlat *MyTask::get_sem_rem()
{
    return m_sem_rem;
}

SemaphorePlat *MyTask::get_sem()
{
    return &m_sem;
}

/*! \class TaskPlat02ESysC esys/os/esysc/test/taskplat02_esysc.cpp
 * "esys/os/esysc/test/taskplat02_esysc.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(TaskPlat02ESysC)
{
    fix::System system;
    MyTask master{true};
    MyTask slave{false};

    master.set_sem_rem(slave.get_sem());
    slave.set_sem_rem(master.get_sem());

    int result = system.init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = master.plat_init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = slave.plat_init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = system.run();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    slave.wait_done();
    master.wait_done();

    result = slave.plat_release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = master.plat_release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = system.release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);
}

} // namespace taskplat02

} // namespace esys::os::esysc::test
