/*!
 * \file esys/os/esysc/task_esysc.cpp
 * \brief Definition of the FreeRTOS Task class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysc/esysos_esysc_prec.h"
#include "esys/os/esysc/task.h"

namespace esys::os::esysc
{

Task::Task(const base::ObjectName &name)
    : base::Object(name)
    , TaskBase()
    , TaskPlat(name.get_name())
{
    set_task_base(this);
}

Task::~Task() = default;

} // namespace esys::os::esysc
