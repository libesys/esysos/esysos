/*!
 * \file esys/os/esysc/semaphore_esysc.cpp
 * \brief Definition of the FreeRTOS Semaphore class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysc/esysos_esysc_prec.h"
#include "esys/os/esysc/semaphore.h"

namespace esys::os::esysc
{

Semaphore::Semaphore(const base::ObjectName &name, int count)
    : base::Object(name)
    , SemaphorePlat(name.get_name(), count)
{
}

Semaphore::~Semaphore() = default;

} // namespace esys::os::esysc
