/*!
 * \file esys/os/esysc/semaphoreplatimpl.h
 * \brief Header of the SystemC Semaphore PIML class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysc/esysos_esysc_defs.h"
#include "esys/os/semaphoreif.h"

#include <systemc.h>

#include <memory>

namespace esys::os::esysc
{

/*! \class SemaphorePlatImpl esys/os/esysc/semaphoreplatimpl.h "esys/os/esysc/semaphoreplatimpl.h"
 *  \brief the Boost Semaphore PIML class
 */
class ESYSOS_ESYSC_API SemaphorePlatImpl
{
public:
    //! Constructor
    /*!
     * \param[in] count the initial count of the semaphore
     */
    SemaphorePlatImpl(const sc_module_name &name, int count);

    //! Destructor
    ~SemaphorePlatImpl();

    int plat_init();

    int post();
    int wait();
    int try_wait();

    int get_count();

    int wait_for(int ms);

private:
    //!< \cond DOXY_IMPL
    sc_semaphore m_sem;
    //!< \endcond
};

} // namespace esys::os::esysc
