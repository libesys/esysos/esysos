/*!
 * \file esys/os/esysc/systemplat_esysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysc/esysos_esysc_prec.h"
#include "esys/os/esysc/systemplat.h"
#include "esys/os/esysc/systemplatimpl.h"

#include <esys/os/assert.h>

namespace esys::os::esysc
{

SystemPlat::SystemPlat()
    : SystemIf()
{
    m_impl = std::make_unique<SystemPlatImpl>();
}

SystemPlat::~SystemPlat() = default;

int SystemPlat::init()
{
    assert(m_impl != nullptr);

    return m_impl->init();
}

int SystemPlat::run()
{
    assert(m_impl != nullptr);

    return m_impl->run();
}

int SystemPlat::release()
{
    assert(m_impl != nullptr);

    return m_impl->release();
}

} // namespace esys::os::esysc
