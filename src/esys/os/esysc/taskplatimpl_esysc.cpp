/*!
 * \file esys/os/esysc/taskplatimpl_esysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysc/esysos_esysc_prec.h"
#include "esys/os/esysc/taskplatimpl.h"
#include "esys/os/esysc/taskplat.h"

#include <sysc/kernel/sc_spawn.h>
#include <sysc/kernel/sc_dynamic_processes.h>

#include <cassert>

namespace esys::os::esysc
{

TaskPlatImpl::TaskPlatImpl(const sc_module_name &name, TaskPlat *self)
    : sc_module(name)
    , m_self(self)
{
    sc_core::sc_spawn(sc_bind(&TaskPlatImpl::call_entry, this), sc_core::sc_gen_unique_name("call_entry"));
}

TaskPlatImpl::~TaskPlatImpl() = default;

int TaskPlatImpl::plat_init()
{
    return 0;
}

int TaskPlatImpl::plat_release()
{
    return 0;
}

int TaskPlatImpl::start()
{
    return 0;
}

int TaskPlatImpl::stop()
{
    return 0;
}

int32_t TaskPlatImpl::kill()
{
    return 0;
}

void TaskPlatImpl::sleep(uint32_t ms)
{
    wait(sc_time((double)ms, SC_MS));
}

void TaskPlatImpl::usleep(uint32_t us)
{
    wait(sc_time((double)us, SC_US));
}

void TaskPlatImpl::exit_system([[maybe_unused]] int result)
{
    sc_stop();
}

void TaskPlatImpl::call_entry()
{
    assert(m_task != nullptr);

    m_task->started();

    m_task->entry();
    m_task->done();
}

void TaskPlatImpl::set_task_base(os::TaskBase *task)
{
    m_task = task;
}

os::TaskBase *TaskPlatImpl::get_task_base()
{
    return m_task;
}

void TaskPlatImpl::set_name(const std::string &name)
{
    m_name = name;
}

const std::string &TaskPlatImpl::get_name() const
{
    return m_name;
}

TaskPlat *TaskPlatImpl::self()
{
    return m_self;
}

} // namespace esys::os::esysc
