/*!
 * \file esys/os/esysc/mutexplatimpl.cpp
 * \brief Implementation of the ESysC/SystemC Mutex PIMPL class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysc/esysos_esysc_prec.h"
#include "esys/os/esysc/mutexplatimpl.h"

namespace esys::os::esysc
{

MutexPlatImpl::MutexPlatImpl(const sc_module_name &name, MutexIf::Type type)
    : m_type(type)
    , m_mutex(name)
{
}

MutexPlatImpl::~MutexPlatImpl() = default;

void MutexPlatImpl::set_type(MutexIf::Type type)
{
    m_type = type;
}

MutexIf::Type MutexPlatImpl::get_type() const
{
    return m_type;
}

int MutexPlatImpl::plat_init()
{
    return 0;
}

int MutexPlatImpl::plat_release()
{
    return 0;
}

int MutexPlatImpl::lock()
{
    int result;

    result = m_mutex.lock();
    return result;
}

int MutexPlatImpl::unlock()
{
    int result;

    result = m_mutex.unlock();
    return result;
}

int MutexPlatImpl::try_lock()
{
    bool result;

    result = m_mutex.trylock();

    if (result) return 0;
    return -1;
}

} // namespace esys::os::esysc
