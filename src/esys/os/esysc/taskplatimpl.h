/*!
 * \file esys/os/esysc/taskplatimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysc/esysos_esysc_defs.h"
#include "esys/os/taskplatif.h"
#include "esys/os/taskbase.h"
#include "esys/os/esysc/semaphoreplat.h"

#include <systemc.h>

#include <string>

namespace esys::os::esysc
{

// class ESYSOS_API TaskMngrImpl;
class ESYSOS_ESYSC_API TaskPlat;

/*! \class TaskImpl esys/os/esysc/taskplatimpl.h "esys/os/esysc/taskplatimpl.h"
 *  \brief SystemC implementation of a Task
 */
class ESYSOS_ESYSC_API TaskPlatImpl : public sc_module
{
public:
    TaskPlatImpl(const sc_module_name &name, TaskPlat *self);
    ~TaskPlatImpl() override;

    int plat_init();
    int plat_release();

    int start();
    int stop();
    int kill();

    void sleep(uint32_t ms);
    void usleep(uint32_t us);
    void exit_system(int result);

    void call_entry();

    void set_task_base(TaskBase *task);
    TaskBase *get_task_base();

    void set_name(const std::string &name);
    const std::string &get_name() const;

    TaskPlat *self();

private:
    std::string m_name; //!< The name of the task
    TaskPlat *m_self = nullptr;
    os::TaskBase *m_task = nullptr; //!< Pointer to the TaskBase object using this TaskPlatImpl object

    SemaphorePlat m_sem_registered{"RegisteredSem", 0};
};

} // namespace esys::os::esysc
