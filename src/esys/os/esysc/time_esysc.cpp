/*!
 * \file esys/os/esysc/time_esysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysc/esysos_esysc_prec.h"
#include "esys/os/esysc/time.h"

#include <systemc.h>

#include <boost/date_time/posix_time/posix_time.hpp>

namespace esys::os::esysc
{

uint32_t Time::millis(Source source)
{
    double millis = sc_time_stamp().to_seconds() * 1000;

    return static_cast<uint32_t>(millis);
}

uint64_t Time::micros(Source source)
{
    double micros = sc_time_stamp().to_seconds() * 1000000.0;

    return static_cast<uint64_t>(micros);
}

uint32_t Time::micros_low()
{
    double micros = sc_time_stamp().to_seconds() * 1000000.0;
    uint64_t micros_64 = (uint64_t)micros;
    return static_cast<uint32_t>((micros_64 << 32) >> 32);
}

void Time::sleep(uint32_t ms)
{
    wait((double)ms, SC_MS);
}

void Time::usleep(uint32_t us)
{
    wait((double)us, SC_US);
}

static boost::posix_time::ptime s_ptime = ::boost::posix_time::second_clock::universal_time();
static uint32_t s_millis_when_date_time_set = 0;

int32_t Time::set_time(Struct &time)
{
    s_ptime = ::boost::posix_time::ptime(
        s_ptime.date(), ::boost::posix_time::time_duration(time.get_hours(), time.get_minutes(), time.get_seconds()));
    s_millis_when_date_time_set = millis();
    return 0;
}

void Time::get_time(Struct &time)
{
    boost::posix_time::ptime wall_time;
    boost::posix_time::time_duration duration;

    duration = ::boost::posix_time::time_duration(0, 0, (millis() - s_millis_when_date_time_set) / 1000);

    wall_time = s_ptime + duration;
    duration = wall_time.time_of_day();
    time.set_hours(static_cast<uint8_t>(duration.hours()));
    time.set_minutes(static_cast<uint8_t>(duration.minutes()));
    time.set_seconds(static_cast<uint8_t>(duration.seconds()));
}

int32_t Time::set_date(Date &date)
{
    s_ptime = boost::posix_time::ptime(::boost::gregorian::date(date.get_year(), date.get_month(), date.get_day()),
                                       s_ptime.time_of_day());
    s_millis_when_date_time_set = millis();
    return 0;
}

void Time::get_date(Date &date)
{
    boost::posix_time::ptime wall_time;
    boost::posix_time::time_duration duration;

    duration = ::boost::posix_time::time_duration(0, 0, (millis() - s_millis_when_date_time_set) / 1000);

    wall_time = s_ptime + duration;

    date.set_year(static_cast<uint8_t>(wall_time.date().year()));
    date.set_month(static_cast<uint8_t>(wall_time.date().month()));
    date.set_day(static_cast<uint8_t>(wall_time.date().day()));
}

int32_t Time::set_date_time(DateTime &date_time)
{
    int32_t result;

    result = set_date(date_time.get_date());
    if (result < 0) return -1;
    result = set_time(date_time.get_time());
    if (result < 0) return -2;
    return 0;
}

void Time::get_date_time(DateTime &date_time)
{
    set_time(date_time.get_time());
    set_date(date_time.get_date());
}

int32_t Time::start_timestamp()
{
    return 0; // Since always enabled
}

int32_t Time::stop_timestamp()
{
    return 0; // Since always enabled
}

} // namespace esys::os::esysc
