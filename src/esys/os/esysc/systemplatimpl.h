/*!
 * \file esys/os/esysc/systemplatimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysc/esysos_esysc_defs.h"

#include <systemc.h>
#include <sysc/sc_simulation.h>

#include <memory>

namespace esys::os::esysc
{

/*! \class SystemPlatImpl esys/os/esysc/systemplatimpl.h "esys/os/esysc/systemplatimpl.h"
 *  \brief ESysC/SystemC Operating System PIMPL class
 */
class ESYSOS_ESYSC_API SystemPlatImpl : public sc_simulation
{
public:
    //! Default constructor
    SystemPlatImpl();

    //! Destructor
    ~SystemPlatImpl() override;

    int init();

    int run();

    int release();
};

} // namespace esys::os::esysc
