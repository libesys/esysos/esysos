/*!
 * \file esys/os/esysc/mp/platform_esysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysc/esysos_esysc_prec.h"
#include "esys/os/esysc/mp/platform.h"
#include "esys/os/esysc/mutexplat.h"
#include "esys/os/esysc/semaphoreplat.h"
#include "esys/os/esysc/systemplat.h"
#include "esys/os/esysc/taskplat.h"

namespace esys::os::esysc::mp
{

Platform::Platform()
    : os::mp::PlatformBase()
{
}

Platform::~Platform() = default;

std::shared_ptr<MutexIf> Platform::new_mutex(const std::string &name, MutexIf::Type type)
{
    return std::make_shared<MutexPlat>(name, type);
}

std::shared_ptr<SemaphoreIf> Platform::new_semaphore(const std::string &name, int count)
{
    return std::make_shared<SemaphorePlat>(name, count);
}

std::shared_ptr<TaskPlatIf> Platform::new_task_plat(const std::string &name)
{
    return std::make_shared<TaskPlat>(name);
}

std::shared_ptr<SystemIf> Platform::new_system()
{
    return std::make_shared<SystemPlat>();
}

} // namespace esys::os::esysc::mp
