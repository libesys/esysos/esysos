/*!
 * \file esys/os/esysc/mp/plugin_esysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysc/esysos_esysc_prec.h"
#include "esys/os/esysc/mp/plugin.h"
#include "esys/os/esysc/mp/platform.h"
#include "esys/os/esysc/version.h"

DEFINE_ESYSOS_PLUGIN(ESYSOS_ESYSC_API, esys::os::esysc::mp::Plugin);

namespace esys::os::esysc::mp
{

Plugin::Plugin()
    : BaseType()
{
    set_name("esysc");
    set_short_name("esysc");
    set_version(ESYSOS_ESYSC_VERSION_NUM_DOT_STRING);
}

Plugin::~Plugin() = default;

std::shared_ptr<os::mp::PlatformBase> Plugin::get_platform()
{
    return std::make_shared<Platform>();
}

} // namespace esys::os::esysc::mp
