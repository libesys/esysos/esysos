/*!
 * \file esys/os/esysc/semaphoreplat_esysc.cpp
 * \brief Implementation of the SystemC SemaphorePlat class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysc/esysos_esysc_prec.h"
#include "esys/os/esysc/semaphoreplat.h"
#include "esys/os/esysc/semaphoreplatimpl.h"

#include <esys/os/assert.h>

namespace esys::os::esysc
{

SemaphorePlat::SemaphorePlat(const std::string &name, int count)
    : SemaphoreIf()
    , ObjPlatIf(name)
{
}

SemaphorePlat::~SemaphorePlat() = default;

void SemaphorePlat::set_init_count(int init_count)
{
    m_init_count = init_count;
}

int SemaphorePlat::get_init_count() const
{

    return m_init_count;
}

void SemaphorePlat::set_max_count(int max_count)
{
    m_max_count = max_count;
}

int SemaphorePlat::get_max_count() const
{
    return m_max_count;
}

int SemaphorePlat::get_count() const
{
    assert(m_impl != nullptr);

    return m_impl->get_count();
}

int SemaphorePlat::plat_init()
{
    assert(m_impl == nullptr);

    m_impl = std::make_unique<SemaphorePlatImpl>(sc_core::sc_gen_unique_name(get_name()), get_init_count());

    return m_impl->plat_init();
}

int SemaphorePlat::plat_release()
{
    return 0;
}

int SemaphorePlat::post(bool from_isr)
{
    assert(m_impl != nullptr);

    return m_impl->post();
}

int SemaphorePlat::wait(bool from_isr)
{
    assert(m_impl != nullptr);

    return m_impl->wait();
}

int SemaphorePlat::try_wait(bool from_isr)
{
    assert(m_impl != nullptr);

    return m_impl->try_wait();
}

int SemaphorePlat::wait_for(int ms, bool from_isr)
{
    assert(m_impl != nullptr);

    return m_impl->wait_for(ms);
}

} // namespace esys::os::esysc
