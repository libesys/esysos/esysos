/*!
 * \file esys/os/esysc/mutexplat_esysc.cpp
 * \brief Implementation of the ESysC/SystemC Mutex class
 *
 * \cond
 *__legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 *__legal_e__
 * \endcond
 *
 */

#include "esys/os/esysc/esysos_esysc_prec.h"
#include "esys/os/esysc/mutexplat.h"
#include "esys/os/esysc/mutexplatimpl.h"

namespace esys::os::esysc
{

MutexPlat::MutexPlat(const std::string &name, Type type)
    : MutexIf()
    , ObjPlatIf(name)
{
}

MutexPlat::~MutexPlat() = default;

void MutexPlat::set_type(Type type)
{
    m_impl->set_type(type);
}

MutexPlat::Type MutexPlat::get_type() const
{
    return m_impl->get_type();
}

int MutexPlat::plat_init()
{
    assert(m_impl == nullptr);

    m_impl = std::make_unique<MutexPlatImpl>(sc_core::sc_gen_unique_name(get_name()));

    return m_impl->plat_init();
}

int MutexPlat::plat_release()
{
    assert(m_impl != nullptr);

    return m_impl->plat_release();
}

int MutexPlat::lock(bool from_isr)
{
    assert(m_impl != nullptr);

    return m_impl->lock();
}

int MutexPlat::unlock(bool from_isr)
{
    assert(m_impl != nullptr);

    return m_impl->unlock();
}

int MutexPlat::try_lock(bool from_isr)
{
    assert(m_impl != nullptr);

    return m_impl->try_lock();
}

} // namespace esys::os::esysc
