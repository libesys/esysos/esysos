#
# __legal_b__
#
# Copyright (c) 2016-2022 Michel Gillet Distributed under the wxWindows Library
# Licence, Version 3.1. (See accompanying file LICENSE_3_1.txt or copy at
# http://www.wxwidgets.org/about/licence)
#
# __legal_e__
#

project(esysos_esysc CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(Boost 1.66 REQUIRED COMPONENTS filesystem system)

set(ESYSOS_ESYSC_VERSION_MAJOR ${ESYSOS_VERSION_MAJOR})
set(ESYSOS_ESYSC_VERSION_MINOR ${ESYSOS_VERSION_MINOR})
set(ESYSOS_ESYSC_VERSION_PATCH 0)
set(ESYSOS_ESYSC_VERSION
    ${ESYSOS_ESYSC_VERSION_MAJOR}.${ESYSOS_ESYSC_VERSION_MINOR}.${ESYSOS_ESYSC_VERSION_PATCH}
)

include_directories(../../../../include)

set(ESYSOS_ESYSC_CXX_SOURCES
    esysos_esysc_prec.cpp
    mutex_esysc.cpp
    mutexplat_esysc.cpp
    mutexplatimpl_esysc.cpp
    objplatif_esysc.cpp
    semaphore_esysc.cpp
    semaphoreplat_esysc.cpp
    semaphoreplatimpl_esysc.cpp
    systemplat_esysc.cpp
    systemplatimpl_esysc.cpp
    task_esysc.cpp
    taskplat_esysc.cpp
    taskplatimpl_esysc.cpp
    time_esysc.cpp)

set(ESYSOS_ESYSC_MP_CXX_SOURCES mp/platform_esysc.cpp mp/plugin_esysc.cpp)

add_library(esysos_esysc SHARED ${ESYSOS_ESYSC_CXX_SOURCES}
                                ${ESYSOS_ESYSC_MP_CXX_SOURCES})

if(ESYSOS_SHARED)
  set_property(TARGET esysos_esysc PROPERTY VERSION ${ESYSOS_ESYSC_VERSION})
  set_property(TARGET esysos_esysc PROPERTY SOVERSION
                                            ${ESYSOS_ESYSC_VERSION_MAJOR})

  set_target_properties(
    esysos_esysc
    PROPERTIES
      ARCHIVE_OUTPUT_DIRECTORY
      "${CMAKE_BINARY_DIR}/lib/esysos/${ESYSOS_ESYSC_VERSION_MAJOR}/plugins"
      LIBRARY_OUTPUT_DIRECTORY
      "${CMAKE_BINARY_DIR}/lib/esysos/${ESYSOS_ESYSC_VERSION_MAJOR}/plugins")
endif()

target_include_directories(esysos_esysc PUBLIC ../../.. ../../../../include)

target_link_libraries(esysos_esysc PRIVATE Boost::filesystem Boost::system
                                           esysbase esysos SystemC::systemc)

if(ESYSOS_SHARED)
  install(
    TARGETS esysos_esysc
    LIBRARY
      DESTINATION
        ${CMAKE_INSTALL_LIBDIR}/esysos/${ESYSOS_ESYSC_VERSION_MAJOR}/plugins)
endif()

install(TARGETS esysos_esysc LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})

if(ESYSOS_VHW)
  if(COMMAND add_clang_format_target)
    add_clang_format_target(esysos_esysc)
  endif()

  if(COMMAND add_clang_tidy_target)
    add_clang_tidy_target(esysos_esysc)
  endif()
endif()

if(ESYSOS_BUILD_UTS)
  add_subdirectory(test)
endif()
