/*!
 * \file esys/os/esysc/semaphoreplatimpl_esysc.h
 * \brief Implementation of the SystemC Semaphore PIML class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysc/esysos_esysc_prec.h"
#include "esys/os/esysc/semaphoreplatimpl.h"

namespace esys::os::esysc
{

SemaphorePlatImpl::SemaphorePlatImpl(const sc_module_name &name, int count)
    : m_sem(name, count)
{
}

SemaphorePlatImpl::~SemaphorePlatImpl() = default;

int SemaphorePlatImpl::plat_init()
{
    return 0;
}

int SemaphorePlatImpl::get_count()
{
    return m_sem.get_value();
}

int SemaphorePlatImpl::post()
{
    int result;

    result = m_sem.post();
    return result;
}

int SemaphorePlatImpl::wait()
{
    int result;

    result = m_sem.wait();
    return result;
}

int SemaphorePlatImpl::try_wait()
{
    int result;

    result = m_sem.trywait();
    return result;
}

int SemaphorePlatImpl::wait_for(int ms)
{
    int32_t result = try_wait();
    unsigned int count = ms;

    while ((result == -1) && (count > 0))
    {
        ::wait(1, SC_MS);
        --count;

        result = try_wait();
    }

    if (count == 0) return -1;
    return result;
}

} // namespace esys::os::esysc
