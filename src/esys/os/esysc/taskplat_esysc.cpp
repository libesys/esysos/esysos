/*!
 * \file esys/os/esysc/taskplat_esysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysc/esysos_esysc_prec.h"
#include "esys/os/esysc/taskplat.h"
#include "esys/os/esysc/taskplatimpl.h"

namespace esys::os::esysc
{

TaskPlat::TaskPlat()
    : TaskPlatIf()
    , ObjPlatIf()
{
}

TaskPlat::TaskPlat(const std::string &name)
    : TaskPlatIf()
    , ObjPlatIf(name)
{
}

TaskPlat::~TaskPlat() = default;

int TaskPlat::plat_init()
{
    m_impl = std::make_unique<TaskPlatImpl>(sc_core::sc_gen_unique_name(get_name()), this);
    m_impl->set_task_base(m_task_base);
    return 0;
}

int TaskPlat::plat_release()
{
    return 0;
}

int32_t TaskPlat::start()
{
    assert(m_impl != nullptr);

    return m_impl->start();
}

int32_t TaskPlat::stop()
{
    assert(m_impl != nullptr);

    return m_impl->stop();
}

int32_t TaskPlat::kill()
{
    return 0;
}

void TaskPlat::sleep(uint32_t ms)
{
    assert(m_impl != nullptr);

    return m_impl->sleep(ms);
}

void TaskPlat::usleep(uint32_t us)
{
    assert(m_impl != nullptr);

    return m_impl->usleep(us);
}

void TaskPlat::set_task_base(TaskBase *task_base)
{
    m_task_base = task_base;
}

void TaskPlat::exit_system(int result)
{
    assert(m_impl != nullptr);

    m_impl->exit_system(result);
}

TaskPlatImpl *TaskPlat::get_impl()
{
    return m_impl.get();
}

} // namespace esys::os::esysc
