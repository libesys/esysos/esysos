/*!
 * \file esys/os/esysc/mutexplatimpl.h
 * \brief Header of the ESysC/SystemC Mutex PIMPL class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/esysc/esysos_esysc_defs.h"
#include "esys/os/mutexif.h"

#include <systemc.h>

#include <memory>

namespace esys::os::esysc
{

/*! \class MutexPlatImpl esys/os/esysc/mutexplatimpl.h "esys/os/esysc/mutexplatimpl.h"
 *  \brief ESysC/SystemC Mutex class
 */
class ESYSOS_ESYSC_API MutexPlatImpl
{
public:
    MutexPlatImpl(const sc_module_name &name, MutexIf::Type type = MutexIf::Type::DEFAULT);
    ~MutexPlatImpl();

    void set_type(MutexIf::Type type);
    MutexIf::Type get_type() const;

    int plat_init();
    int plat_release();

    int lock();
    int unlock();
    int try_lock();

private:
    MutexIf::Type m_type = MutexIf::Type::DEFAULT;
    sc_mutex m_mutex;
};

} // namespace esys::os::esysc
