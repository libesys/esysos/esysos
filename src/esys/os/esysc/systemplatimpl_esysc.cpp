/*!
 * \file esys/os/esysc/systemplatimpl_esysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysc/esysos_esysc_prec.h"
#include "esys/os/esysc/systemplatimpl.h"

namespace esys::os::esysc
{

SystemPlatImpl::SystemPlatImpl()
    : sc_simulation()
{
    sc_simulation::set_display_copyright_message(false);
}

SystemPlatImpl::~SystemPlatImpl() = default;

int SystemPlatImpl::init()
{
    return 0;
}

int SystemPlatImpl::run()
{
    return sc_simulation::run();
}

int SystemPlatImpl::release()
{
    return 0;
}

} // namespace esys::os::esysc
