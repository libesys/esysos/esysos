/*!
 * \file esys/os/taskbase.cpp
 * \brief The Task
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/taskbase.h"
#include "esys/os/taskmngrif.h"

#include <cassert>

namespace esys::os
{

TaskBase::TaskBase() = default;

TaskBase::~TaskBase() = default;

void TaskBase::set_next(TaskBase *next)
{
    m_next = next;
}

TaskBase *TaskBase::get_next() const
{
    return m_next;
}

void TaskBase::set_prev(TaskBase *prev)
{
    m_prev = prev;
}

TaskBase *TaskBase::get_prev() const
{
    return m_prev;
}

void TaskBase::set_exit(int exit_value)
{
    m_exit_value = exit_value;
}

int TaskBase::get_exit() const
{
    return m_exit_value;
}

void TaskBase::set_stack_size(unsigned int stack_size)
{
    m_stack_size = static_cast<uint16_t>(stack_size);
}

unsigned int TaskBase::get_stack_size() const
{
    return m_stack_size;
}

void TaskBase::set_priority(Priority priority)
{
    m_priority = priority;
}

TaskBase::Priority TaskBase::get_priority() const
{
    assert((m_priority >= Priority::LOWEST0) && (m_priority <= Priority::CRITICAL3));
    return m_priority;
}

void TaskBase::set_mngr_if(TaskMngrIf *mngr_if)
{
    m_mngr_if = mngr_if;
}

TaskMngrIf *TaskBase::get_mngr_if() const
{
    return m_mngr_if;
}

void TaskBase::done()
{
    assert((get_mngr_if() != nullptr) || (get_kind() == TaskKind::STANDALONE));

    set_state(State::STOPPED);
    if (get_mngr_if() != nullptr) get_mngr_if()->done(this);
}

void TaskBase::started()
{
    assert((get_mngr_if() != nullptr) || (get_kind() == TaskKind::STANDALONE));

    if (get_mngr_if() != nullptr) get_mngr_if()->started(this);
}

void TaskBase::set_kind(TaskKind kind)
{
    m_kind = kind;
}

TaskKind TaskBase::get_kind() const
{
    return m_kind;
}

void TaskBase::set_state(State state)
{
    m_state = state;
}

TaskBase::State TaskBase::get_state() const
{
    return m_state;
}

} // namespace esys::os
