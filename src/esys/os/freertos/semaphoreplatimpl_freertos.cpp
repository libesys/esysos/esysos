/*!
 * \file esys/os/freertos/semaphoreplatimpl_freertos.cpp
 * \brief Definition of the FreeRTOS Semaphore platform PIML class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/freertos/esysos_freertos_prec.h"
#include "esys/os/freertos/semaphoreplatimpl.h"

#include <esys/os/assert.h>

namespace esys::os::freertos
{

SemaphorePlatImpl::SemaphorePlatImpl(const char *name, int count)
#ifndef ESYSOS_FREERTOS_PIMPL
    : SemaphoreIf()
#endif
{
}

SemaphorePlatImpl::~SemaphorePlatImpl() = default;

void SemaphorePlatImpl::set_init_count(int init_count)
{
    m_init_count = init_count;
}

int SemaphorePlatImpl::get_init_count() const
{
    return m_init_count;
}

void SemaphorePlatImpl::set_max_count(int max_count)
{
    m_max_count = max_count;
}

int SemaphorePlatImpl::get_max_count() const
{
    return m_max_count;
}

int SemaphorePlatImpl::plat_init()
{
    if (get_max_count() == -1) set_max_count(ConfigDefs::SEMAPHORE_MAX_COUNT);

    m_sem = xSemaphoreCreateCounting(get_max_count(), get_init_count());
    if (m_sem == nullptr) return -1;
    return 0;
}

int SemaphorePlatImpl::plat_release()
{
    if (m_sem == nullptr) return -1;

    vSemaphoreDelete(m_sem);
    m_sem = nullptr;
    return 0;
}

int SemaphorePlatImpl::get_count() const
{
    assert(m_sem != nullptr);

    if (m_sem == nullptr) return -1;

    return uxSemaphoreGetCount(m_sem);
}

int SemaphorePlatImpl::post(bool from_isr)
{
    assert(m_sem != nullptr);

    if (m_sem == nullptr) return -1;

    if (from_isr == false)
    {
        if (xSemaphoreGive(m_sem) != pdTRUE) return -1;
    }
    else
    {
        BaseType_t xHigherPriorityTaskWoken = pdFALSE;

        if (xSemaphoreGiveFromISR(m_sem, &xHigherPriorityTaskWoken) != pdTRUE) return -1;

        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }
    return 0;
}

int SemaphorePlatImpl::wait(bool from_isr)
{
    assert(m_sem != nullptr);

    if (m_sem == nullptr) return -1;

    if (from_isr == false)
    {
        if (xSemaphoreTake(m_sem, portMAX_DELAY) != pdTRUE) return -1;
    }
    else
    {
        BaseType_t xHigherPriorityTaskWoken = pdFALSE;

        if (xSemaphoreTakeFromISR(m_sem, &xHigherPriorityTaskWoken) != pdTRUE) return -1;
        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }
    return 0;
}

int SemaphorePlatImpl::try_wait(bool from_isr)
{
    assert(m_sem != nullptr);
    assert(from_isr == false);

    if ((m_sem == nullptr) || from_isr) return -1;

    if (xSemaphoreTake(m_sem, 0) != pdTRUE) return -1;
    return 0;
}

int SemaphorePlatImpl::wait_for(int ms, bool from_isr)
{
    assert(m_sem != nullptr);

    if (m_sem == nullptr) return -1;

    const TickType_t xDelay = ms / portTICK_PERIOD_MS;

    if (from_isr == false)
    {
        if (xSemaphoreTake(m_sem, xDelay) != pdTRUE) return -1;
    }
    else
    {
        BaseType_t xHigherPriorityTaskWoken = pdFALSE;

        if (xSemaphoreTakeFromISR(m_sem, &xHigherPriorityTaskWoken) != pdTRUE) return -1;
        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }
    return 0;
}

} // namespace esys::os::freertos
