/*!
 * \file esys/os/freertos/taskplat_freertos.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/freertos/esysos_freertos_prec.h"
#include "esys/os/freertos/taskplat.h"
#include "esys/os/freertos/taskplatimpl.h"

#include <esys/os/assert.h>

namespace esys::os::freertos
{

#ifdef ESYSOS_FREERTOS_PIMPL

TaskPlat::TaskPlat()
    : TaskPlatIf()
{
    m_impl = std::make_unique<TaskPlatImpl>(this);
}

TaskPlat::TaskPlat([[maybe_unused]] const char *name)
    : TaskPlatIf()
{
    m_impl = std::make_unique<TaskPlatImpl>(this);
}

TaskPlat::~TaskPlat() = default;

int TaskPlat::plat_init()
{
    return m_impl->plat_init();
}

int TaskPlat::plat_release()
{
    return m_impl->plat_release();
}

int32_t TaskPlat::start()
{
    assert(m_impl != nullptr);

    return m_impl->start();
}

int32_t TaskPlat::stop()
{
    assert(m_impl != nullptr);

    return m_impl->stop();
}

int32_t TaskPlat::kill()
{
    return 0;
}

void TaskPlat::sleep(uint32_t ms)
{
    assert(m_impl != nullptr);

    return m_impl->sleep(ms);
}

void TaskPlat::usleep(uint32_t us)
{
    assert(m_impl != nullptr);

    return m_impl->usleep(us);
}

void TaskPlat::set_task_base(TaskBase *task)
{
    assert(m_impl != nullptr);

    return m_impl->set_task_base(task);
}

void TaskPlat::exit_system(int result)
{
    assert(m_impl != nullptr);

    return m_impl->exit_system(result);
}

TaskPlatImpl *TaskPlat::get_impl()
{
    return m_impl.get();
}

#endif

} // namespace esys::os::freertos
