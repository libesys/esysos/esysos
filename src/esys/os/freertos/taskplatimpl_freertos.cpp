/*!
 * \file esys/os/freertos/taskplatimpl_freertos.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/freertos/esysos_freertos_prec.h"
#include "esys/os/freertos/taskplatimpl.h"
#include "esys/os/freertos/taskplat.h"

#include <cassert>

namespace esys::os::freertos
{

void TaskPlatImpl::call_entry(void *param)
{
    TaskBase *task = static_cast<TaskBase *>(param);

    assert(task != nullptr);

    task->started();

    task->entry();
    task->done();

    vTaskDelete(nullptr);
}

#ifdef ESYSOS_FREERTOS_PIMPL
TaskPlatImpl::TaskPlatImpl(TaskPlat *self)
    : m_self(self)
#else
TaskPlatImpl::TaskPlatImpl(const char *name)
#endif
{
}

TaskPlatImpl::~TaskPlatImpl() = default;

int TaskPlatImpl::plat_init()
{
    assert(get_task_base() != nullptr);
    assert(get_task_base()->get_state() == os::TaskBase::State::INSTANCIATED);

    BaseType_t result = xTaskCreate(
        TaskPlatImpl::call_entry, get_task_base()->get_name(), static_cast<uint16_t>(get_task_base()->get_stack_size()),
        get_task_base(), tskIDLE_PRIORITY + (static_cast<int>(get_task_base()->get_priority()) / 2), &m_created_task);

    if (result == pdPASS) return 0;
    return -1;
}

int TaskPlatImpl::plat_release()
{
    return 0;
}

int TaskPlatImpl::start()
{
    return 0;
}

int TaskPlatImpl::stop()
{
    return 0;
}

int TaskPlatImpl::kill()
{
    return 0;
}

void TaskPlatImpl::sleep(uint32_t ms)
{
    const TickType_t xDelay = ms / portTICK_PERIOD_MS;

    vTaskDelay(xDelay);
}

void TaskPlatImpl::usleep(uint32_t us)
{
    const TickType_t xDelay = (us / 1000) / portTICK_PERIOD_MS;

    vTaskDelay(xDelay);
}

void TaskPlatImpl::set_task_base(TaskBase *task)
{
    m_task_base = task;
}

void TaskPlatImpl::exit_system(int result)
{
    vTaskEndScheduler();
}

os::TaskBase *TaskPlatImpl::get_task_base()
{
    return m_task_base;
}

#ifdef ESYSOS_FREERTOS_PIMPL
TaskPlat *TaskPlatImpl::self()
{
    return m_self;
}
#endif

} // namespace esys::os::freertos
