/*!
 * \file esys/os/freertos/semaphoreplat_freertos.cpp
 * \brief Implementation of the FreeRTOS Semaphore platform class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/freertos/esysos_freertos_prec.h"
#include "esys/os/freertos/semaphoreplat.h"
#include "esys/os/freertos/semaphoreplatimpl.h"

namespace esys::os::freertos
{

#ifdef ESYSOS_FREERTOS_PIMPL

SemaphorePlat::SemaphorePlat(int count)
    : SemaphoreIf()
{
    m_impl = std::make_unique<SemaphorePlatImpl>("", count);
}

SemaphorePlat::SemaphorePlat(const char *name, int count)
    : SemaphoreIf()
{
    m_impl = std::make_unique<SemaphorePlatImpl>(name, count);
}

SemaphorePlat::~SemaphorePlat() = default;

void SemaphorePlat::set_init_count(int count)
{
    return m_impl->set_init_count(count);
}

int SemaphorePlat::get_init_count() const
{
    return m_impl->get_init_count();
}

void SemaphorePlat::set_max_count(int max_count)
{
    m_impl->set_max_count(max_count);
}

int SemaphorePlat::get_max_count() const
{
    return m_impl->get_max_count();
}

int SemaphorePlat::plat_init()
{
    return m_impl->plat_init();
}

int SemaphorePlat::plat_release()
{
    return m_impl->plat_release();
}

int SemaphorePlat::get_count() const
{
    return m_impl->get_count();
}

int SemaphorePlat::post(bool from_isr)
{
    return m_impl->post(from_isr);
}

int SemaphorePlat::wait(bool from_isr)
{
    return m_impl->wait(from_isr);
}

int SemaphorePlat::try_wait(bool from_isr)
{
    return m_impl->try_wait(from_isr);
}

int SemaphorePlat::wait_for(int ms, bool from_isr)
{
    return m_impl->wait_for(ms, from_isr);
}

#endif

} // namespace esys::os::freertos
