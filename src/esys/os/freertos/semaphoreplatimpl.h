/*!
 * \file esys/os/freertos/semaphoreplatimpl.h
 * \brief Header of the FreeRTOS Semaphore platform PIML class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/freertos/esysos_freertos_defs.h"

#include <esys/os/semaphoreif.h>

#include <FreeRTOS.h>
#include <FreeRTOS/task.h>
#include <FreeRTOS/semphr.h>

namespace esys::os::freertos
{

/*! \class SemaphorePlatImpl esys/os/freertos/semaphoreplatimpl.h "esys/os/freertos/semaphoreplatimpl.h"
 *  \brief the FreeRTOS Semaphore platform PIML class
 */
class ESYSOS_FREERTOS_API SemaphorePlatImpl
#ifndef ESYSOS_FREERTOS_PIMPL
    : public SemaphoreIf
#endif
{
public:
    //! Constructor
    /*!
     * \param[in] count the initial count of the semaphore
     */
    explicit SemaphorePlatImpl(const char *name, int count);

    //! Destructor
    virtual ~SemaphorePlatImpl();

    virtual void set_init_count(int init_count);
    virtual int get_init_count() const;

    virtual void set_max_count(int max_count);
    virtual int get_max_count() const;

    virtual int plat_init();
    virtual int plat_release();

    virtual int get_count() const;

    virtual int post(bool from_isr = false);
    virtual int wait(bool from_isr = false);
    virtual int try_wait(bool from_isr = false);

    virtual int wait_for(int ms, bool from_isr = false);

private:
    //!< \cond DOXY_IMPL
    SemaphoreHandle_t m_sem = nullptr;
    int m_init_count = 0;
    int m_max_count = -1;
    //!< \endcond
};

} // namespace esys::os::freertos
