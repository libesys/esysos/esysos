/*!
 * \file esys/os/freertos/test/taskplat01_freertos.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/freertos/test/esysos_freertos_t_prec.h"
#include "esys/os/freertos/test/fix/system.h"

#include <esys/os/freertos/semaphoreplat.h>
#include <esys/os/freertos/taskplat.h>
#include <esys/os/taskbase.h>

#include <FreeRTOS/task.h>
#include <FreeRTOS/timers.h>

#include <esys/os/impl_boost/time.h>

#include <iostream>

namespace esys::os::freertos::test
{

namespace taskplat01
{

class MyTask : public TaskBase, public TaskPlat
{
public:
    MyTask();

    int plat_init() override;

    int entry() override;

    void wait_done();

    int get_value() const;

protected:
    SemaphorePlat m_sem_done{0};
    int m_value = 0;
};

MyTask::MyTask()
    : TaskBase()
    , TaskPlat()
{
    set_task_base(this);
    set_name("MyTask");
    set_kind(TaskKind::STANDALONE);
}

int MyTask::plat_init()
{
    int result = TaskPlat::plat_init();
    if (result < 0) return result;

    result = m_sem_done.plat_init();
    return result;
}

int MyTask::entry()
{
    ++m_value;
    sleep(1000);

    m_sem_done.post();
    vTaskEndScheduler();
    return 0;
}

void MyTask::wait_done()
{
    m_sem_done.wait();
}

int MyTask::get_value() const
{
    return m_value;
}

/*! \class TaskPlat01FreeRTOS esys/os/freertos/test/taskplat01_freertos.cpp
 * "esys/os/freertos/test/taskplat01_freertos.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(TaskPlat01FreeRTOS)
{
    fix::System system;
    MyTask task;

    int result = system.init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = task.plat_init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = system.run();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    task.wait_done();
    ESYSTEST_REQUIRE_EQUAL(task.get_value(), 1);

    result = task.plat_release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = system.release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    impl_boost::Time::sleep(1000);
}

} // namespace taskplat01

} // namespace esys::os::freertos::test
