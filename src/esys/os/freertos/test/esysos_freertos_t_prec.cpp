/*!
 * \file esys/os/freertos/test/esysos_freertos_t_prec.cpp
 * \brief For precompiled headers
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/freertos/test/esysos_freertos_t_prec.h"

// TODO: reference any additional headers you need in esysos_freertos_t_prec.h
// and not in this file
