/*!
 * \file esys/os/freertos/test/task01_freertos.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/freertos/test/esysos_freertos_t_prec.h"

// Note that there is no FreeRTOS include files. but nonly generic ones
#include <esys/os/semaphore.h>
#include <esys/os/systemplat.h>
#include <esys/os/task.h>

// Include needed only to display the structure of the Object
#include <esys/base/stdcpp/objectoperation.h>

#include <iostream>

// We are only in the generic esys::os namesapce, no traces of FreeRTOS
namespace esys::os::test
{

namespace task01
{

class MyTask : public Task
{
public:
    MyTask(const base::ObjectName &name);
    ~MyTask() override;

    int entry() override;

    void wait_done();

    int get_value() const;

protected:
    Semaphore m_sem_done{"sem", 0};
    int m_value = 0;
};

MyTask::MyTask(const base::ObjectName &name)
    : Task(name)
{
    set_kind(TaskKind::STANDALONE);
}

MyTask::~MyTask() = default;

int MyTask::entry()
{
    ++m_value;
    sleep(1000);

    m_sem_done.post();

    // The following is needed to getting out of the OS / Scheduler.
    exit_system();
    return 0;
}

void MyTask::wait_done()
{
    m_sem_done.wait();
}

int MyTask::get_value() const
{
    return m_value;
}

/*! \class Task01FreeRTOS esys/os/freertos/test/task01_freertos.cpp
 * "esys/os/freertos/test/task01_freertos.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(Task01FreeRTOS)
{
    SystemPlat system;
    MyTask task("MyTask");

    // Initialize the Operation System
    int result = system.init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    // Only the top Object of the hierarchy must be initialized:
    // all children Objects will be automatically initialized
    result = task.init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);
    ESYSTEST_REQUIRE_EQUAL(task.get_value(), 0);

    // Run the Operating System
    result = system.run();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    // Just to make sure the task has terminated
    task.wait_done();
    ESYSTEST_REQUIRE_EQUAL(task.get_value(), 1);

    // Release the top Object, which will also release all
    // children Objects
    result = task.release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    // Release the Operating System
    result = system.release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    // Just displaying the Object hierarchy
    base::stdcpp::ObjectOperation op(task);

    result = op.print(std::cout);
    ESYSTEST_REQUIRE_EQUAL(result, 0);
}

} // namespace task01

} // namespace esys::os::test
