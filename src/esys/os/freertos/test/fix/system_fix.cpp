/*!
 * \file esys/os/freertos/test/fix/system_fix.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/freertos/test/esysos_freertos_t_prec.h"
#include "esys/os/freertos/test/fix/system.h"

#include <FreeRTOS/task.h>
#include <FreeRTOS/timers.h>

namespace esys::os::freertos::test::fix
{

System::System()
    : SystemIf()
{
}

System::~System()
{
}

int System::init()
{
    vPortInitHeapRegions();
#ifdef configKERNEL_END_SCHEDULER
    xTaskInit();
    xTimerInit();
    xPortInit();
#endif

    vPortDefineHeapRegions(m_heap_regions);

    vTraceEnable(TRC_START);

    return 0;
}

int System::run()
{
    vTaskStartScheduler();

    return 0;
}

int System::release()
{
    return 0;
}

} // namespace esys::os::freertos::test::fix
