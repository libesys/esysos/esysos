/*!
 * \file esys/os/freertos/test/emptytest.cpp
 * \brief Empty test to test the CI
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/freertos/test/esysos_freertos_t_prec.h"

#include <iostream>

namespace esys::os::freertos::test
{

/*! \class EmpyTest esys/os/freertos/test/emptytest.cpp "esys/os/freertos/test/emptytest.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(EmptyTest)
{
}

} // namespace esys::os::freertos::test
