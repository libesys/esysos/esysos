/*!
 * \file esys/os/freertos/test/semaphoreplat01_freertos.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/freertos/test/esysos_freertos_t_prec.h"
#include "esys/os/freertos/test/fix/system.h"

#include <esys/os/freertos/semaphoreplat.h>

#include <iostream>

namespace esys::os::freertos::test
{

namespace semaphore_plat01
{

/*! \class SemaphorePlat01Boost esys/os/freertos/test/semaphoreplat01_freertos.cpp
 * "esys/os/freertos/test/semaphoreplat01_freertos.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(SemaphorePlat01FreeRTOS)
{
    fix::System system;
    SemaphorePlat sem(0);

    int result = system.init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = sem.plat_init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    ESYSTEST_REQUIRE_EQUAL(sem.get_count(), 0);

    ESYSTEST_REQUIRE_EQUAL(sem.post(), 0);

    ESYSTEST_REQUIRE_EQUAL(sem.get_count(), 1);

    ESYSTEST_REQUIRE_EQUAL(sem.wait(), 0);

    ESYSTEST_REQUIRE_EQUAL(sem.try_wait(), -1);

    ESYSTEST_REQUIRE_EQUAL(sem.post(), 0);

    ESYSTEST_REQUIRE_EQUAL(sem.try_wait(), 0);

    result = sem.plat_release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = system.release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);
}

} // namespace semaphore_plat01

} // namespace esys::os::freertos::test
