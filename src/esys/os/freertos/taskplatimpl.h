/*!
 * \file esys/os/freertos/taskplatimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/freertos/esysos_freertos_defs.h"
#include "esys/os/freertos/semaphoreplat.h"

#include <esys/os/taskplatif.h>
#include <esys/os/taskbase.h>

#include <FreeRTOS.h>
#include <FreeRTOS/task.h>

namespace esys::os::freertos
{

#ifdef ESYSOS_FREERTOS_PIMPL
class ESYSOS_FREERTOS_API TaskPlat;
#endif

/*! \class TaskImpl esys/os/freertos/taskplatimpl.h "esys/os/freertos/taskplatimpl.h"
 *  \brief FreeRTOS implementation of a Task
 */
class ESYSOS_FREERTOS_API TaskPlatImpl
#ifndef ESYSOS_FREERTOS_PIMPL
    : public virtual TaskPlatIf
#endif
{
public:
#ifdef ESYSOS_FREERTOS_PIMPL
    explicit TaskPlatImpl(TaskPlat *self);
#else
    TaskPlatImpl(const char *name);
#endif

    virtual ~TaskPlatImpl();

    virtual int plat_init();
    virtual int plat_release();

    virtual int start();
    virtual int stop();
    virtual int kill();

    virtual void sleep(uint32_t ms);
    virtual void usleep(uint32_t us);

    static void call_entry(void *param);

    virtual void set_task_base(TaskBase *task);
    virtual void exit_system(int result);

    TaskBase *get_task_base();

#ifdef ESYSOS_FREERTOS_PIMPL
    TaskPlat *self();
#endif

private:
#ifdef ESYSOS_FREERTOS_PIMPL
    TaskPlat *m_self = nullptr;
#endif
    os::TaskBase *m_task_base = nullptr; //!< Pointer to the TaskBase object using this TaskPlatImpl object

    SemaphorePlat m_sem_registered{"sem", 0};
    TaskHandle_t m_created_task;
};

} // namespace esys::os::freertos
