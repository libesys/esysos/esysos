/*!
 * \file esys/os/esysos_freertos_prec.cpp
 * \brief For precompiled headers
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/freertos/esysos_freertos_prec.h"

// TODO: reference any additional headers you need in esysos_freertos_prec.h
// and not in this file
