/*!
 * \file esys/os/freertos/mp/plugin_freertos.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/freertos/esysos_freertos_prec.h"
#include "esys/os/freertos/mp/plugin.h"
#include "esys/os/freertos/mp/platform.h"
#include "esys/os/freertos/version.h"

DEFINE_ESYSOS_PLUGIN(ESYSOS_FREERTOS_API, esys::os::freertos::mp::Plugin);

namespace esys::os::freertos::mp
{

Plugin::Plugin()
    : BaseType()
{
    set_name("freertos");
    set_short_name("freertos");
    set_version(ESYSOS_FREERTOS_VERSION_NUM_DOT_STRING);
}

Plugin::~Plugin() = default;

std::shared_ptr<os::mp::PlatformBase> Plugin::get_platform()
{
    return std::make_shared<Platform>();
}
} // namespace esys::os::freertos::mp
