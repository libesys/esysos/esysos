/*!
 * \file esys/os/freertos/mp/platform_freertos.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/freertos/esysos_freertos_prec.h"
#include "esys/os/freertos/mp/platform.h"
#include "esys/os/freertos/mutexplat.h"
#include "esys/os/freertos/semaphoreplat.h"
#include "esys/os/freertos/systemplat.h"
#include "esys/os/freertos/taskplat.h"

namespace esys::os::freertos::mp
{

Platform::Platform()
    : os::mp::PlatformBase()
{
}

Platform::~Platform() = default;

std::shared_ptr<MutexIf> Platform::new_mutex(const std::string &name, MutexIf::Type type)
{
    return std::make_shared<MutexPlat>();
}

std::shared_ptr<SemaphoreIf> Platform::new_semaphore(const std::string &name, int count)
{
    return std::make_shared<SemaphorePlat>(count);
}

std::shared_ptr<TaskPlatIf> Platform::new_task_plat(const std::string &name)
{
    return std::make_shared<TaskPlat>();
}

std::shared_ptr<SystemIf> Platform::new_system()
{
    return std::make_shared<SystemPlat>();
}

} // namespace esys::os::freertos::mp
