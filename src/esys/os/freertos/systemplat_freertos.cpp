/*!
 * \file esys/os/freertos/systemplat_freertos.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/freertos/esysos_freertos_prec.h"
#include "esys/os/freertos/systemplat.h"

#include <FreeRTOS.h>
#include <FreeRTOS/task.h>
#include <FreeRTOS/timers.h>

#ifdef WIN32
//#include <FreeRTOS/app.h>
#endif

namespace esys::os::freertos
{

SystemPlat::SystemPlat()
    : SystemIf()
{
}

SystemPlat::~SystemPlat() = default;

int SystemPlat::init()
{
    int result = init_heap();

#ifdef configKERNEL_END_SCHEDULER
    xTaskInit();
    xTimerInit();
    xPortInit();
#endif

#ifdef ESYSOS_TRACE_FACILITY
    vTraceEnable(TRC_START);
#endif

    return result;
}

int SystemPlat::run()
{
    vTaskStartScheduler();

    return 0;
}

int SystemPlat::release()
{
    return 0;
}

int SystemPlat::init_heap()
{
    return init_heap5();
}

int SystemPlat::init_heap5()
{
    static const size_t mainREGION_1_SIZE = 2001;
    static const size_t mainREGION_2_SIZE = 18005;
    static const size_t mainREGION_3_SIZE = 1007;
    static const uint32_t AdditionalOffset = 19;

    static uint8_t heap[configTOTAL_HEAP_SIZE];

    const HeapRegion_t heap_regions[4] = {{heap + 1, mainREGION_1_SIZE},
                                          {heap + 15 + mainREGION_1_SIZE, mainREGION_2_SIZE},
                                          {heap + 19 + mainREGION_1_SIZE + mainREGION_2_SIZE, mainREGION_3_SIZE},
                                          {nullptr, 0}};

    vPortInitHeapRegions();

    vPortDefineHeapRegions(heap_regions);

    return 0;
}

} // namespace esys::os::freertos
