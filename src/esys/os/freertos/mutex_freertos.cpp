/*!
 * \file esys/os/freertos/mutex_freertos.cpp
 * \brief Definition of the FreeRTOS Mutex class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/freertos/esysos_freertos_prec.h"
#include "esys/os/freertos/mutex.h"

namespace esys::os::freertos
{

Mutex::Mutex(const base::ObjectName &name, Type type)
    : base::Object(name)
    , MutexPlat(type)
{
}

Mutex::~Mutex() = default;

} // namespace esys::os::freertos
