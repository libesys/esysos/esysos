/*!
 * \file esys/os/freertos/mutexplatimpl.h
 * \brief Header of the FreeRTOS Mutex PIMPL class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/os/freertos/esysos_freertos_defs.h"
#include "esys/os/mutexif.h"

#include <FreeRTOS.h>
#include <FreeRTOS/task.h>
#include <FreeRTOS/semphr.h>

namespace esys::os::freertos
{

/*! \class MutexPlatImpl esys/os/freertos/mutexplatimpl.h "esys/os/freertos/mutexplatimpl.h"
 *  \brief FreeRTOS Mutex class
 */
class ESYSOS_FREERTOS_API MutexPlatImpl
#ifndef ESYSOS_FREERTOS_PIMPL
    : public MutexIf
#endif
{
public:
    MutexPlatImpl(MutexIf::Type type);
    virtual ~MutexPlatImpl();

    virtual void set_type(MutexIf::Type type);
    virtual MutexIf::Type get_type() const;

    virtual int plat_init();
    virtual int plat_release();

    virtual int lock(bool from_isr = false);
    virtual int unlock(bool from_isr = false);
    virtual int try_lock(bool from_isr = false);

private:
    MutexIf::Type m_type = MutexIf::Type::DEFAULT;
    SemaphoreHandle_t m_mutex = nullptr;
};

} // namespace esys::os::freertos
