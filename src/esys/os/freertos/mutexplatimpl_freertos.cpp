/*!
 * \file esys/os/freertos/mutexplatimpl_freertos.cpp
 * \brief Source of the FreeRTOS Mutex PIMPL class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/freertos/esysos_freertos_prec.h"
#include "esys/os/freertos/mutexplatimpl.h"
#include "esys/os/assert.h"

namespace esys::os::freertos
{

MutexPlatImpl::MutexPlatImpl(MutexIf::Type type)
#ifndef ESYSOS_FREERTOS_PIMPL
    : MutexIf()
    , m_type(type)
#else
    : m_type(type)
#endif
{
}

MutexPlatImpl::~MutexPlatImpl() = default;

void MutexPlatImpl::set_type(MutexIf::Type type)
{
    m_type = type;
}

MutexIf::Type MutexPlatImpl::get_type() const
{
    return m_type;
}

int MutexPlatImpl::plat_init()
{
    int result = 0;

    if (m_type == MutexIf::Type::DEFAULT)
        m_mutex = xSemaphoreCreateMutex();
    else
        m_mutex = xSemaphoreCreateRecursiveMutex();
    if (m_mutex == nullptr) result = -1;

    return result;
}

int MutexPlatImpl::plat_release()
{

    assert(m_mutex != nullptr);

    if (m_mutex != nullptr)
    {
        vSemaphoreDelete(m_mutex);
        m_mutex = nullptr;
        return 0;
    }
    return -1;
}

int MutexPlatImpl::lock(bool from_isr)
{
    int result = 0;

    assert(m_mutex != nullptr);

    if (from_isr == false)
    {
        if (m_type == MutexIf::Type::DEFAULT)
        {
            if (xSemaphoreTake(m_mutex, portMAX_DELAY) != pdTRUE) result = -1;
        }
        else
        {
            if (xSemaphoreTakeRecursive(m_mutex, portMAX_DELAY) != pdTRUE) result = -1;
        }
    }
    else
    {
        assert(m_type == MutexIf::Type::DEFAULT);

        BaseType_t xTaskWoken = pdFALSE;
        if (xSemaphoreTakeFromISR(m_mutex, &xTaskWoken) != pdTRUE)
            result = -1;
        else
            portYIELD_FROM_ISR(xTaskWoken);
    }
    return result;
}

int MutexPlatImpl::unlock(bool from_isr)
{
    int result = 0;

    assert(m_mutex != nullptr);

    if (from_isr == false)
    {
        if (m_type == MutexIf::Type::DEFAULT)
        {
            if (xSemaphoreGive(m_mutex) != pdTRUE) result = -1;
        }
        else
        {
            if (xSemaphoreGiveRecursive(m_mutex) != pdTRUE) result = -1;
        }
    }
    else
    {
        assert(m_type == MutexIf::Type::DEFAULT);

        BaseType_t xHigherPriorityTaskWoken = pdFALSE;

        if (xSemaphoreGiveFromISR(m_mutex, &xHigherPriorityTaskWoken) != pdTRUE)
            result = -1;
        else
            portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }
    return result;
}

int MutexPlatImpl::try_lock(bool from_isr)
{
    assert(m_mutex != nullptr);
    assert(from_isr != true);

    if (m_type == MutexIf::Type::DEFAULT)
    {
        if (xSemaphoreTake(m_mutex, 0) != pdTRUE) return -1;
    }
    else
    {
        if (xSemaphoreTakeRecursive(m_mutex, 0) != pdTRUE) return -1;
    }
    return 0;
}

} // namespace esys::os::freertos
