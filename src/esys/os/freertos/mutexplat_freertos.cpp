/*!
 * \file esys/os/freertos/mutexplat_freertos.cpp
 * \brief Definition of the FreeRTOS Mutex class
 *
 * \cond
 *__legal_b__
 *
 * Copyright (c) 2016-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 *__legal_e__
 * \endcond
 *
 */

#include "esys/os/freertos/esysos_freertos_prec.h"
#include "esys/os/freertos/mutexplat.h"
#include "esys/os/freertos/mutexplatimpl.h"

namespace esys::os::freertos
{

#ifdef ESYSOS_FREERTOS_PIMPL

MutexPlat::MutexPlat(Type type)
    : MutexIf()
{
    m_impl = std::make_unique<MutexPlatImpl>(type);
}

MutexPlat::~MutexPlat() = default;

void MutexPlat::set_type(Type type)
{
    m_impl->set_type(type);
}

MutexPlat::Type MutexPlat::get_type() const
{
    return m_impl->get_type();
}

int MutexPlat::plat_init()
{
    return m_impl->plat_init();
}

int MutexPlat::plat_release()
{
    return m_impl->plat_release();
}

int MutexPlat::lock(bool from_isr)
{
    return m_impl->lock(from_isr);
}

int MutexPlat::unlock(bool from_isr)
{
    return m_impl->unlock(from_isr);
}

int MutexPlat::try_lock(bool from_isr)
{
    return m_impl->try_lock(from_isr);
}

#endif

} // namespace esys::os::freertos
