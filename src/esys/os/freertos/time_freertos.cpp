/*!
 * \file esys/os/freertos/time_freertos.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/freertos/esysos_freertos_prec.h"
#include "esys/os/freertos/time.h"

#include <esys/os/config.h>

#if defined(ESYSOS_VHW)
#include <esys/os/impl_boost/time.h>
#endif

#include <FreeRTOS.h>
#include <FreeRTOS/task.h>

namespace esys::os::freertos
{

uint32_t Time::millis(Source source)
{
    if (source == DEFAULT_CLK) source = get_dft_time_source();

    return xTaskGetTickCount() / portTICK_PERIOD_MS;
}

uint64_t Time::micros(Source source)
{
    if (source == DEFAULT_CLK) source = get_dft_time_source();

    if (source == MCU_CLK_BASED)
    {
        uint64_t result = xTaskGetTickCount() * 1000;
        result = result / portTICK_PERIOD_MS;
        return result;
    }
    else
    {
        uint64_t result = xTaskGetTickCount() * 1000;
        result = result / portTICK_PERIOD_MS;
        return result;
    }
}

uint32_t Time::micros_low()
{
    uint64_t us = micros();
    us = (us << 32) >> 32;
    return (uint32_t)us;
}

void Time::sleep(uint32_t ms)
{
    const TickType_t xDelay = ms / portTICK_PERIOD_MS;

    vTaskDelay(xDelay);
}

void Time::usleep(uint32_t us)
{
    /*! \todo find a way to directly wait us and not converting to ms
     */
    const TickType_t xDelay = (us / 1000) / portTICK_PERIOD_MS;

    vTaskDelay(xDelay);
}

int32_t Time::set_time(Struct &time)
{
#if defined(ESYSOS_HW)
    return -1;
#elif defined(ESYSOS_VHW)
    return impl_boost::Time::set_time(time);
#else
    return -1;
#endif
}

void Time::get_time(Struct &time)
{
#if defined(ESYSOS_HW)
    return;
#elif defined(ESYSOS_VHW)
    impl_boost::Time::get_time(time);
#else
    return;
#endif
}

int32_t Time::set_date(Date &date)
{
#if defined(ESYSOS_HW)
    return -1;
#elif defined(ESYSOS_VHW)
    return impl_boost::Time::set_date(date);
#else
    return -1;
#endif
}

void Time::get_date(Date &date)
{
#if defined(ESYSOS_HW)
#elif defined(ESYSOS_VHW)
    impl_boost::Time::set_date(date);
#else
#endif
}

int32_t Time::set_date_time(DateTime &date_time)
{
    int32_t result;

    result = set_date(date_time.get_date());
    if (result < 0) return -1;
    result = set_time(date_time.get_time());
    if (result < 0) return -2;
    return 0;
}

void Time::get_date_time(DateTime &date_time)
{
    set_time(date_time.get_time());
    set_date(date_time.get_date());
}

int32_t Time::start_timestamp()
{
    return -1;
}

int32_t Time::stop_timestamp()
{
    return -1;
}

} // namespace esys::os::freertos
