/*!
 * \file esys/os/objplatif.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/objplatif.h"

namespace esys::os
{

ObjPlatIf::ObjPlatIf() = default;

ObjPlatIf::~ObjPlatIf() = default;

void ObjPlatIf::set_name(const char *name)
{
    // This is empty and must be overritten in platform requiring it
}

const char *ObjPlatIf::get_name() const
{
    return nullptr;
}

} // namespace esys::os
