/*!
 * \file esys/os/test/emptytest.cpp
 * \brief Empty test to test the CI
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/test/esysos_t_prec.h"

#include <iostream>

namespace esys::os::test
{

/*! \class EmpyTest esys/os/test/emptytest.cpp "esys/os/test/emptytest.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(EmptyTest)
{
}

} // namespace esys::os::test
