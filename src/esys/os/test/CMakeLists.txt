#
# cmake-format: off
# __legal_b__
#
# Copyright (c) 2022 Michel Gillet
# Distributed under the wxWindows Library Licence, Version 3.1.
# (See accompanying file LICENSE_3_1.txt or
# copy at http://www.wxwidgets.org/about/licence)
#
# __legal_e__
# cmake-format: on
#

project(esysos_t CXX)

include(GNUInstallDirs)

add_definitions(-DESYSTEST_MULTIOS -DESYSTEST_USE_BOOST)
set(ESYSTEST_USE_BOOST 1)

include_directories(../../../../include ${termcolor_DIR}/include)

set(ESYSOS_T_CXX_SOURCES emptytest.cpp esysos_t_prec.cpp main.cpp
                         systemplat01.cpp testcasectrl.cpp)

set(ESYSOS_T_IMPL_BOOST_CXX_SOURCES
    ../impl_boost/test/semaphoreplat01_boost.cpp
    ../impl_boost/test/systemexit01_boost.cpp
    ../impl_boost/test/taskplat01_boost.cpp
    ../impl_boost/test/taskplat02_boost.cpp)

set(ESYSOS_T_MP_IMPL_BOOST_CXX_SOURCES
    ../mp/test/mutexplat01_mp.cpp ../mp/test/pluginmngr01.cpp
    ../mp/test/semaphoreplat01_mp.cpp ../mp/test/taskplat01_mp.cpp)

add_executable(
  esysos_t ${ESYSOS_T_CXX_SOURCES} ${ESYSOS_T_IMPL_BOOST_CXX_SOURCES}
           ${ESYSOS_T_MP_IMPL_BOOST_CXX_SOURCES})

find_package(Boost 1.66 REQUIRED QUIET
             COMPONENTS date_time filesystem iostreams program_options thread)

target_link_libraries(
  esysos_t PUBLIC Boost::date_time Boost::filesystem Boost::iostreams
                  Boost::program_options Boost::thread esysos)

add_dependencies(esysos_t esysos_esysc esysos_freertos)

target_link_libraries(esysos_t PUBLIC esystest)

install(TARGETS esysos_t RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

install(
  DIRECTORY ../../../../res/esysos_t
  DESTINATION share
  PATTERN "../../../../res/esysos_t/*")

add_test(
  NAME esysos_t
  WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}
  COMMAND esysos_t -l test_suite)

if(DEFINED ESYSTEST_USE_BOOST)
  add_definitions(-DBOOST_ALL_DYN_LINK)
  find_package(Boost 1.66 REQUIRED QUIET COMPONENTS unit_test_framework)
  target_link_libraries(esysos_t PUBLIC Boost::unit_test_framework)
endif()
