/*!
 * \file esys/os/test/systemplat01.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/test/esysos_t_prec.h"

#include <esys/os/taskplat.h>
#include <esys/os/systemplat.h>
#include <esys/os/taskbase.h>

#include <iostream>

namespace esys::os::test
{

namespace system_plat01
{

class MyTask : public TaskBase, public TaskPlat
{
public:
    MyTask(const char *name);

    int entry() override;

    int get_value() const;

private:
    int m_value = 0;
};

MyTask::MyTask(const char *name)
    : TaskBase()
    , TaskPlat(name)
{
    set_task_base(this);
    set_kind(TaskKind::STANDALONE);
}

int MyTask::entry()
{
    sleep(100);
    ++m_value;
    sleep(100);
    return 0;
}

int MyTask::get_value() const
{
    return m_value;
}

/*! \class SystemPlat01 esys/os/test/systemplat01.cpp "esys/os/test/systemplat01.cpp"
 *
 *  \brief
 */
ESYSTEST_AUTO_TEST_CASE(SystemPlat01)
{
    SystemPlat system;
    MyTask task("mytask");

    int result = system.init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = task.plat_init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = system.run();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    ESYSTEST_REQUIRE_EQUAL(task.get_value(), 1);

    result = task.plat_release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    result = system.release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);
}

} // namespace system_plat01

} // namespace esys::os::test
