/*!
 * \file esys/os/taskmngrif.cpp
 * \brief The Task Manager
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/os/esysos_prec.h"
#include "esys/os/taskmngrif.h"

namespace esys::os
{

TaskMngrIf::TaskMngrIf() = default;

TaskMngrIf::~TaskMngrIf() = default;

} // namespace esys::os
