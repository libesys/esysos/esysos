import jinja2
import os
import sys

print("Creating RC file ...")

script_path = os.path.dirname(__file__)
input_path = os.path.join(script_path, "../../res")
output_path = input_path
template_file = sys.argv[2] + ".j2"
output_file = sys.argv[2] + ".rc"

print("Template file : %s" % template_file)
print("Output file   : %s" % output_file)

loader = jinja2.FileSystemLoader(searchpath=input_path)
env = jinja2.Environment(loader=loader)

template = env.get_template(template_file)

if "TargetFileName" in os.environ:
    print("TargetFileName : %s" % os.environ["TargetFileName"])
    target_file_name = os.environ["TargetFileName"]
else:
    target_file_name = sys.argv[1]

data = {
    "target_file_name": target_file_name,
}

# this is where to put args to the template renderer
output_text = template.render(data)

# print(output_text)

output_path = os.path.join(output_path, output_file)
with open(output_path, "w", encoding="utf-8") as f:
    f.write(output_text)

print("Creating RC file done.")
